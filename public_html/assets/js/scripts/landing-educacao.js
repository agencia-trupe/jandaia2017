$(document).ready(function() {
    var $landingEducacaoWrapper = $('.landing-educacao-banners');

    var cycleConfig = {
        slides: 'img',
        pager: '> .cycle-pager',
        pagerTemplate: '<a href="#">{{slideNum}}</a>',
    };

    $landingEducacaoWrapper.cycle(cycleConfig);
});
