$('document').ready( function(){

	Modernizr.load([
		{
			test: Modernizr.input.placeholder,
		  	nope: BASE+'/assets/js/plugins/polyfill-placeholder.js'
		}
	]);

	if($('#timeline').length){
		$().timelinr({
	      orientation: 'horizontal',
	      containerDiv: '#timeline',
	      datesDiv: '#dates',
	      datesSelectedClass: 'selected',
	      datesSpeed: 'normal',
	      issuesDiv : '#issues',
	      issuesSelectedClass: 'selected',
	      issuesSpeed: 'fast',
	      issuesTransparency: 0.2,
	      issuesTransparencySpeed: 500,
	      prevButton: '#prev',
	      nextButton: '#next',
	      arrowKeys: 'false',
	      startAt: 1,
	      autoPlay: 'false',
	      autoPlayDirection: 'forward',
	      autoPlayPause: 2000
	   	});
	}

	if($('#banners #slides .animate .escondido-desktop').length){
		if($('#banners #slides .animate .escondido-desktop').css('display') == 'none'){
			$('#banners #slides .animate .escondido-desktop').remove();
		}
	}

	$('#banners #slides .animate').cycle({
		pause : true,
		timeout : 7000,
		pager : $('#banners #pager'),
		pagerAnchorBuilder: function(idx, slide) { 
	        return '<a href="#"></a>'; 
	    }
	});

	$('#seleciona-tipo .selecionado').click( function(e){
		e.preventDefault();
		var parent = $(this).parent();
		if(parent.hasClass('opcoes-escondidas'))
			parent.removeClass('opcoes-escondidas');
		else
			parent.addClass('opcoes-escondidas');
	});

	$('#seleciona-tipo').on('mouseleave', function(){
		if(!$(this).hasClass('opcoes-escondidas')){
			setTimeout( function(){
				$('#seleciona-tipo').addClass('opcoes-escondidas');
			}, 300);
		}
	});

	$('#form-contato').submit( function(){
		if($('#input-nome').val() == ''){
			alert('Informe seu Nome');
			return false;
		}
		if($('#input-email').val() == ''){
			alert('Informe seu Email');
			return false;
		}
		if($('#input-mensagem').val() == ''){
			alert('Informe sua Mensagem');
			return false;
		}
	});

	$('#form-cadastro').submit( function(){
		if($('#input-nome').val() == ''){
			alert('Informe seu Nome');
			return false;
		}
		if($('#input-email').val() == ''){
			alert('Informe seu Email');
			return false;
		}
	});

	$('.linksVideoExportacao a').fancybox({
		padding		 	 : 0,
		titleShow		 : false,
		autoScale		 : true,
		type			 : "iframe",
		iframe : {
			preload: false
	    }	
	});

	$('nav ul li.first a').click( function(e){
		e.preventDefault();
		$('nav ul').toggleClass('escondido-mobile');
	});

	$('.main-representantes-produtos aside h1').click( function(){
		$('.main-representantes-produtos aside').toggleClass('aberto');
	})

});