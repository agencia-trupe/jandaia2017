$(document).ready(function() {

    var $handle = $('#mobile-toggle'),
        $nav    = $('#nav-mobile');

    $handle.on('click touchstart', function(event) {
        event.preventDefault();
        $nav.slideToggle();
        $handle.toggleClass('close');
    });

    var $bannersWrapper = $('.banners');

    var cycleConfig = {
        slides: '> a',
        pager: '> .cycle-pager',
        pagerTemplate: '<a href="#">{{slideNum}}</a>',
    };

    $bannersWrapper.cycle(cycleConfig);

    $('.aside-linhas .mobile > a').on('click touchstart', function(event) {
        event.preventDefault();
        $(this).toggleClass('active').next().slideToggle();
    });

    $('#sel-estado').change( function(){
        var id_estado = $(this).val();
        if(id_estado){
            $.getJSON('ajax/pegarCidades/'+id_estado, function(resposta){
                $('#sel-cidade').html("<option value=''>Carregando Cidades...</option>");

                var itens = "<option value=''>Todas as Cidades do Estado</option>";
                resposta.map( function(a, b){
                    itens += "<option value='"+a.id+"'>"+a.nome+"</option>";
                });
                $('#sel-cidade').html(itens);
            });
        }
    });

    $('#sel-estado').each(function(i, e){
        if($(this).val() != ''){
            $.getJSON('ajax/pegarCidades/'+$('#sel-estado').val(), function(resposta){
                $('#sel-cidade').html("<option value=''>Carregando Cidades...</option>");

                var itens = "<option value=''>Todas as Cidades do Estado</option>";
                resposta.map( function(a, b){
                    itens += "<option value='"+a.id+"'>"+a.nome+"</option>";
                });
                $('#sel-cidade').html(itens);
                if( $('#sel-cidade').attr('data-cidade') )
                    $('#sel-cidade').val($('#sel-cidade').attr('data-cidade'))
            });
        }
    });

    $('#input-cep').mask("99999-999");

    $('.exportacao-colunas .videos a').fancybox({
        padding: 0,
        type: 'iframe',
        width: 800,
        height: 450,
        aspectRatio: true
    });

    var iniciaTimeline = function() {
        $().timelinr({
            orientation: 'horizontal',
            containerDiv: '#timeline',
            datesDiv: '#dates',
            datesSelectedClass: 'selected',
            datesSpeed: 'normal',
            issuesDiv : '#issues',
            issuesSelectedClass: 'selected',
            issuesSpeed: 'fast',
            issuesTransparency: 0.2,
            issuesTransparencySpeed: 500,
            prevButton: '#prev',
            nextButton: '#next',
            arrowKeys: 'false',
            startAt: 1,
            autoPlay: 'false',
            autoPlayDirection: 'forward',
            autoPlayPause: 2000
        });
    }

    $(window).resize(function() {
        if($('#timeline').length){
            var larguraCentro = $('.jandaia .center').width();
            $('#issues > li').css('width', larguraCentro);
            iniciaTimeline();
        }
    }).trigger('resize');

});
