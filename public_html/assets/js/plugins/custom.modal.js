(function ($){

	$.fn.customModal = function(){

		var j = this;

		return this.each( function(){

			$(this).bind('click', function(e){
				e.preventDefault();
				$.getJSON($(this).attr('href'), function(resposta){
					if(resposta.response == true)
						j.customModal.abreJanela(resposta);
					else
						return false;
				});
			});

		});
	};

	$.fn.customModal.abreJanela = function(dados){
		var shadow = $("<div class='customModal dropShadow'></div>");

		var str_janela = "";
		str_janela  = "<div class='customModal janela'>";
			str_janela += "<div class='coluna'><img src='assets/images/produtos/"+dados.imagem+"'></div>";
			str_janela += "<div class='coluna'>";
				str_janela += "<h2>linha: <strong>"+dados.linha+"</strong></h2>";
				str_janela += "<h3>"+dados.titulo+"</h3>";
				str_janela += "<h4>Descrição/dimensões:</h4>";
				str_janela += dados.detalhes;
				str_janela += "<h4>Características da linha:</h4>";
				str_janela += "<p>"+dados.linhaTexto+"</p>";
			str_janela += "</div>";
		str_janela += "</div>";

		$(shadow).append($(str_janela));
		$('body').append(shadow);

		$('.customModal.dropShadow').click( function(e){
			if(e.target == this){
				$('.customModal.dropShadow').addClass('escondido');
				setTimeout( function(){
					$('.customModal.dropShadow').remove();
				}, 310);
			}
		});
	};

}(jQuery));