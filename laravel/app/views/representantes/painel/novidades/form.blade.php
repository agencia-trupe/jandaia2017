@section('conteudo')

<div class="container">

  	<div class="page-header users-header">
    	<h2>
      		Adicionar Novidade
    	</h2>
  	</div>

  	<div class="row">
    	<div class="span12 columns">

			{{ Form::open( array('route' => 'painel.novidades.store', 'files' => true, 'method' => 'post', 'id' => 'form-create-novidade') ) }}

			<label>Imagem<br>
			<input type="file" name="imagem" required required-message="A imagem é obrigatória!"></label>

			<label>Imagem de Destaque da Home<br>
			<input type="file" name="imagem_destaque" required required-message="A imagem de destaque é obrigatória!"></label>

			<label>Título<br>
			<input type="text" name="titulo" required required-message="O título é obrigatório!" class="input-xxlarge"></label>

			<label>Data<br>
			<input type="text" name="data" class="datepicker" required required-message="A Data é obrigatória!"></label>

			<label>Olho<br>
			<textarea name="olho" class="pequeno basico"></textarea></label>

			<label>Texto<br>
			<textarea name="texto" class="medio basico"></textarea></label>

			<div class="form-actions">
	        	{{ Form::submit('Inserir', array('class' => 'btn btn-primary')) }}
	        	{{ Form::button('Voltar', array('class' => 'btn btn-voltar')) }}
	      	</div>

			{{ Form::close() }}

		</div>
	</div>

@stop