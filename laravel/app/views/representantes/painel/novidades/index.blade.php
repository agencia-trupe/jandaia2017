@section('conteudo')

<div class="container">

	@if(Session::has('sucesso'))
	  <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
  @endif

	@if(Session::has('falha'))
		<div class="alert alert-block alert-error fade in"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('falha') }}</div>
	@endif

  	<div class="page-header users-header">
    	<h2>
      		Novidades <a href="{{ URL::route('painel.novidades.create') }}" class="btn btn-success"><i class="icon-plus-sign"></i> Adicionar Novidade</a>
    	</h2>
  	</div>

  	<div class="row">
    	<div class="span12 columns">
      		<table class="table table-striped table-bordered table-condensed table-sortable" data-tabela="banners">

          		<thead>
            		<tr>
                    <th>Título</th>
                    <th>Data</th>
              			<th>Olho</th>
              			<th>Texto</th>
              			<th><i class="icon-cog"></i></th>
            		</tr>
          		</thead>

          		<tbody>
            	@foreach ($novidades as $novidade)

                	<tr class="tr-row" id="row_{{ $novidade->id }}">
                      <td>{{ $novidade->titulo }}</td>
                  		<td>{{ Tools::converteData($novidade->data) }}</td>
                      <td>{{ Str::words( $novidade->olho, 10 ) }}</td>
                      <td>{{ Str::words( $novidade->texto, 10 ) }}</td>
                  		<td class="crud-actions">
                    		<a href="{{ URL::route('painel.novidades.edit', $novidade->id ) }}" class="btn btn-primary">editar</a>

                    	   {{ Form::open(array('route' => array('painel.novidades.destroy', $novidade->id), 'method' => 'delete')) }}
                                <button type="submit" href="{{ URL::route( 'painel.novidades.destroy', $novidade->id) }}" class="btn btn-danger btn-delete">excluir</butfon>
	                       {{ Form::close() }}
                  		</td>
                	</tr>

            	@endforeach
          		</tbody>

        	</table>

        {{ $novidades->links() }}

    	</div>
  	</div>

@stop