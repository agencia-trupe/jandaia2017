@section('conteudo')

<div class="container">

  	<div class="page-header users-header">
    	<h2>
      		Alterar Novidade
    	</h2>
  	</div>

  	<div class="row">
    	<div class="span12 columns">

			{{ Form::open( array('route' => array('painel.novidades.update', $novidade->id), 'files' => true, 'method' => 'put', 'id' => 'form-edit-novidade') ) }}

			<label>Imagem<br>
			@if($novidade->imagem)
				<img src="assets/images/novidades/{{ $novidade->imagem }}"><br>
			@endif
			<input type="file" name="imagem"></label>

			<label>Imagem de Destaque da Home<br>
			@if($novidade->imagem_destaque)
				<img src="assets/images/novidades/{{ $novidade->imagem_destaque }}"><br>
			@endif
			<input type="file" name="imagem_destaque" required-message="A imagem de destaque é obrigatória!"></label>

			<label>Título<br>
			<input type="text" name="titulo" required-message="O título é obrigatório!" value="{{ $novidade->titulo }}" required class="input-xxlarge"></label>

			<label>Data<br>
			<input type="text" name="data" required-message="A Data é obrigatória!" class="datepicker" value="{{ Tools::converteData($novidade->data) }}" required></label>

			<label>Olho<br>
			<textarea name="olho" class="pequeno basico">{{ $novidade->olho }}</textarea></label>

			<label>Texto<br>
			<textarea name="texto" class="medio basico">{{ $novidade->texto }}</textarea></label>

			<div class="form-actions">
	        	{{ Form::submit('Alterar', array('class' => 'btn btn-primary')) }}
	        	{{ Form::button('Voltar', array('class' => 'btn btn-voltar')) }}
	      	</div>
		{{ Form::close() }}

		</div>
	</div>

@stop