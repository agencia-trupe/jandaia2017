@section('conteudo')

<div class="container">

  	<div class="page-header users-header">
    	<h2>
      		Alterar Linhas
    	</h2>
  	</div>

  	<div class="row">
    	<div class="span12 columns">

			{{ Form::open( array('route' => array('painel.linhas.update', $linha->id), 'files' => true, 'method' => 'put', 'id' => 'form-edit-linhas') ) }}

			<label>Título<br>
			<input type="text" name="titulo" required required-message="O título é obrigatório!" value="{{ $linha->titulo }}" class="input-xxlarge"></label>

			<label>Texto Promocional da Linha<br>
			<input type="text" name="texto_promocional" class="input-xxlarge" value="{{ $linha->texto_promocional }}"></label>

			<div class="form-actions">
	        	{{ Form::submit('Alterar', array('class' => 'btn btn-primary')) }}
	        	{{ Form::button('Voltar', array('class' => 'btn btn-voltar')) }}
	      	</div>
		{{ Form::close() }}

		</div>
	</div>

@stop