@section('conteudo')

<div class="container">

	@if(Session::has('sucesso'))
	  <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
  @endif

	@if(Session::has('falha'))
		<div class="alert alert-block alert-error fade in"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('falha') }}</div>
	@endif

  	<div class="page-header users-header">
    	<h2>
      		Produtos - Linhas <a href="{{ URL::route('painel.linhas.create') }}" class="btn btn-success"><i class="icon-plus-sign"></i> Adicionar Linha</a>
    	</h2>
  	</div>

  	<div class="row">
    	<div class="span12 columns">
      		<table class="table table-striped table-bordered table-condensed">

          		<thead>
            		<tr>
                    <th>Título</th>
              			<th>Detalhe de Título</th>
              			<th><i class="icon-cog"></i></th>
            		</tr>
          		</thead>

          		<tbody>
            	@foreach ($linhas as $linha)

                	<tr class="tr-row" id="row_{{ $linha->id }}">
                      <td>{{ $linha->titulo }}</td>
                  		<td>{{ $linha->detalhe_titulo }}</td>
                      <td class="crud-actions">
                    		<a href="{{ URL::route('painel.linhas.edit', $linha->id ) }}" class="btn btn-primary">editar</a>

                    	   {{ Form::open(array('route' => array('painel.linhas.destroy', $linha->id), 'method' => 'delete')) }}
                                <button type="submit" href="{{ URL::route( 'painel.linhas.destroy', $linha->id) }}" class="btn btn-danger btn-delete">excluir</butfon>
	                       {{ Form::close() }}
                  		</td>
                	</tr>

            	@endforeach
          		</tbody>

        	</table>
    	</div>
  	</div>

@stop