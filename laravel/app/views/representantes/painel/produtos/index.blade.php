@section('conteudo')

<div class="container">

	@if(Session::has('sucesso'))
	  <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
  @endif

	@if(Session::has('falha'))
		<div class="alert alert-block alert-error fade in"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('falha') }}</div>
	@endif

  	<div class="page-header users-header">
    	<h2>
      		Produtos
          @if($filtro_tipo)
            - {{ Tipo::find($filtro_tipo)->titulo }}
          @endif
          @if($filtro_linha)
           - {{ Linha::find($filtro_linha)->titulo }}
          @endif
          <a href="{{ URL::route('painel.produtos.create') }}" class="btn btn-success"><i class="icon-plus-sign"></i> Adicionar Produto</a>
    	</h2>
  	</div>

    {{ Form::open( array('route' => 'painel.produtos.index', 'method' => 'get', 'class' => 'form-inline') ) }}
      <select name="filtra-tipo">
        <option value="">Todos os Produtos</option>
        @foreach ($tipos as $tipo)
          <option value="{{ $tipo->id }}" @if($tipo->id == $filtro_tipo)selected @endif>{{ $tipo->titulo }}</option>
        @endforeach;
      </select>
      <select name="filtra-linha">
        <option value="">Todas as Linhas</option>
        @foreach ($linhas as $linha)
          <option value="{{ $linha->id }}" @if($linha->id == $filtro_linha)selected @endif>{{ $linha->titulo }}</option>
        @endforeach;
      </select>
      <button type="submit" class="btn btn-info">Filtrar Resultados</button>
    {{ Form::close() }}

    <hr>

  	<div class="row">
    	<div class="span12 columns">
      		<table class="table table-striped table-bordered table-condensed table-sortable" data-tabela="produtos">

          		<thead>
            		<tr>
                    @if($filtro_tipo && $filtro_linha)
                      <th>Ordenar</th>
                    @endif
              			<th>Título</th>
              			<th>Subtítulo</th>
              			<th><i class="icon-cog"></i></th>
            		</tr>
          		</thead>

          		<tbody>
            	@foreach ($produtos as $produto)

                	<tr class="tr-row" id="row_{{ $produto->id }}">
                      @if($filtro_tipo && $filtro_linha)
                        <td class="move-actions"><a href="#" class="btn btn-info btn-move">mover</a></td>
                      @endif
                  		<td>{{ $produto->titulo }}</td>
                  		<td>{{ Str::words(strip_tags($produto->detalhes),10) }}</td>
                  		<td class="crud-actions">
                    		<a href="{{ URL::route('painel.produtos.edit', $produto->id ) }}" class="btn btn-primary">editar</a>

                    	   {{ Form::open(array('route' => array('painel.produtos.destroy', $produto->id), 'method' => 'delete')) }}
                                <button type="submit" href="{{ URL::route( 'painel.produtos.destroy', $produto->id) }}" class="btn btn-danger btn-delete">excluir</butfon>
	                       {{ Form::close() }}
                  		</td>
                	</tr>

            	@endforeach
          		</tbody>

        	</table>

    	</div>
  	</div>

@stop