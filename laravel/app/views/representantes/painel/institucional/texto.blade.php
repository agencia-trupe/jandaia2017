@section('conteudo')

<div class="container">

  	<div class="page-header users-header">
    	<h2>
      		Alterar Texto Box de Certificações
    	</h2>
  	</div>

  	<div class="row">
    	<div class="span12 columns">

			{{ Form::open( array('route' => array('painel.institucional.updateTexto'), 'files' => true, 'method' => 'put', 'id' => 'form-edit-txt') ) }}

			<label>Texto<br>
			<textarea name="texto_extra" class="grande completo">{{ $registro->texto_extra }}</textarea></label>

			<div class="form-actions">
	        	{{ Form::submit('Alterar', array('class' => 'btn btn-primary')) }}
	        	{{ Form::button('Voltar', array('class' => 'btn btn-voltar')) }}
	      	</div>
		{{ Form::close() }}

		</div>
	</div>

@stop