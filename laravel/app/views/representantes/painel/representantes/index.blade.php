@section('conteudo')

<div class="container">

	@if(Session::has('sucesso'))
	  <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
  @endif

	@if(Session::has('falha'))
		<div class="alert alert-block alert-error fade in"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('falha') }}</div>
	@endif

  	<div class="page-header users-header">
    	<h2>
      		Representantes
          @if($filtra_estado)
            - {{ Estado::find($filtra_estado)->nome }}
          @endif
          @if($filtra_cidade)
           - {{ Cidade::find($filtra_cidade)->nome }}
          @endif
          <a href="{{ URL::route('painel.representantes.create') }}" class="btn btn-success"><i class="icon-plus-sign"></i> Adicionar Representante</a>
    	</h2>
  	</div>

    {{ Form::open( array('route' => 'painel.representantes.index', 'method' => 'get', 'class' => 'form-inline') ) }}
      <label>
        <select name="filtra-estado" class="seleciona_estado">
          <option value="">Todos os Estados</option>
          @foreach ($estados as $estado)
            <option value="{{ $estado->id }}" @if($estado->id == $filtra_estado) selected @endif>{{ $estado->nome }}</option>
          @endforeach;
        </select>
      </label>

      <label>
        <select name="filtra-cidade" @if ($filtra_cidade) data-cidade="{{$filtra_cidade}}" @endif class="seleciona_cidade">
          <option value="">Todas as Cidades</option>
        </select>
      </label>

      <button type="submit" class="btn btn-info">Filtrar Resultados</button>
    {{ Form::close() }}

    <hr>

  	<div class="row">
    	<div class="span12 columns">
      		<table class="table table-striped table-bordered table-condensed table-sortable" data-tabela="representantes">

          		<thead>
            		<tr>
                    <th>Área de Atuação</th>
                    <th>Região</th>
              			<th>Informações</th>
              			<th><i class="icon-cog"></i></th>
            		</tr>
          		</thead>

          		<tbody>
            	@foreach ($representantes as $representante)

                	<tr class="tr-row" id="row_{{ $representante->id }}">
                      <td>
                        @if($representante->locais)
                          @foreach($representante->locais as $local)
                            {{ Estado::find($local->id_estado)->nome }} - @if($local->id_cidade) {{ Cidade::find($local->id_cidade)->nome }} @else Todas as cidades @endif <br>
                          @endforeach
                        @endif
                      </td>
                      <td>{{ $representante->regiao }}</td>
                  		<td style="line-height:115%">
                        Empresa: {{ $representante->empresa }}<br>
                        {{ Str::words(strip_tags($representante->texto),2) }}
                        @if($representante->email)
                          <br>Email: {{ $representante->email }}
                        @endif
                      </td>
                  		<td class="crud-actions">
                    		<a href="{{ URL::route('painel.representantes.edit', $representante->id ) }}" class="btn btn-primary">editar</a>

                    	   {{ Form::open(array('route' => array('painel.representantes.destroy', $representante->id), 'method' => 'delete')) }}
                                <button type="submit" href="{{ URL::route( 'painel.representantes.destroy', $representante->id) }}" class="btn btn-danger btn-delete">excluir</butfon>
	                       {{ Form::close() }}
                  		</td>
                	</tr>

            	@endforeach
          		</tbody>

        	</table>
          {{ $representantes->appends(array('filtra-estado' => $filtra_estado, 'filtra-cidade' => $filtra_cidade))->links() }}
    	</div>
  	</div>

@stop