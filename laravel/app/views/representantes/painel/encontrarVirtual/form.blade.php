@section('conteudo')

<div class="container">

  	<div class="page-header users-header">
    	<h2>
      		Adicionar Lista
    	</h2>
  	</div>

  	<div class="row">
    	<div class="span12 columns">

			{{ Form::open( array('route' => 'painel.onde-encontrar-online.store', 'files' => true, 'method' => 'post', 'id' => 'form-create-lista') ) }}

			<label>Título<br>
			<input type="text" name="titulo" required required-message="O título é obrigatório!" class="input-xxlarge"></label>

			<label>Endereço (URL)<br>
			<input type="text" name="link" required required-message="O endereço é obrigatório!" class="input-xxlarge"></label>

			<label style="width:340px">
				Imagem<br>
				<input type="file" name="imagem" required>
			</label>

			<div class="form-actions">
	        	{{ Form::submit('Inserir', array('class' => 'btn btn-primary disable-after-submit')) }}
	        	{{ Form::button('Voltar', array('class' => 'btn btn-voltar')) }}
	      	</div>

			{{ Form::close() }}

		</div>
	</div>

@stop
