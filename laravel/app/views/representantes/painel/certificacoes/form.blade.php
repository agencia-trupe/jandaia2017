@section('conteudo')

<div class="container">

  	<div class="page-header users-header">
    	<h2>
      		Adicionar Certificação
    	</h2>
  	</div>

  	<div class="row">
    	<div class="span12 columns">

			{{ Form::open( array('route' => 'painel.certificacoes.store', 'files' => true, 'method' => 'post', 'id' => 'form-create-certif') ) }}

			<label>Título<br>
			<input type="text" name="titulo" required required-message="O título é obrigatório!" class="input-xxlarge"></label>

			<label>Texto<br>
			<textarea type="text" name="texto" class="basico medio input-xxlarge"></textarea></label>
	
			<label>Imagem<br>
			<input type="file" name="imagem" required required-message="A imagem é Obrigatória!"></label>

			<div class="form-actions">
	        	{{ Form::submit('Inserir', array('class' => 'btn btn-primary')) }}
	        	{{ Form::button('Voltar', array('class' => 'btn btn-voltar')) }}
	      	</div>
		{{ Form::close() }}

		</div>
	</div>

@stop