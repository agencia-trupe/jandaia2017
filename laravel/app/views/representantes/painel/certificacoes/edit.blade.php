@section('conteudo')

<div class="container">

  	<div class="page-header users-header">
    	<h2>
      		Alterar Certificação
    	</h2>
  	</div>

  	<div class="row">
    	<div class="span12 columns">

			{{ Form::open( array('route' => array('painel.certificacoes.update', $certificacao->id), 'files' => true, 'method' => 'put', 'id' => 'form-alter-certif') ) }}

			<label>Título<br>
			<input type="text" name="titulo" required required-message="O título é obrigatório!" class="input-xxlarge" value="{{$certificacao->titulo}}"></label>

			<label>Texto<br>
			<textarea type="text" name="texto" class="basico medio input-xxlarge">{{$certificacao->texto}}</textarea></label>
	
			<label>Imagem<br>
			@if($certificacao->imagem)
				<img src="assets/images/certificacoes/{{$certificacao->imagem}}"><br>
			@endif
			<input type="file" name="imagem"></label>

			<div class="form-actions">
	        	{{ Form::submit('Alterar', array('class' => 'btn btn-primary')) }}
	        	{{ Form::button('Voltar', array('class' => 'btn btn-voltar')) }}
	      	</div>
		{{ Form::close() }}

		</div>
	</div>

@stop