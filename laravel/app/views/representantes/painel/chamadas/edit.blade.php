@section('conteudo')

<div class="container">

  	<div class="page-header users-header">
    	<h2>
      		Alterar Chamada
    	</h2>
  	</div>

  	<div class="row">
    	<div class="span12 columns">

			{{ Form::open( array('route' => array('painel.chamadas.update', $chamada->id), 'files' => true, 'method' => 'put', 'id' => 'form-edit-chamada') ) }}

			<label>Imagem (370x260px)<br>
			@if($chamada->imagem)
				<img src="assets/images/chamadas/{{ $chamada->imagem }}"><br>
			@endif
			<input type="file" name="imagem"></label>

			<label>Título<br>
			<input type="text" name="titulo" required value="{{ $chamada->titulo }}" class="input-xxlarge"></label>

			<label>Link<br>
			<input type="text" name="link" required value="{{ $chamada->link }}" class="input-xxlarge"></label>

			<div class="form-actions">
	        	{{ Form::submit('Alterar', array('class' => 'btn btn-primary')) }}
	        	{{ Form::button('Voltar', array('class' => 'btn btn-voltar')) }}
	      	</div>
		{{ Form::close() }}

		</div>
	</div>

@stop
