@section('conteudo')

<div class="container">

	@if(Session::has('sucesso'))
	  <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
  @endif

	@if(Session::has('falha'))
		<div class="alert alert-block alert-error fade in"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('falha') }}</div>
	@endif

  	<div class="page-header users-header">
    	<h2>
      		Usuários <a href="{{ URL::route('painel.usuarios.create') }}" class="btn btn-success"><i class="icon-plus-sign"></i> Adicionar Usuário</a>
    	</h2>
  	</div>

  	<div class="row">
    	<div class="span12 columns">
      		<table class="table table-striped table-bordered table-condensed">

          		<thead>
            		<tr>
              			<th>Usuário</th>
              			<th>E-mail</th>
              			<th><i class="icon-cog"></i></th>
            		</tr>
          		</thead>

          		<tbody>
            	@foreach ($usuarios as $usuario)

                	<tr class="tr-row">
                  		<td>{{ $usuario->username }}</td>
                  		<td>{{ $usuario->email }}</td>
                  		<td class="crud-actions">
                    		<a href="{{ URL::route('painel.usuarios.edit', $usuario->id ) }}" class="btn btn-primary">editar</a>

                    	   {{ Form::open(array('route' => array('painel.usuarios.destroy', $usuario->id), 'method' => 'delete')) }}
                                <button type="submit" href="{{ URL::route( 'painel.usuarios.destroy', $usuario->id) }}" class="btn btn-danger btn-delete">excluir</butfon>
	                       {{ Form::close() }}
                  		</td>
                	</tr>

            	@endforeach
          		</tbody>

        	</table>
    	</div>
  	</div>

@stop