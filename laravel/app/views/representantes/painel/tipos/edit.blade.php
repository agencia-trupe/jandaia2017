@section('conteudo')

<div class="container">

  	<div class="page-header users-header">
    	<h2>
      		Alterar Tipo
    	</h2>
  	</div>

  	<div class="row">
    	<div class="span12 columns">

			{{ Form::open( array('route' => array('painel.tipos.update', $tipo->id), 'files' => true, 'method' => 'put', 'id' => 'form-edit-tipo') ) }}

			<label>Imagem<br>
			@if($tipo->imagem)
				<img src="assets/images/produtos/tipos/{{ $tipo->imagem }}"><br>
			@endif

			<label>Título<br>
			<input type="text" name="titulo" required required-message="O título é obrigatório!" value="{{ $tipo->titulo }}" class="input-xxlarge"></label>

			<label>Cor<br>
			<input type="text" name="cor" value="{{ $tipo->cor }}"></label>

			<div class="form-actions">
	        	{{ Form::submit('Alterar', array('class' => 'btn btn-primary')) }}
	        	{{ Form::button('Voltar', array('class' => 'btn btn-voltar')) }}
	      	</div>
		{{ Form::close() }}

		</div>
	</div>

@stop