@extends('templates.representantes')

@section('conteudo')

<aside>

	<h1 class="azul">LINHAS</h1>

	@if($listaLinhas)
		<ul id="menu-linhas">
		@foreach($listaLinhas as $val)
			<li><a href="produtos/linha/{{ $val->slug }}" title="{{ $val->titulo }}"><span>&raquo;</span> {{ $val->titulo }}</a></li>
		@endforeach
		</ul>
	@endif

</aside>

<section>

	@if($listaTipos)
		<div id="seleciona-tipo" class="opcoes-escondidas">

			<a href="produtos/" title="Exibindo Busca" class="cor-produto-0 selecionado">exibindo <strong>BUSCA</strong> <i></i></a>

			@foreach($listaTipos as $val)
				<a href="produtos/{{ $val->slug }}" title="Exibir {{$val->titulo}}" class="cor-produto-{{$val->id}}">exibir <strong>{{ $val->titulo }}</strong></a>
			@endforeach
		</div>
	@endif

	<form action="{{ URL::route('representantes.busca') }}" method="post" id="form-busca" class="topo">
		<span>BUSCA</span>
		<input type="text" name="termo" class="reduzido" required placeholder="PALAVRA-CHAVE">
		<input type="submit" value="buscar" title="Buscar">
	</form>

	<div id="produtos" class="por_linha">

		<h1 class="azul fs24 espac-bot">{{ $mensagem }}</h1>

		@if($resultados)
			@foreach($resultados as $resultado)
				<a href="produtos/linha/{{ $resultado->slug }}" class="link-resultado" title="{{ $resultado->titulo }}">ver linha {{ $resultado->titulo }}</a>
			@endforeach
		@endif

	</div>

</section>

@stop