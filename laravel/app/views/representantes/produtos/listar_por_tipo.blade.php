@section ('conteudo')

<aside>

	<h1 class="azul">LINHAS</h1>

	@if($listaLinhas)
		<ul id="menu-linhas">
		@foreach($listaLinhas as $val)
			@if(!is_null($linha) && $val->slug == $linha->slug)
				<li><a href="produtos/linha/{{ $val->slug }}" class="ativo" title="{{ $val->titulo }}"><span>&raquo;</span> {{ $val->titulo }}</a></li>
			@else
				<li><a href="produtos/linha/{{ $val->slug }}" title="{{ $val->titulo }}"><span>&raquo;</span> {{ $val->titulo }}</a></li>
			@endif
		@endforeach
		</ul>
	@endif

</aside>

<section>

	@if($listaTipos)
		<div id="seleciona-tipo" class="opcoes-escondidas">

			@if(!is_null($tipo))
				<a href="produtos/{{ $tipo->slug }}" title="Exibindo {{$tipo->titulo}}" class="cor-produto-{{$tipo->id}} selecionado">exibindo <strong>{{ $tipo->titulo }}</strong> <i></i></a>
			@else
				<a href="produtos/linha" title="Exibindo Linha" class="cor-produto-0 selecionado">exibindo <strong>LINHA</strong> <i></i></a>
			@endif

			@foreach($listaTipos as $val)
				@if(!is_null($tipo) && $val->slug != $tipo->slug)
					<a href="produtos/{{ $val->slug }}" title="Exibir {{$val->titulo}}" class="cor-produto-{{$val->id}}">exibir <strong>{{ $val->titulo }}</strong></a>
				@endif
			@endforeach
		</div>
	@endif

	<form action="{{ URL::route('representantes.busca') }}" method="post" id="form-busca" class="topo">
		<span>BUSCA</span>
		<input type="text" name="termo" class="reduzido" required placeholder="PALAVRA-CHAVE">
		<input type="submit" value="buscar" title="Buscar">
	</form>

	<div id="produtos" class="maiores">
		@if($listaProdutos)
			@foreach($listaProdutos as $produto)
				<a href="produtos/{{ $produto->tipo_slug }}/{{ $produto->linha_slug }}/{{ $produto->slug }}" title="{{ $produto->titulo }}">
					<img src="assets/images/produtos/thumbs/{{ $produto->imagem }}" alt="{{ $produto->titulo }}">
					<div class="overlay"></div>
				</a>
			@endforeach
		@endif
	</div>

</section>

@stop