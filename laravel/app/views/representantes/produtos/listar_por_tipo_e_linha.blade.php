@section ('conteudo')

<aside>

	<h1 class="azul">LINHAS</h1>

	@if($listaLinhas)
		<ul id="menu-linhas">
		@foreach($listaLinhas as $val)
			@if(!is_null($linha) && $val->slug == $linha->slug)
				<li><a href="produtos/linha/{{ $val->slug }}" class="ativo" title="{{ $val->titulo }}"><span>&raquo;</span> {{ $val->titulo }}</a></li>
			@else
				<li><a href="produtos/linha/{{ $val->slug }}" title="{{ $val->titulo }}"><span>&raquo;</span> {{ $val->titulo }}</a></li>
			@endif
		@endforeach
		</ul>
	@endif

</aside>

<section>

	@if($listaTipos)
		<div id="seleciona-tipo" class="opcoes-escondidas">

			@if(!is_null($tipo))
				<a href="produtos/{{ $tipo->slug }}{{ is_null($linha) ? '' : '/'.$linha->slug }}" title="Exibindo {{$tipo->titulo}}" class="cor-produto-{{$tipo->id}} selecionado">exibindo <strong>{{ $tipo->titulo }}</strong> <i></i></a>
			@else
				<a href="produtos/linha{{ is_null($linha) ? '' : '/'.$linha->slug }}" title="Exibindo Linha" class="cor-produto-0 selecionado">exibindo <strong>LINHA</strong> <i></i></a>
			@endif

			@foreach($listaTipos as $val)
				@if(!is_null($tipo) && $val->slug != $tipo->slug)
					<a href="produtos/{{ $val->slug }}" title="Exibir {{$val->titulo}}" class="cor-produto-{{$val->id}}">exibir <strong>{{ $val->titulo }}</strong></a>
				@endif
			@endforeach
			@if(!is_null($linha))
				<a href="produtos/linha/{{ $linha->slug }}" title="exibir Linha {{ $linha->titulo }}" class="cor-produto-0">exibir Linha Completa</a>
			@endif
		</div>
	@endif

	<form action="{{ URL::route('representantes.busca') }}" method="post" id="form-busca" class="topo">
		<span>BUSCA</span>
		<input type="text" name="termo" class="reduzido" required placeholder="PALAVRA-CHAVE">
		<input type="submit" value="buscar" title="Buscar">
	</form>

	<div id="produtos">

		<h1 class="azul fs24 espac-bot-red">Linha <strong>{{ $linha->titulo }}</strong> | <span class="menor">{{ $tipo->titulo }}</span></h1>

		@if($produto)

			<div class="coluna esquerda">

				<div id="ampliada">
					<img src="assets/images/produtos/{{ $produto->imagem }}" alt="{{ $produto->titulo }}">
				</div>

				@if($listaProdutosMesmoTipoCapas)
					<span class="listagem">
						@foreach($listaProdutosMesmoTipoCapas as $prod)
								<a href="produtos/{{ $tipo->slug }}/{{ $linha->slug }}/{{ $prod->slug }}" title="{{ $prod->titulo }}" @if(!is_null($produto) && $prod->id == $produto->id) class='ativo' @endif>
									<img src="assets/images/produtos/thumbs/{{ $prod->imagem }}" alt="{{ $prod->titulo }}">
									<div class="overlay"></div>
								</a>
						@endforeach
					</span>
				@endif

			</div>

			<div class="coluna direita">

				<div id="detalhes" @if($esticarCaixa) class="esticada" @endif >
					<h1 class="azul bold">{{ $tipo->titulo }}
						@if($tipo->id == 11)
							@if($produto->tipo_caderno == 1)
								BROCHURA
							@elseif($produto->tipo_caderno == 2)
								ESPIRAL
							@elseif($produto->tipo_caderno == 3)
								PLUS
							@elseif($produto->tipo_caderno == 4)
								PEDAGÓGICOS
							@endif
						@endif
					</h1>
					<h3>{{ $produto->titulo }}</h3>
					{{ $produto->detalhes }}
				</div>

				@if($listaProdutosMesmoTipoMiolos)
					<span class="listagem">
						@foreach($listaProdutosMesmoTipoMiolos as $prod)
							<a href="produtos/{{ $tipo->slug }}/{{ $linha->slug }}/{{ $prod->slug }}" title="{{ $prod->titulo }}" @if(!is_null($produto) && $prod->id == $produto->id) class='ativo' @endif>
								<img src="assets/images/produtos/thumbs/{{ $prod->imagem }}" alt="{{ $prod->titulo }}">
								<div class="overlay"></div>
							</a>
						@endforeach
					</span>
				@endif

			</div>

			<?php $tipo_caderno_atual = $produto->tipo_caderno;?>
		@else
			<?php $tipo_caderno_atual = 0?>
		@endif

		<?php $contador = 0; ?>
		@if($listaProdutos)
			<span>
				@foreach($listaProdutos as $i => $prod)

					@if($prod->produtos_tipo_id == 11 && $prod->tipo_caderno != $tipo_caderno_atual && !in_array($prod->tipo_caderno, array(5,6,7)))
						</span>
						<h1 class="branco espac-top-med">
							@if($prod->tipo_caderno == 1)
								BROCHURA
							@elseif($prod->tipo_caderno == 2)
								ESPIRAL
							@elseif($prod->tipo_caderno == 3)
								PLUS
							@elseif($prod->tipo_caderno == 4)
								PEDAGÓGICO
							@elseif($prod->tipo_caderno == 8)
								CARTOGRÁFICO
							@endif
						</h1>
						<span>
						<?php $tipo_caderno_atual = $prod->tipo_caderno ?>
					@endif
					<a href="produtos/{{ $prod->tipo_slug }}/{{ $prod->linha_slug }}/{{ $prod->slug }}" title="{{ $prod->titulo }}" @if(!is_null($produto) && $prod->id == $produto->id) class='ativo' @endif>
						<img src="assets/images/produtos/thumbs/{{ $prod->imagem }}" alt="{{ $prod->titulo }}">
						<div class="overlay"></div>
					</a>
					<?php $contador++; ?>
					@if($contador%4 == 0)
						<br>
					@endif
				@endforeach
			</span>
		@endif

		@if(isset($outrosProdutos) && $outrosProdutos)
			<h1 class="branco espac-top espac-bot">VER MAIS PRODUTOS DA MESMA LINHA</h1>
			@foreach($outrosProdutos as $links)
				<a href="produtos/{{$links->slug}}/{{ $linha->slug }}" title="ver {{ $links->titulo }} da linha {{ $linha->titulo }}" class="outros-tipos">{{ mb_strtoupper($links->titulo) }}</a>
			@endforeach
		@endif
	</div>

</section>

@stop