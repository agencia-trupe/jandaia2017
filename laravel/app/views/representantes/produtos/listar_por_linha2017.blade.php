@section ('conteudo')

<div class="produtos">
	<div class="center">
		<div class="submenu-tipos">
			@foreach($listaTipos as $t)
				<a href="produtos/{{ $t->slug }}">
					{{ $t->titulo }}
				</a>
			@endforeach
		</div>

		<div class="aside-linhas">
			<h2>Linhas</h2>

			<div class="desktop">
				<h3>SELECIONE A LINHA</h3>
				@if($listaLinhas)
					@foreach($listaLinhas as $l)
						@if(!is_null($linha) && $l->slug == $linha->slug)
							<a href="produtos/linha/{{ $l->slug }}" class="active">
								<span>&raquo;</span> {{ $l->titulo }}
							</a>
						@else
							<a href="produtos/linha/{{ $l->slug }}">
								<span>&raquo;</span> {{ $l->titulo }}
							</a>
						@endif
					@endforeach
				@endif
			</div>
			<div class="mobile">
				<a href="#">SELECIONAR LINHA</a>
				<div>
					@if($listaLinhas)
						@foreach($listaLinhas as $l)
							@if(!is_null($linha) && $l->slug == $linha->slug)
								<a href="produtos/linha/{{ $l->slug }}" class="active">
									<span>&raquo;</span> {{ $l->titulo }}
								</a>
							@else
								<a href="produtos/linha/{{ $l->slug }}">
									<span>&raquo;</span> {{ $l->titulo }}
								</a>
							@endif
						@endforeach
					@endif
				</div>
			</div>
		</div>

		<div class="lista-produtos-linha">
			<h1>
				Linha <strong>{{ $linha->titulo }}</strong>
				<span>&middot; todos os produtos da linha</span>
			</h1>

			@if($linha->texto_promocional)
				<h3>{{ $linha->texto_promocional }}</h3>
			@endif

			@if($listaProdutos)
				<?php $tipo_atual = ''; ?>
				@foreach($listaProdutos as $key => $prod)
					@if($tipo_atual != $prod->tipo_slug.$prod->miolo)
						@if($key > 0)
						</div>
						@endif
						<h2>{{ $prod->tipo_titulo }}</h2>
						<div class="lista-produtos-linha-thumbs">
						<?php $tipo_atual = $prod->tipo_slug.$prod->miolo; ?>
					@endif

					<a href="produtos/{{ $prod->tipo_slug }}/{{ $prod->linha_slug }}/{{ $prod->slug }}" title="{{ $prod->titulo }}" @if(!is_null($produto) && $prod->id == $produto->id) class='ativo' @endif>
						<img src="assets/images/produtos/thumbs/{{ $prod->imagem }}" alt="{{ $prod->titulo }}">
						<div class="overlay"></div>
					</a>
				@endforeach
			@endif
		</div>
		</div>
	</div>
</div>

@stop
