@section ('conteudo')

<div class="produtos">
	<div class="center">
		<div class="submenu-tipos">
			@foreach($listaTipos as $t)
				<a href="produtos/{{ $t->slug }}" @if(!is_null($tipo) && $tipo->slug == $t->slug) class="active" @endif>
					{{ $t->titulo }}
				</a>
			@endforeach
		</div>

		<div class="aside-linhas">
			<h2>{{ $tipo->titulo }}</h2>

			<div class="desktop">
				<h3>SELECIONE A LINHA</h3>
				@if($listaLinhas)
					@foreach($listaLinhas as $l)
						@if(!is_null($linha) && $l->slug == $linha->slug)
							<a href="produtos/linha/{{ $l->slug }}" class="active">
								<span>&raquo;</span> {{ $l->titulo }}
							</a>
						@else
							<a href="produtos/linha/{{ $l->slug }}">
								<span>&raquo;</span> {{ $l->titulo }}
							</a>
						@endif
					@endforeach
				@endif
			</div>
			<div class="mobile">
				<a href="#">SELECIONAR LINHA</a>
				<div>
					@if($listaLinhas)
						@foreach($listaLinhas as $l)
							@if(!is_null($linha) && $l->slug == $linha->slug)
								<a href="produtos/linha/{{ $l->slug }}" class="active">
									<span>&raquo;</span> {{ $l->titulo }}
								</a>
							@else
								<a href="produtos/linha/{{ $l->slug }}">
									<span>&raquo;</span> {{ $l->titulo }}
								</a>
							@endif
						@endforeach
					@endif
				</div>
			</div>
		</div>

		<div class="lista-produtos-tipo">
			@if($listaProdutos)
				@foreach($listaProdutos as $produto)
					<a href="produtos/{{ $produto->tipo_slug }}/{{ $produto->linha_slug }}/{{ $produto->slug }}">
						<img src="assets/images/produtos/thumbs/{{ $produto->imagem }}" alt="{{ $produto->titulo }}">
						<div class="overlay"></div>
					</a>
				@endforeach
			@endif
		</div>
	</div>
</div>

@stop
