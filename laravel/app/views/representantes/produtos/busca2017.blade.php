@extends('templates.representantes2017')

@section('conteudo')

<div class="produtos">
	<div class="center">
		<div class="submenu-tipos">
			@foreach($listaTipos as $t)
				<a href="produtos/{{ $t->slug }}">
					{{ $t->titulo }}
				</a>
			@endforeach
		</div>

		<div class="aside-linhas">
			<div class="desktop">
				<h3>SELECIONE A LINHA</h3>
				@if($listaLinhas)
					@foreach($listaLinhas as $l)
						<a href="produtos/linha/{{ $l->slug }}">
							<span>&raquo;</span> {{ $l->titulo }}
						</a>
					@endforeach
				@endif
			</div>
			<div class="mobile">
				<a href="#">SELECIONAR LINHA</a>
				<div>
					@if($listaLinhas)
						@foreach($listaLinhas as $l)
							<a href="produtos/linha/{{ $l->slug }}">
								<span>&raquo;</span> {{ $l->titulo }}
							</a>
						@endforeach
					@endif
				</div>
			</div>
		</div>

		<div class="resultados-busca">
			<h2>{{ $mensagem }}</h2>

			@if($resultados)
				@foreach($resultados as $resultado)
					<a href="produtos/{{ $resultado->tipo_slug }}/{{ $resultado->linha_slug }}/{{ $resultado->slug }} class="link-resultado" title="{{ $resultado->titulo }}">{{ $resultado->titulo }}</a>
				@endforeach
			@endif
		</div>
	</div>
</div>

@stop
