@section ('conteudo')

<div class="produtos">
	<div class="center">
		<div class="submenu-tipos">
			@foreach($listaTipos as $t)
				<a href="produtos/{{ $t->slug }}">
					{{ $t->titulo }}
				</a>
			@endforeach
		</div>

		<div class="aside-linhas">
			<h2>Linhas</h2>

			<div class="desktop">
				<h3>SELECIONE A LINHA</h3>
				@if($listaLinhas)
					@foreach($listaLinhas as $l)
						@if(!is_null($linha) && $l->slug == $linha->slug)
							<a href="produtos/linha/{{ $l->slug }}" class="active">
								<span>&raquo;</span> {{ $l->titulo }}
							</a>
						@else
							<a href="produtos/linha/{{ $l->slug }}">
								<span>&raquo;</span> {{ $l->titulo }}
							</a>
						@endif
					@endforeach
				@endif
			</div>
			<div class="mobile">
				<a href="#">SELECIONAR LINHA</a>
				<div>
					@if($listaLinhas)
						@foreach($listaLinhas as $l)
							@if(!is_null($linha) && $l->slug == $linha->slug)
								<a href="produtos/linha/{{ $l->slug }}" class="active">
									<span>&raquo;</span> {{ $l->titulo }}
								</a>
							@else
								<a href="produtos/linha/{{ $l->slug }}">
									<span>&raquo;</span> {{ $l->titulo }}
								</a>
							@endif
						@endforeach
					@endif
				</div>
			</div>
		</div>

		<div class="lista-produtos-detalhe">
			@if($produto)
				<div class="esquerda">
					<img src="assets/images/produtos/{{ $produto->imagem }}" alt="{{ $produto->titulo }}">
				</div>

				<div class="direita">
					<div class="detalhes">
						<h1>{{ $produto->titulo }}</h1>
						{{ $produto->detalhes }}
					</div>

					@if($listaProdutosMesmoTipoMiolos)
						<div class="lista-produtos-miolos">
							@foreach($listaProdutosMesmoTipoMiolos as $prod)
								<a href="produtos/{{ $tipo->slug }}/{{ $linha->slug }}/{{ $prod->slug }}" title="{{ $prod->titulo }}" @if(!is_null($produto) && $prod->id == $produto->id) class='ativo' @endif>
									<img src="assets/images/produtos/thumbs/{{ $prod->imagem }}" alt="{{ $prod->titulo }}">
									<div class="overlay"></div>
								</a>
							@endforeach
						</div>
					@endif
				</div>
				<?php $tipo_caderno_atual = $produto->tipo_caderno;?>
				<?php $tipo_agenda_atual = $produto->tipo_agenda; ?>
			@else
				<?php $tipo_caderno_atual = 0?>
				<?php $tipo_agenda_atual = 0?>
			@endif
			<div class="compartilhamento">
				<div id="fb-root"></div>
				<script>(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.9&appId=114778919157489";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));</script>
				<div class="fb-like" data-href="{{ Request::url() }}" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
			</div>

			@if($listaProdutosMesmoTipoCapas)
				<div class="lista-produtos-capas" style="float:left;width:100%;">
					@foreach($listaProdutosMesmoTipoCapas as $prod)
						<a href="produtos/{{ $tipo->slug }}/{{ $linha->slug }}/{{ $prod->slug }}" title="{{ $prod->titulo }}" @if(!is_null($produto) && $prod->id == $produto->id) class='ativo' @endif>
							<img src="assets/images/produtos/thumbs/{{ $prod->imagem }}" alt="{{ $prod->titulo }}">
							<div class="overlay"></div>
						</a>
					@endforeach
				</div>
			@endif

			<div style="float:left;width:100%">
			<?php $contador = 0; ?>
			@if($listaProdutos)
				<div class="lista-produtos-linha-thumbs">
					@foreach($listaProdutos as $i => $prod)
						@if($prod->produtos_tipo_id == 11 && $prod->tipo_caderno != $tipo_caderno_atual && !in_array($prod->tipo_caderno, array(5,6,7)))
							</div>
							<h2>
								@if($prod->tipo_caderno == 1)
									BROCHURA
								@elseif($prod->tipo_caderno == 2)
									ESPIRAL
								@elseif($prod->tipo_caderno == 3)
									PLUS
								@elseif($prod->tipo_caderno == 4)
									PEDAGÓGICO
								@elseif($prod->tipo_caderno == 8)
									CARTOGRÁFICO
								@endif
							</h2>
							<div class="lista-produtos-linha-thumbs">
							<?php $tipo_caderno_atual = $prod->tipo_caderno ?>
						@endif
						@if($prod->produtos_tipo_id == 12 && $prod->tipo_agenda != $tipo_agenda_atual && in_array($prod->tipo_agenda, array(1,2)))
							</div>
							<h2>
								@if($prod->tipo_agenda == 1)
									DIÁRIA
								@elseif($prod->tipo_agenda == 2)
									PERMANENTE
								@endif
							</h2>
							<div class="lista-produtos-linha-thumbs">
							<?php $tipo_agenda_atual = $prod->tipo_agenda ?>
						@endif
						<a href="produtos/{{ $prod->tipo_slug }}/{{ $prod->linha_slug }}/{{ $prod->slug }}" title="{{ $prod->titulo }}" @if(!is_null($produto) && $prod->id == $produto->id) class='ativo' @endif>
							<img src="assets/images/produtos/thumbs/{{ $prod->imagem }}" alt="{{ $prod->titulo }}">
							<div class="overlay"></div>
						</a>
						<?php $contador++; ?>
					@endforeach
				</div>
			@endif
			</div>

			@if(isset($outrosProdutos) && $outrosProdutos)
			<div class="mais-mesma-linha">
				<p>VER MAIS PRODUTOS DA MESMA LINHA:</p>
				@foreach($outrosProdutos as $links)
					<a href="produtos/{{$links->slug}}/{{ $linha->slug }}" class="mais-{{ $links->slug }}">&raquo; {{ $links->titulo }}</a>
				@endforeach
			</div>
			@endif
		</div>
	</div>
</div>

@stop
