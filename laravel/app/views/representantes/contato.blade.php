@section('conteudo')

	<div class="conteudo">
		<section>

			<h1>CONTATO</h1>

			<p class="branco">
				<a href="mailto:sac@jandaia.com" title="Entrar em contato">sac@jandaia.com</a><br>
				SAC 0800 165656
			</p>

			@if(Session::has('envio'))
				@if(Session::get('envio'))
					<div class="resposta sucesso">
						Email de contato enviado com sucesso.<br>
						Agradecemos o contato e responderemos assim que possível.
					</div>
				@else
					<div class="resposta falha">
						Erro ao enviar o email.<br>
						Verifique se todos os campos obrigatórios foram preenchidos.
					</div>
				@endif
			@endif

			<form action="{{ URL::route('representantes.contato.enviar') }}" method="post" id="form-contato">

				<h2>ENVIE UMA MENSAGEM</h2>

				<input type="text" name="nome" placeholder="nome" required id="input-nome">

				<input type="email" name="email" placeholder="e-mail" required id="input-email">

				<input type="text" name="telefone" placeholder="telefone">

				<textarea name="mensagem" placeholder="mensagem" required id="input-mensagem"></textarea>

				<input type="submit" value="ENVIAR">

			</form>
			
			<!--
			<div class="press">
				<h3>Assessoria de Imprensa</h3>
				<div class="coluna">
					img-marca-assessoria
				</div>
				<div class="coluna">
					<p>
						Nome<br>
						Telefones<br>
						<a href="mailto:" title="Envie um e-mail" target="_blank"></a>
					</p>
				</div>
			</div>
			-->

		</section>

		<aside>
			<div class="mapa">
				<iframe width="460" height="595" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com.br/maps?q=Rod.+Presidente+Tancredo+de+Almeida+Neves,+s%2Fn+-+Km+38,5&amp;ie=UTF8&amp;hq=&amp;hnear=Rod.+Pres.+Tancredo+de+Almeida+Neves,+38,+Caieiras+-+S%C3%A3o+Paulo,+07700-000&amp;gl=br&amp;t=m&amp;z=14&amp;ll=-23.381572,-46.737156&amp;output=embed"></iframe>
			</div>
			<p>
				Bignardi Ind. e Com. de Papéis e Artefatos Ltda. <br>
				Rod. Presidente Tancredo de Almeida Neves, s/n - Km 38,5 <br>
				Jardim Vera Tereza<br>
				07.700-000 - Caieiras, SP
			</p>
		</aside>
	</div>

@stop