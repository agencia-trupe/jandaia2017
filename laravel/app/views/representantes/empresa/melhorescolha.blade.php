@section('conteudo')

    <section class="melhor-escolha">
        <div class="centro">
            <div class="melhor-escolha-right">
                <img src="assets/images/empresa/melhorescolha/melhorescolha-titulo.png" alt="">
            </div>

            <div class="melhor-escolha-left">
                <h2>AS CAPAS DE CADERNO MAIS COBIÇADAS</h2>
                <p>Super-heróis, personagens famosos, influenciadores, moda, esportes... As capas que estampam os <strong>Cadernos Jandaia</strong> têm a cara da nova geração e são resultado do trabalho do <strong>Jandaia Lab</strong>, o laboratório de pesquisa sobre comportamento do usuário que aponta tendências e auxilia a identificar as melhores licenças do mercado.</p>
                <h2>AS MELHORES OPORTUNIDADES DE NEGÓCIOS</h2>
                <p>Crianças, jovens, adultos, meninos, meninas, geeks, pops, gamers... A <strong>Jandaia</strong> possui uma super variedade de cadernos que agrada aos mais diversos tipos de consumidores. Identifique os perfis de clientes, que a <strong>Jandaia</strong> tem o caderno perfeito para eles.</p>
            </div>

            <div class="melhor-escolha-capas">
                <img src="assets/images/empresa/melhorescolha/melhorescolha-capas.png" alt="">
            </div>
            <div class="melhor-escolha-left">
                <h2>O INCENTIVO QUE FALTAVA</h2>
                <p>A <strong>Jandaia</strong> se orgulha de ser uma empresa incentivadora das equipes que trabalham com ela, tanto internamente quanto em outros canais, porque acredita que um time motivado faz toda a diferença nos resultados.</p>
                <h2>UM TRABALHO SÉRIO QUE RESULTA EM VENDAS</h2>
                <p>As melhores licenças, produtos de qualidade, uso de tecnologia, pesquisas, apoio de vendas. Todos os profissionais <strong>Jandaia</strong> envolvidos em cada processo são comprometidos em levar os melhores produtos e dar todo o suporte que os parceiros precisam para que suas compras se convertam em vendas e em clientes cada vez mais satisfeitos.</p>
                <h2>UMA PARCERIA DURADOURA</h2>
                <p>O grande compromisso da <strong>Jandaia</strong> é fazer chegar até os parceiros as melhores licenças com a garantia de obter o melhor retorno. Os parceiros <strong>Jandaia</strong> têm à sua disposição e como apoio às suas vendas: materiais de PDV, ações mercadológicas, promotores, divulgação dos produtos na mídia e nas redes sociais. Tudo para que você possa vender mais e melhor. Pode confiar!</p>
            </div>
        </div>
    </section>

@stop
