<!DOCTYPE html>
<html lang="pt-BR" class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="robots" content="index, follow" />
  <meta name="author" content="Trupe Design" />
  <meta name="copyright" content="2013 Trupe Design" />
  <meta name="viewport" content="width=device-width,initial-scale=1">
  <link rel="shortcut icon" href="favicon-jandaia.ico">

  <title>Jandaia – Site para o lojista</title>
  <meta name="description" content=" Site para o lojista. Jandaia é uma marca de cadernos, agendas, fichários, bolsas e outros produtos de papelaria.">
  <meta name="keywords" content="cadernos jandaia, cadernos, agendas, fichários, etiquetas, bolsas," />
  <meta property="og:title" content="Jandaia – Site para o lojista"/>
  <meta property="og:site_name" content="Jandaia"/>
  <meta property="og:type" content="website"/>
  <meta property="og:image" content="{{ asset('assets/images/layout/marca-jandaia.jpg'); }}"/>
  <meta property="og:url" content="{{ Request::url() }}"/>
  <meta property="og:description" content=" Site para o lojista. Jandaia é uma marca de cadernos, agendas, fichários, bolsas e outros produtos de papelaria."/>

	<meta property="fb:admins" content="100002297057504"/>
	<meta property="fb:app_id" content="580310192049536"/>

	<base href="{{ url() }}/">
	<script>var BASE = "{{ url() }}"</script>

	<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700,700i' rel='stylesheet' type='text/css'>

	<?=Assets::CSS(array(
		'reset',
		'fancybox/fancybox',
		'representantes/international'
	))?>

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="js/jquery-1.10.2.min.js"><\/script>')</script>

	@if(App::environment()=='local')
		<?=Assets::JS(array('plugins/jquery-migrate-1.2.1','libs/modernizr-2.0.6.min', 'libs/less-1.3.0.min'))?>
	@else
		<?=Assets::JS(array('plugins/jquery-migrate-1.2.1','libs/modernizr-2.0.6.min'))?>
	@endif

	<script type="text/javascript">
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-44518347-1']);
		_gaq.push(['_trackPageview']);
		(function() {
		   var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		   ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
		   (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(ga);
		})();
	</script>

	<script type="application/ld+json">
	{ "@context" : "http://schema.org",
	  "@type" : "Organization",
	  "name" : "Cadernos Jandaia",
	  "url" : "http://jandaia.com/",
	  "logo": "http://jandaia.com/assets/images/layout/marca-jandaia.jpg",
	  "sameAs" : [ "https://www.facebook.com/cadernosjandaia",
	    "http://www.youtube.com/user/cadernosjandaia",
	    "https://instagram.com/cadernosjandaia/",
	    "https://plus.google.com/117280432477296799371"]
	}
	</script>
</head>
<body>

    <div class="centralizar">
      <header>
        <img src="assets/images/international/marca-bignardiStationery.png" id="imagem-topo">
        <img src="assets/images/international/marca-dagua.png" id="imagem-marca-dagua">
        <img src="assets/images/international/mapa-mundi.png" id="imagem-mapa">
        <div class="texto">
          <p>
            <strong>The Largest Brazilian Exporter</strong><br>
            since the last decade, exporting to <br>
            North America, Europe and Africa.
          </p>
        </div>
      </header>
      <div class="conteudo">
        @if(Session::has('contato_internacional_enviado'))
          <div class="resposta">
            Thank you. We will reply you soon.
          </div>
        @endif
        <div class="imagem">
          <img src="assets/images/international/foto-jandaia.png" alt="Jandaia">
        </div>
        <div class="texto">
          <h1>The group is composed by 3 business units</h1>
          <ul>
            <li>Paper manufacturer</li>
            <li>Notebook manufacturer</li>
            <li class="espacado">Wholesale store</li>
            <li>The largest Brazilian Exporter</li>
            <li>972 direct employees</li>
            <li>Leader on the recycled paper</li>
            <li>Big player on notebooks production</li>
          </ul>
        </div>
      </div>
      <div class="downloads">
        <img src="assets/images/international/cadernos1.png" id="imagem-cadernos">
        <ul>
          <li>
            <a href="download/BG-CAT-EXPO-2017.pdf" target="_blank" title="Export Product Catalog">
              <img src="assets/images/international/icone-download.png" alt="DOWNLOAD">
              <strong>DOWNLOAD &bull; </strong>Export Product Catalog
            </a>
          </li>
          <li>
            <a href="http://jandaia.com/download/bignardi-group-presentation.pdf" target="_blank" title="Bignardi Group Presentation">
              <img src="assets/images/international/icone-download.png" alt="DOWNLOAD">
              <strong>DOWNLOAD &bull; </strong>Bignardi Group Presentation
            </a>
          </li>
          <li>
            <a href="http://issuu.com/thiagodias51/docs/miolo_catalogo_exportacao_site" target="_blank" title="View the online catalog">
              <img src="assets/images/international/icone-view.png" alt="VIEW" class="xtra">
              <strong>VIEW &bull; </strong>View the online catalog
            </a>
          </li>
        </ul>
      </div>
      <div class="thumbs">
        <ul>
          <li>
            <a href="http://www.youtube.com/embed/q9ix0g9km8Q?wmode=opaque&autoplay=1" title="Institucional">
      			  <img src="assets/images/empresa/exportacoes/video-thumb1-institucional.png" alt="Institucional">
      			  <div class="texto">Institutional Video</div>
      		  </a>
          </li>
      		<li>
            <a href="http://www.youtube.com/embed/jWgGx2vl33s?wmode=opaque&autoplay=1" title="Bignardi Papéis">
      			  <img src="assets/images/empresa/exportacoes/video-thumb2-bignardipapeis.png" alt="Bignardi Papéis">
      			  <div class="texto">Bignardi Papéis</div>
      		  </a>
          </li>
      		<li>
            <a href="http://www.youtube.com/embed/DWIHCx6uajA?wmode=opaque&autoplay=1" title="Fabricação dos Cadernos">
      			  <img src="assets/images/empresa/exportacoes/video-thumb3-fabricacaocadernos.png" alt="Fabricação dos Cadernos">
      			  <div class="texto">Jandaia</div>
      		  </a>
          </li>
      		<li>
            <a href="http://www.youtube.com/embed/DGE-1TKa1uI?wmode=opaque&autoplay=1" title="Veja como são feitos">
      			  <img src="assets/images/empresa/exportacoes/video-thumb4-comosaofeitos.png" alt="Veja como são feitos">
      			  <div class="texto">Jandaia Promotional Video</div>
      		  </a>
          </li>
      		<li>
            <a href="http://www.youtube.com/embed/wkd4zv-WpvA?wmode=opaque&autoplay=1" title="Collection 2017">
      			  <img src="assets/images/empresa/exportacoes/video-thumb5-colecao2017.png" alt="Collection 2017">
      			  <div class="texto">Collection 2017</div>
      		  </a>
          </li>
        </ul>
      </div>
      <img src="assets/images/international/cadernos2.png" id="imagem-cadernos-2">
    </div>

    <div class="faixa-cinza">
      <div class="centralizar">
        <h2>BIGNARDI GROUP</h2>
        <h3>Composed by Bignardi, Jandaia and wholesale Jandaia. Visit our websites:</h3>
      </div>
      <a href="http://www.bignardi.com.br" title="Bignardi Papéis"><img src="assets/images/international/marca-bignardi.png" alt="Bignardi Papéis"></a>
      <a href="http://www.jandaia.com" title="Jandaia"><img src="assets/images/international/marca-jandaia.png" alt="Jandaia"></a>
      <a href="http://www.atacadaojandaia.com.br/atacadaojandaia/index.php" title="Atacadão Jandaia"><img src="assets/images/international/marca-atacadao.png" alt="Atacadão Jandaia"></a>
    </div>

    <div class="centralizar">

      <form action="international/enviar" method="post">
        <h2>CONTACT US</h2>
        <div class="coluna">
          <input type="text" name="name" placeholder="nome" required>
          <input type="email" name="email" placeholder="e-mail" required>
          <input type="text" name="telephone" placeholder="telephone" required>
          <input type="text" name="country" placeholder="country" required>
        </div>
        <div class="coluna">
          <textarea name="message" required placeholder="message"></textarea>
        </div>
        <div class="submit">
          <input type="submit" value="SEND">
        </div>
      </form>

      <footer>
        &copy; {{date('Y')}} Grupo Bignardi - All rights reserved
      </div>
    </div>

		<?=Assets::JS(array(
			'plugins/cycle',
			'plugins/fancybox'
		))?>

    <script>
    $('.thumbs a').fancybox({
  		padding		 	 : 0,
  		titleShow		 : false,
  		autoScale		 : true,
  		type			 : "iframe",
  		iframe : {
  			preload: false
  	  }
	   });
    </script>
	</body>
</html>
