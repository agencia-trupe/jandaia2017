@section ('conteudo')

<section>
	<div class="imagens">
		<img src="assets/images/empresa/qualidade.jpg" alt="Jandaia Qualidade">
	</div>
	<div class="texto">
		<h1>QUALIDADE JANDAIA</h1>
		<p>
			Desde seu início, como uma pequena indústria gráfica, até a mudança da parte fabril para um parque industrial, com uma área de 12 mil metros quadrados, na cidade de Caieiras, interior de São Paulo, a Jandaia cresce em qualidade, competência e credibilidade no mercado.
		</p>
		<p>
			Ao longo dos últimos anos, a empresa realizou investimentos em tecnologia de última geração afim de modernizar o seu processo produtivo, o que a permitiu dominar as mais avançadas técnicas na produção de cadernos.
		</p>
		<p>
			Atualizada às tendências mundiais de design, moda e comportamento, a Jandaia é hoje referência quando se pensa em produtos de papelaria de qualidade.
		</p>
	</div>
</section>

@stop