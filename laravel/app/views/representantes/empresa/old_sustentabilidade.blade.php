@section ('conteudo')

<section>
	<div class="imagens">
		<img src="assets/images/empresa/sustentabilidade.png" alt="Jandaia Sustentabilidade">
	</div>
	<div class="texto">
		<h1>SUSTENTABILIDADE</h1>
		<p>
			SUSTENTABILIDADE NAS PRÁTICAS DE NEGÓCIO
		</p>
		<p>
			A sociedade vem demandando cada vez mais ações voltadas ao desenvolvimento sustentável, que implicam no uso de tecnologias limpas e no uso racional de recursos naturais para a proteção do meio ambiente e respeito à vida.
		</p>
		<p>
			Frente a esses desafios, o Grupo Bignardi entende seu papel de empresa socialmente responsável e insere-se nos padrões mais elevados de responsabilidade socioambiental. Trata-se de um comprometimento permanente e de longo prazo da empresa que, ao conduzir seus negócios com transparência e ética contribui com uma sociedade mais justa e sustentável.
		</p>
		<p>
			O grupo avança em uma estratégia de negócio focando seus investimentos no aumento da capacidade produtiva, utilizando as mais modernas tecnologias de reciclagem de fibras. Utilizamos aparas de papel pré e pós consumo, como matérias primas que são transformadas em fibras, gerando o papel Eco Millennium, do tipo offset, destinado à impressão e escrita. O Grupo Bignardi é hoje o maior fabricante brasileiro de papel reciclado para impressão e escrita.
		</p>
	</div>
</section>

@stop