@section ('conteudo')

<section>
	<div class="imagens">
		<img src="assets/images/empresa/placeholder400x600.jpg">
	</div>
	<div class="texto">
		<h1>PRODUTOS PERSONALIZADOS</h1>
		<h2>Um caderno com a sua marca</h2>
		<p>
			Com o consumidor cada vez mais exigente, empresas promovem sua marca através de brindes e promoções.
		</p>
		<p>
			Há mais de 10 anos, o Grupo Bignardi tem em seu leque de opções a possibilidade de personalizar a marca da sua empresa em cadernos, agendas, blocos de anotações, entre outros produtos de papelaria, com a qualidade e tecnologia que acompanham todos os produtos do Grupo. A personalização de produtos é uma tecnologia usada no mundo todo. Afinal, é uma grande alternativa para fixar a sua marca e presentear o seu cliente. Este ano o Grupo Bignardi adquiriu novas máquinas que viabilizaram, para pequenas e médias empresas, personalizar sua marca em quantidade menor a um custo acessível.
		</p>
		<p>
			Para atender a essa demanda, a empresa dispõe de uma equipe de profissionais altamente qualificados e prontos para esclarecer qualquer dúvida e/ou orientação. No mais, você terá a certeza e a segurança de uma linha exclusiva com a garantia de um Grupo que é referência no mercado papeleiro.
		</p>
		<p>
			Informações:<br>
			<a href="mailto:sac@jandaia.com" title="Entre em contato!">sac@jandaia.com</a><br>
			SAC: 0800-165656
		</p>
	</div>
</section>

@stop