<!DOCTYPE html>
<html lang="pt-BR" class="no-js">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="robots" content="index, follow" />
    <meta name="author" content="Trupe Design" />
    <meta name="copyright" content="2017 Trupe Design" />
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="favicon-jandaia.ico">

    <title>Jandaia Cadernos</title>
    <meta name="description" content="Jandaia é uma marca de cadernos, agendas, fichários, bolsas e outros produtos de papelaria.">
    <meta name="keywords" content="cadernos jandaia, cadernos, agendas, fichários, etiquetas, bolsas, blocos de papel, cadernos de anotações" />
    <meta property="og:title" content="Jandaia – Cadernos, agendas, fichários e outros produtos de papelaria"/>
    <meta property="og:site_name" content="Jandaia"/>
    <meta property="og:type" content="website"/>
    <meta property="og:image" content="{{ asset('assets/images/layout/marca-jandaia.jpg'); }}"/>
    <meta property="og:url" content="{{ Request::url() }}"/>
    <meta property="og:description" content="Jandaia é uma marca de cadernos, agendas, fichários, bolsas e outros produtos de papelaria."/>

    <meta property="fb:admins" content="100002297057504"/>
    <meta property="fb:app_id" content="580310192049536"/>

    <base href="{{ url() }}/">
    <script>var BASE = "{{ url() }}"</script>

    <link rel="stylesheet" href="{{ asset('assets/js/plugins/fancybox2/jquery.fancybox.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/layout2017/main.min.css') }}">
</head>

<body>
    @include('representantes2017._header')
    @yield('conteudo')
    @include('representantes2017._footer')

    <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-44518347-1']);
        _gaq.push(['_trackPageview']);
        (function() {
           var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
           ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
           (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(ga);
        })();
    </script>

    <script type="application/ld+json">
    { "@context" : "http://schema.org",
      "@type" : "Organization",
      "name" : "Cadernos Jandaia",
      "url" : "http://jandaia.com/",
      "logo": "http://jandaia.com/assets/images/layout/marca-jandaia.jpg",
      "sameAs" : [ "https://www.facebook.com/cadernosjandaia",
        "http://www.youtube.com/user/cadernosjandaia",
        "https://instagram.com/cadernosjandaia/",
        "https://plus.google.com/117280432477296799371"]
    }
    </script>

    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1&appId=580310192049536";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="{{ asset("assets/js/libs/jquery.min.js")  }}"><\/script>')</script>
    <script src="{{ asset('assets/js/plugins/fancybox2/jquery.fancybox.pack.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery.cycle2.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery.timelinr-0.9.53.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery.maskedinput.min.js') }}"></script>
    <script src="{{ asset('assets/js/scripts/representantes2017.js') }}"></script>
    <script src="{{ asset('assets/js/scripts/landing-educacao.js') }}"></script>
</body>
</html>
