<!DOCTYPE html>
<html lang="pt-BR" class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="robots" content="index, follow" />
    <meta name="author" content="Trupe Design" />
    <meta name="copyright" content="2013 Trupe Design" />
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link rel="shortcut icon" href="favicon-concurso.ico">
    <meta name="keywords" content="cadernos jandaia, concurso cultural #tôdentro, concurso jandaia, concurso xbox one" />

    @if(isset($seo) && $seo)
		<title>Jandaia - {{ trim(strip_tags($seo['title'])) }}</title>
		<meta name="description" content="{{ trim(strip_tags($seo['description'])) }}">
    	<meta property="og:description" content="{{ trim(strip_tags($seo['description'])) }}"/>
    	<meta property="og:title" content="Jandaia - {{ trim(strip_tags($seo['title'])) }}"/>
    	<meta property="og:image" content="{{ asset('assets/images/concurso/'.$seo['image']); }}"/>
    @endif

    <meta property="og:site_name" content="Concurso Cultural #TôDentro de um Mundo Melhor"/>
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="{{ Request::url() }}"/>
  	<meta property="fb:admins" content="100002297057504"/>
  	<meta property="fb:app_id" content="262328160587194"/>

	<base href="{{ url() }}/">
	<script>var BASE = "{{ url() }}"</script>
	
	<?=Assets::CSS(array(
		'reset',
		'pure/pure-css',
		'fancybox/fancybox_concurso',
		'concurso/base',
	))?>

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="js/jquery-1.10.2.min.js"><\/script>')</script>
	
	@if(App::environment()=='local')
		<?=Assets::JS(array('plugins/jquery-migrate-1.2.1', 'libs/modernizr-2.0.6.min', 'libs/less-1.3.0.min'))?>
	@else
		<?=Assets::JS(array('plugins/jquery-migrate-1.2.1', 'libs/modernizr-2.0.6.min'))?>
	@endif

</head>
<body>
	
	<!-- Facebook SDK -->
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1&appId=262328160587194";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>

	<!-- Twitter SDK -->
	<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>

	
	<!-- Conteúdo Principal -->
	@yield('conteudo')
	

	<?=Assets::JS(array(
		'plugins/fancybox',
		'plugins/jquery.maskedinput.min',
		'scripts/concurso'
	))?>
	
	
	<script type="text/javascript">
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-46406344-1']);
		_gaq.push(['_trackPageview']);
		(function() {
		   var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		   ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
		   (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(ga);
		})();
	</script>
	
</body>
</html>