<div class="regulamento">
	<p><strong>REGULAMENTO CONCURSO CULTURAL #todentro de um mundo melhor</strong></p>
	<p><strong>1. DO CONCURSO</strong></p>
	<p><strong>1.1</strong>. O concurso cultural denominado "#todentro de um mundo melhor" é promovido pelo Grupo Bignardi – Indústria e Comércio de Papéis e Artefatos Ltda., com sede na rodovia Presidente Tancredo de Almeida Neves, km 38,5, Jardim Vera Tereza, na Cidade de Caieiras, Estado de São Paulo, doravante designada, simplesmente, PROMOTORA.</p>
	<p><strong>1.2.</strong> O presente concurso é promovido em observância às determinações legais. A participação dos interessados é voluntária e gratuita, não estando condicionada em hipótese alguma à sorte, pagamento de preço e/ou compra de produtos, sendo, portanto, de caráter exclusivamente cultural/recreativo, de acordo com o disposto no artigo 30, do Decreto nº 70.951/72.</p>
	<p><strong>1.3.</strong> O concurso tem como finalidade incentivar a reflexão e produção cultural, elegendo dentre os participantes inscritos no hotsite exclusivo www.concursotodentro.com.br, as melhores ideias para o tema "#todentro de um mundo melhor".</p>
	<p><strong>1.4.</strong> O concurso está aberto à participação de quaisquer interessados, desde que pessoas físicas, residentes e domiciliadas no território nacional.</p>
	<p><strong>1.4.1.</strong> A participação de menores de idade neste concurso é de total responsabilidade dos pais ou responsáveis legais.</p>
	<p><strong>1.5.</strong> É vetado no presente concurso a participação dos sócios, diretores, administradores, funcionários da PROMOTORA e seus respectivos familiares, tal como qualquer pessoa que possua vínculo com a agência de propaganda e promoção responsável, bem como quaisquer outras empresas e parceiros envolvidos com o presente concurso cultural.</p>
	<p><strong>1.6.</strong> Este concurso ocorrerá no período compreendido entre as 11h do dia 12 de dezembro de 2013 (início das inscrições) até as 23h59 do dia 27 de fevereiro de 2014 e terá 3 (três) etapas de participação, conforme quadro abaixo:</p>
	
	<table>
		<thead>
			<tr>
				<th rowspan="2">ETAPA</th>
				<th rowspan="2">1.6.1 TEMAS</th>
				<th colspan="2">1.6.2 Inscrição</th>
				<th rowspan="2">1.6.3 Divulgação dos Ganhadores</th>
			</tr>
			<tr>
				<th>Início</th>
				<th>Término</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>I</td>
				<td>Meio Ambiente</td>
				<td>12/12/2013 - 11h00</td>
				<td>10/01/2014 - 23h59</td>
				<td>17/01/2014</td>
			</tr>
			<tr>
				<td>II</td>
				<td>Responsabilidade Social</td>
				<td>13/01/2014 - 11h00</td>
				<td>31/01/2014 - 23h59</td>
				<td>07/02/2014</td>
			</tr>
			<tr>
				<td>III</td>
				<td>Mobilidade Urbana</td>
				<td>03/02/2014 - 11h00</td>
				<td>21/02/2014 - 23h59</td>
				<td>27/02/2014</td>
			</tr>
		</tbody>
	</table>

<p>1.7. A cada etapa do concurso cultural, a PROMOTORA lançará o novo tema por meio de hotsite do concurso no endereço www.concursotodentro.com.br. </p>

<p><strong>2. DA PARTICIPAÇÃO NO CONCURSO</strong></p>
<p>2.1. Para participar, o interessado deverá acessar, por meio da internet, o hotsite do presente concurso no endereço www.concursotodentro.com.br, enviar um TEXTO de autoria própria relativo ao tema da etapa correspondente e preencher o formulário disponível na seção “Participe”, onde serão requisitados obrigatoriamente os seguintes dados: nome completo, e-mail, telefone, CEP, CPF e aceite das regras explicitadas neste regulamento.</p>
<p>2.1.1. Por TEXTO, entende-se a elaboração de conteúdo escrito em até 140 caracteres, incluindo a hashtag #todentro, submetido por meio de plataforma específica disponível no hotsite do concurso. Somente os TEXTOS que incluírem a hashtag #todentro serão considerados válidos.</p>
<p>2.1.2. Todos os campos obrigatórios devem ser preenchidos para que a inscrição seja aceita. </p>
<p>2.1.3. Caso o participante não possua CPF, deverá fornecer o número de seu responsável legal.</p>
<p>2.1.3.1. Cada CPF de responsável habilitará a participação de 1 (um) a 3 (três) dependentes.</p>
<p>2.1.4. Nenhum outro dado será solicitado, exceto aos premiados, após a divulgação, sendo essa solicitação realizada via e-mail.</p>
<p>2.2. Cada participante poderá inscrever 1 (um) TEXTO, dentro do prazo estipulado para cada etapa, conforme item 1.6 e desde que observadas as demais regras de participação estabelecidas neste regulamento, totalizando no máximo 3 (três) TEXTOS enviados até o término do concurso.</p>
<p>2.3. Após o envio completo, o TEXTO não poderá ser substituído ou alterado pelo participante. Desta forma, recomenda-se que seja verificado antes do envio. </p>
<p>2.4. Não serão aceitas participações por quaisquer outros meios que não pela página do concurso cultural “#todentro de um mundo melhor”.</p>
<p>2.5. Na hipótese de recebimento pela PROMOTORA de dois ou mais TEXTOS idênticos ou significativamente similares e/ou que, de algum modo, possam ser interpretados como cópia ou reprodução total ou parcial, quando encaminhados por participantes distintos, poderá ser considerado para efeito de inscrição somente a primeira sugestão recebida pela PROMOTORA, levando-se em conta a data e o horário de envio, ficando a critério da PROMOTORA desclassificar ou desconsiderar as demais para fins de julgamento e/ou premiação.</p>
<p>2.6. Os TEXTOS encaminhados não poderão conter qualquer conotação comercial, ainda que relacionadas ao nome da PROMOTORA, nem qualquer tipo de indução ao consumo ou utilização de qualquer bem, produto ou serviço, sob pena de o participante ser excluído sumariamente deste concurso, não cabendo a esta decisão qualquer espécie de recurso.</p>
<p>2.7. Os TEXTOS encaminhados também não poderão conter conotação contrária à moral, aos bons costumes ou à Legislação Federal, Estadual e/ou Municipal vigentes e não poderão incentivar a violência, tampouco poderão conter qualquer tipo de indicação de preconceito, difamação, injúria, calúnia, desrespeito ou discriminação racial, religiosa, sexual, social ou política, sob pena de o participante ser desclassificado deste concurso e ainda responder integralmente por qualquer dano porventura causado, não cabendo a esta decisão qualquer espécie de recurso.</p>
<p>2.8. O participante deste concurso declara, desde já, que qualquer TEXTO enviado para participação foi por ele elaborado, não contendo plágio, cópia ou outra forma de apropriação de qualquer direito de propriedade intelectual de terceiros, responsabilizando-se integral e unicamente por todas e quaisquer reclamações e ações recebidas pela PROMOTORA se infringida a Lei que regula os direitos autorais no Brasil.</p>
<p>2.9. Se identificado TEXTO que já constitua propriedade intelectual, inclusive direito autoral e respectivo direito patrimonial, que não seja de propriedade do participante ou que não tenha sido previamente autorizado por escrito, está o participante ciente, desde já, de que responderá, sozinho e integralmente, por todos e quaisquer danos causados pelas práticas desses atos, e sua inscrição será desclassificada. </p>
<p>2.10. O participante do presente concurso cultural, desde já, cede e transfere para a PROMOTORA, a título gratuito, os direitos de propriedade intelectual emergentes do TEXTO encaminhado, criado em decorrência da participação do concurso, incluídos entre outros, e não se limitando aos direitos autorais e conexos, para serem explorados em qualquer mídia e sem restrição de frequência e tempo de exposição, podendo esta utilizá-los da forma como melhor lhe convier. Fica assegurado ao autor do TEXTO o direito de mencioná-lo sempre que necessário na condição de autor, para fins de divulgação ou comprovação de autoria.</p>
<p>2.11. Serão desconsideradas as participações realizadas de maneira incorreta, ou seja, incompletas, anônimas ou com dados falsos, confusos ou, ainda, se houver a constatação de formulários idênticos. Da mesma maneira, fica invalidada a participação do TEXTO que já tenha sido inscrito, publicado ou premiado em outro evento.</p>
<p>2.12. A participação neste concurso cultural é gratuita, não sendo vinculada a qualquer modalidade de sorte ou obrigatoriedade de aquisição de produtos, bens, serviços, etc., condicionando-se esta, tão somente, à aceitação de todos os itens deste regulamento.</p>
<p>2.13. O envio dos TEXTOS pode ser feito até o dia 21 de fevereiro de 2014, até as 23h59, tendo por base o horário oficial de Brasília, respeitando os temas e as datas de início e término de cada etapa conforme item 1.6.2 deste regulamento.</p>

<p><strong>3. DA APURAÇÃO, DOS PRÊMIOS E DA ENTREGA</strong></p>
<p>3.1. Haverá a moderação automática de conteúdo, que inclui: validar a inclusão da hashtag “#todentro” e invalidar a inscrição no caso de utilização de palavras indevidas ao regulamento do concurso. </p>
<p>3.2. Ao término do período de inscrição de cada etapa, a PROMOTORA irá, segundo os seus próprios critérios, escolher os 3 (três) melhores TEXTOS. Os autores serão contemplados com os prêmios descritos no item 3.3 deste regulamento.</p>
<p>3.2.1. Ao final das 3 (três) etapas do concurso, haverá um total de 9 (nove) premiados.</p>
<p>3.3. Os autores dos melhores TEXTOS em cada etapa serão contemplados com os respectivos prêmios:</p>
<ul>
	<li>Primeiro colocado: 1 (um) Xbox One </li>
	<li>Segundo colocado: 1 (um) Tablet Samsung Galaxy 7“ – Branco - 8GB</li>
	<li>Terceiro colocado: 1 (um) Short Skate</li>
</ul>
<p>3.4. Os prêmios serão entregues de forma gratuita e sem ônus, no domicílio do contemplado, no prazo máximo de 90 (noventa) dias, a contar da data de divulgação do resultado.</p>	
<p>3.5. O resultado com os nomes dos contemplados neste concurso será divulgado pelo hotsite do concurso cultural e pelas redes sociais da PROMOTORA, e os ganhadores serão avisados dessa condição por meio de e-mail enviado para o endereço eletrônico indicado no ato de sua inscrição, no qual serão solicitados, para confirmação, os dados pessoais (nome completo, RG, CPF, endereço completo e telefone), conforme necessário, para que possam receber seus prêmios. </p>	
<p>3.5.1. Os TEXTOS premiados poderão ser divulgados pela PROMOTORA, porém, não existe qualquer obrigação da PROMOTORA em divulgar os TEXTOS encaminhados para participação no concurso cultural.</p>	
<p>3.6. Na eventualidade de o contemplado, por algum motivo, não puder ser contatado por qualquer um dos meios descritos no item 3.5, ou se contatado, não se manifestar com a confirmação dos dados pessoais em até 30 (trinta) dias após a comunicação da PROMOTORA, ocorrerá a perda do direito à premiação.</p>	
<p>3.7. É imprescindível que todos os contemplados apresentem documento de identificação e assinem o "Termo de Quitação e Entrega de Prêmio” para que possam receber seus respectivos prêmios. </p>	
<p>3.7.1. Na eventualidade de o contemplado ser menor de idade, o responsável deverá receber o prêmio em nome do menor e, para tanto, deverá comprovar tal condição mediante a apresentação de documento.</p>	
<p>3.8. Os prêmios são pessoais e intransferíveis, não sendo permitida a sua troca por dinheiro ou por qualquer outro produto antes de sua entrega.</p>	
<p>3.9. Após a entrega do prêmio, ficará sob responsabilidade do vencedor todos os gastos, custos e ônus relativos ao bem, produto, direito ou serviço adquirido com a premiação.</p>	
<p>3.10. Reclamações como, por exemplo, avarias, vícios, defeitos, insatisfações ou danos de qualquer ordem e garantias concernentes à premiação devem ser dirigidas diretamente à empresa fabricante do produto, não cabendo nenhuma responsabilidade à PROMOTORA com relação a esses aspectos.</p>	

<p><strong>4. DISPOSIÇÕES GERAIS</strong></p>
<p>4.1. A divulgação deste concurso poderá ser realizada por meio de sites, redes sociais, canais de televisão ou qualquer outro meio disponível e necessário nos momentos que antecederem a realização do concurso, ou durante a realização deste.</p>
<p>4.2. A PROMOTORA reserva-se o direito de desclassificar e excluir do concurso qualquer participante que julgue violar as condições e os termos dispostos neste regulamento, a seu livre e exclusivo critério, em especial e sem limitar-se a: utilização da página deste concurso de forma contrária à legislação em vigor, mediante comprovado desrespeito a direitos de terceiros, aos bons costumes, à ética, à moral e/ou de forma fraudulenta.</p>
<p>4.3. Em caso de fraude, o participante será automaticamente excluído do concurso pela PROMOTORA, cuja deliberação será soberana e irrecorrível.</p>
<p>4.3.1. Poderá ser considerada fraude, a critério da PROMOTORA, sem prejuízo de outras, (I) a tentativa de direcionamento do prêmio decorrente de artifício utilizado pelo usuário como, por exemplo, o uso de TEXTO que não seja de sua autoria; (II) e/ou uso não autorizado de identificação de terceiros; (III) e/ou duplicidade de participação; (IV) e/ou quaisquer outros mecanismos que venham a ser utilizados por participantes que desrespeitem os critérios deste regulamento.</p>
<p>4.4. A qualquer instante, a PROMOTORA poderá solicitar documentos comprobatórios dos participantes, averiguando a veracidade das informações prestadas neste concurso, desclassificando sumariamente aqueles que prestarem quaisquer informações falsas, não cabendo a estes qualquer recurso contra referidas decisões, podendo ainda a PROMOTORA adotar contra estes todas as medidas judiciais cíveis e criminais pertinentes.</p>
<p>4.5. Todos os participantes autorizam, sem contrapartida financeira, a PROMOTORA a utilizar-se de seus nomes, imagens, TEXTOS e som de voz em qualquer mídia impressa, eletrônica ou digital, escolhidos a critério da PROMOTORA para divulgação deste concurso e/ou outras ações de suas empresas, sem restrição de frequência e pelo prazo de cinco anos contados da publicação deste regulamento.</p>
<p>4.6. Para saber mais informações sobre o concurso cultural, o participante deverá entrar em contato com a PROMOTORA através do e-mail sac@jandaia.com.</p>
<p>4.7. Casos omissos e dúvidas geradas durante a realização do concurso cultural serão avaliados e julgados por uma comissão composta por membros da PROMOTORA, sendo suas decisões soberanas e irrecorríveis, não cabendo a interposição de recursos.</p>
<p>4.8. As dúvidas e controvérsias originadas de reclamações dos participantes deste concurso cultural deverão ser preliminarmente dirimidas pela PROMOTORA, quando o participante não optar pela reclamação direta aos órgãos integrantes do Sistema Nacional de Defesa do Consumidor.</p>
<p>4.9. Em momento algum a PROMOTORA poderá ser responsabilizada por inscrições perdidas, atrasadas, enviadas erroneamente, incompletas, incorretas, inválidas, imprecisas e/ou que não estejam de acordo com o disposto neste regulamento.</p>
<p>4.10. A PROMOTORA não se responsabiliza pelo funcionamento, por problemas, ou por falhas técnicas, de qualquer tipo como, por exemplo, em redes de computadores, servidores ou provedores, equipamentos de computadores, hardware ou software, ou erro, interrupção, defeito, atraso ou falha em operações ou transmissões para o correto processamento de inscrições, incluindo, mas não se limitando, a transmissão imprecisa de inscrições ou falha da PROMOTORA em recebê-las, em razão de problemas técnicos, congestionamento na internet ou no portal ligado ao concurso, vírus, falha de programação (bugs) ou violação por terceiros (crackers), ou qualquer caso fortuito ou de força maior que possam impedir a participação do usuário.</p>
<p>4.11. A PROMOTORA reserva-se o direito de cancelar, suspender ou modificar o concurso cultural ou seu regulamento, sem aviso prévio aos participantes, caso ocorram fraudes, dificuldades técnicas ou qualquer outro imprevisto que esteja fora do seu controle e comprometa a integridade do concurso cultural, de forma que não possa ser conduzido como originalmente planejado. Nesses casos, não será devida qualquer tipo de indenização pela PROMOTORA aos participantes.</p>
<p>4.12. A simples participação neste concurso implica na aceitação total e irrestrita de todos os itens, além de pressupor total conhecimento e concordância com as disposições deste regulamento, por parte do participante.</p>
<p>4.13. Este regulamento completo será divulgado em www.concursotodentro.com.br.</p>
<p>4.14. Fica, desde já, eleito o foro central da comarca de São Paulo para solucionar quaisquer questões referentes ao presente concurso.</p>

</div>