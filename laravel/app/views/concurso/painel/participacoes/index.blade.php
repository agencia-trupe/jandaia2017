@section('conteudo')

<div class="container">

	@if(Session::has('sucesso'))
	  <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
  @endif

	@if(Session::has('falha'))
		<div class="alert alert-block alert-error fade in"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('falha') }}</div>
	@endif

  	<div class="page-header users-header">
    	<h2>
          @if($etapa == 0)
      		  Participações - Todas as Etapas
          @else
            Participações - Etapa {{ $etapa }}
          @endif
    	</h2>
  	</div>
  
    <div class="page-header">
      <h5>Filtrar por Etapa</h5>
      <div class="btn-group">
        <a href="{{ URL::route('concurso.painel.participacoes', 1) }}" title="Participações da Etapa 1" class="btn @if($etapa == 1) btn-info @else btn-default @endif">Etapa 1</a>
        <a href="{{ URL::route('concurso.painel.participacoes', 2) }}" title="Participações da Etapa 2" class="btn @if($etapa == 2) btn-info @else btn-default @endif">Etapa 2</a>
        <a href="{{ URL::route('concurso.painel.participacoes', 3) }}" title="Participações da Etapa 3" class="btn @if($etapa == 3) btn-info @else btn-default @endif">Etapa 3</a>
      </div>
    </div>

    @if(count($registros) > 0)

      <div class="page-header">
        <h5>Download destas Participações</h5>
        @if($etapa == 0)
          <a href="painel/download" title="Download do arquivo Excel com as Participações" class="btn btn-success"><i class="icon-white icon-download"></i> download</a>
        @else
          <a href="painel/download/{{$etapa}}" title="Download do arquivo Excel com as Participações" class="btn btn-success"><i class="icon-white icon-download"></i> download</a>
        @endif
      </div>

    	<div class="row">
      	<div class="span12 columns">
        		<table class="table table-striped table-bordered table-condensed">

            		<thead>
              		<tr>
                			<th>Nome</th>
                			<th>E-mail</th>
                      <th>Frase</th>
                      <th>Data de Cadastro</th>
                			<th><i class="icon-cog"></i></th>
              		</tr>
            		</thead>

            		<tbody>
              	@foreach ($registros as $p)

                  	<tr class="tr-row">
                    		<td>{{ $p->nome_completo }}</td>
                    		<td>{{ $p->email }}</td>
                        <td>{{ $p->frase }}</td>
                        <td>{{ Tools::converteData($p->data_cadastro) }}</td>
                    		<td class="crud-actions">
                      		<a href="{{ URL::route('concurso.painel.participacoes.detalhes', $p->id) }}" class="btn btn-primary">visualizar</a>                    	   
                    		</td>
                  	</tr>

              	@endforeach
            		</tbody>

          	</table>
      	</div>
    	</div>

    @else

      <h2>Nenhum participação até o momento</h2>

    @endif

@stop