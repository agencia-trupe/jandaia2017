@section('conteudo')

<div class="container">

  	<div class="page-header users-header">
    	<h2>Detalhes da participação</h2>
  	</div>
	
	<h4 class="visualizacao-concurso">Frase</h4>
	<p>{{ $registro->frase }}</p>

  	<h4 class="visualizacao-concurso">Nome Completo</h4>
  	<p>{{ $registro->nome_completo }}</p>

  	<h4 class="visualizacao-concurso">E-mail</h4>
  	<p>{{ $registro->email }}</p>

  	<h4 class="visualizacao-concurso">Telefone</h4>
  	<p>{{ $registro->telefone }}</p>

  	<h4 class="visualizacao-concurso">CEP</h4>
  	<p>{{ $registro->cep }}</p>

	<h4 class="visualizacao-concurso">CPF</h4>
  	<p>{{ $registro->cpf }}</p>	

  	<h4 class="visualizacao-concurso">Nome do Responsável</h4>
  	<p>{{ $registro->nome_responsavel ?: 'n/a' }}</p>

  	<h4 class="visualizacao-concurso">CPF do Responsável</h4>
  	<p>{{ $registro->cpf_responsavel ?: 'n/a' }}</p>

  	<h4 class="visualizacao-concurso">Data de Nascimento</h4>
  	<p>{{ Tools::converteData($registro->data_nascimento) != '00/00/0000' ? Tools::converteData($registro->data_nascimento) : 'n/a' }}</p>
	
	<h4 class="visualizacao-concurso">Quero receber mais promoções e informações sobre a Jandaia</h4>
	@if($registro->newsletter == 1)
		<p>Sim</p>
	@else
		<p>Não</p>
	@endif
	
	<h4 class="visualizacao-concurso">Data de Cadastro</h4>
	<p>{{ Tools::converteData($registro->data_cadastro) }}</p>

	<div class="form-actions">
    	{{ Form::button('Voltar', array('class' => 'btn btn-voltar')) }}
  	</div>

</div>

@stop