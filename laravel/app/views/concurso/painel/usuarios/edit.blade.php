@section('conteudo')

<div class="container">

  	<div class="page-header users-header">
    	<h2>
      		Alterar Usuário
    	</h2>
  	</div>

  	<div class="row">
    	<div class="span12 columns">

			{{ Form::open( array('route' => array('painel.usuarios.update', $usuario->id), 'method' => 'put', 'id' => 'form-edit-usuario') ) }}

			<label>Usuário<br>
			<input type="text" name="username" required autofocus value="{{ $usuario->username }}" class="input-xxlarge"></label>

			<label>E-mail<br>
			<input type="email" name="email" required value="{{ $usuario->email }}" class="input-xxlarge"></label>

			<label>Senha<br>
			<input type="password" name="password" id="senha"></label>

			<label>Confirmar Senha<br>
			<input type="password" id="conf-senha"></label>

			<div class="form-actions">
	        	{{ Form::submit('Alterar', array('class' => 'btn btn-primary')) }}
	        	{{ Form::button('Voltar', array('class' => 'btn btn-voltar')) }}
	      	</div>
		{{ Form::close() }}

		</div>
	</div>

@stop