@section('conteudo')

    <div class="contato">
        <div class="center">
            <div class="info">
                <h2>FALE CONOSCO</h2>
                <p>
                    <a href="mailto:sac@jandaia.com" title="Entrar em contato">sac@jandaia.com</a>
                    SAC 0800 165656
                </p>
            </div>

            <form action="{{ URL::route('representantes.contato.enviar') }}" method="post" id="form-contato">
                @if(Session::has('envio'))
                    @if(Session::get('envio'))
                        <div class="resposta sucesso">
                            Email de contato enviado com sucesso.<br>
                            Agradecemos o contato e responderemos assim que possível.
                        </div>
                    @else
                        <div class="resposta falha">
                            Erro ao enviar o email.<br>
                            Verifique se todos os campos obrigatórios foram preenchidos.
                        </div>
                    @endif
                @endif

                <input type="text" name="nome" placeholder="nome" required id="input-nome">
                <input type="email" name="email" placeholder="e-mail" required id="input-email">
                <input type="text" name="telefone" placeholder="telefone">
                <textarea name="mensagem" placeholder="mensagem" required id="input-mensagem"></textarea>
                <input type="submit" value="ENVIAR">
            </form>
        </div>
    </div>

@stop
