<footer>
    <div class="colunas">
        <div class="center">
            <div class="representante">
                <p>ÁREA DO REPRESENTANTE</p>
                {{--<a href="http://jandaia.com/catalogo-virtual/" target="_blank">Orçamento Virtual</a>--}}
                <a href="{{ route('representantes.representantes') }}">Área do Lojista</a>
                <a href="http://fvendas.bignardi.com.br/fvweb/" target="_blank">Área do Representante</a>
            </div>
            <div class="links">
                <a href="{{ route('representantes.home') }}">Home</a>
            </div>
            <div class="links">
                <a href="{{ route('representantes.produtos') }}">Produtos</a>
                <a href="{{ route('representantes.produtos', 'agendas') }}" class="sub">&raquo; Agendas</a>
                <a href="{{ route('representantes.produtos', 'cadernos') }}" class="sub">&raquo; Cadernos</a>
                <a href="{{ route('representantes.produtos', 'gifts') }}" class="sub">&raquo; Gifts</a>
                <a href="{{ route('representantes.produtos', 'bolsas-e-ficharios') }}" class="sub">&raquo; Bolsas e Fichários</a>
                <a href="{{ route('representantes.produtos', 'papeis-e-escritorio') }}" class="sub">&raquo; Papéis e Escritório</a>
            </div>
            <div class="links">
                <a href="{{ route('representantes.encontrar') }}">Onde Encontrar</a>
                <a href="{{ route('representantes.encontrar') }}" class="sub">&raquo; Encontre os nossos produtos</a>
            </div>
            <div class="links">
                <a href="{{ route('representantes.jandaia.empresa') }}">A Jandaia</a>
                <a href="{{ route('representantes.jandaia.empresa') }}" class="sub">&raquo; Empresa e serviços</a>
                <a href="{{ route('representantes.jandaia.certificacoes') }}" class="sub">&raquo; Certificações</a>
                <a href="{{ route('representantes.jandaia.sustentabilidade') }}" class="sub">&raquo; Sustentabilidade</a>
                <a href="{{ route('representantes.jandaia.lab') }}" class="sub">&raquo; Jandaia Lab</a>
                <a href="{{ route('representantes.jandaia.exportacao') }}" class="sub">&raquo; Exportação</a>
            </div>
            {{--
            <div class="links">
                <a href="#">Blog</a>
                <a href="#" class="sub">&raquo; Última chamada registrada</a>
                <a href="#" class="sub">&raquo; Penúltima chamada registrada no Fique por Dentro</a>
            </div>
            --}}
            <div class="contato">
                <a href="{{ route('representantes.contato') }}">Contato</a>
                <p>SAC: 0800 165 656</p>
                <a href="mailto:sac@jandaia.com" class="email">sac@jandaia.com</a>
                <a href="{{ route('representantes.contato') }}" class="sub">&raquo; Fale conosco</a>
                {{--<a href="#" class="sub">&raquo; FAQ</a>--}}
                <div class="social">
                    <a href="https://www.facebook.com/cadernosjandaia" class="facebook" target="_blank">facebook</a>
                    <a href="https://instagram.com/cadernosjandaia/" class="instagram" target="_blank">instagram</a>
                    <a href="http://www.youtube.com/user/cadernosjandaia" class="youtube" target="_blank">youtube</a>
                </div>
            </div>
            <div class="marca">
                <img src="{{ asset('assets/images/layout2017/marca-jandaia-rodape.png') }}" alt="">
                <p>A Jandaia é uma empresa do:</p>
                <a href="http://www.bignardi.com.br/">
                    <img src="{{ asset('assets/images/layout2017/marca-grupobignardi.png') }}" alt="">
                </a>
            </div>
        </div>
    </div>
    <div class="copyright">
        <div class="center">
            <p>
                © {{ date('Y') }} Jandaia | Grupo Bignardi - Todos os direitos reservados.
                    <span>||</span>
                    <a href="http://www.trupe.net" target="_blank">Criação de sites</a>:
                    <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
            </p>
        </div>
    </div>
</footer>
