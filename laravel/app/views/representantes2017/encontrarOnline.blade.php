@section('conteudo')

<div class="encontrar">
	<div class="center">
		<h1>ONDE ENCONTRAR OS PRODUTOS JANDAIA</h1>
		<h2>Localize a loja mais próxima onde você pode encontrar os Produtos Jandaia!</h2>

		<form action="{{ URL::route('representantes.encontrar.buscar') }}" method="get" id="form-encontrar">
			<div class="row">
				<label>Selecione a linha de produtos Jandaia que você deseja:</label>
				<select name="linha" required>
					<option value=""></option>
					@if($listaLinhas)
						@foreach($listaLinhas as $k => $v)
							<option value="{{$v->slug}}" @if(isset($objLinha) && !is_null($objLinha) && $objLinha->slug == $v->slug) selected @endif>{{ $v->titulo }}</option>
						@endforeach
					@endif
				</select>
			</div>
			<div class="row">
				<label>Selecione o tipo de produto:</label>
				<select name="tipo" required>
					<option value=""></option>
					@if($listaTipos)
						@foreach($listaTipos as $k => $v)
							<option value="{{$v->slug}}" @if(isset($objTipo) && !is_null($objTipo) && $objTipo->slug == $v->slug) selected @endif>{{ $v->titulo }}</option>
						@endforeach
					@endif
				</select>
			</div>
			<div class="row">
				<label>Indique o Estado, Cidade e o CEP de onde deseja encontrar a loja mais próxima:</label>
				<select name="estado" id="sel-estado" required>
					<option value="">Estado - Selecione (obrigatório)</option>
					@if($listaEstados)
						@foreach($listaEstados as $k => $v)
							<option value="{{$v->id}}" @if(isset($objEstado) && !is_null($objEstado) && $objEstado->id == $v->id) selected @endif>{{$v->nome}}</option>
						@endforeach
					@endif
				</select>
			</div>
			<div class="row">
				<select name="cidade" id="sel-cidade" @if(isset($objCidade) && !is_null($objCidade)) data-cidade="{{$objCidade->id}}" @endif>
					<option value="">Cidade - Selecione</option>
				</select>
			</div>
			<div class="row">
				<input type="text" name="cep" placeholder="CEP" id="input-cep" @if(isset($cepInformado)) value="{{$cepInformado}}" @endif >
			</div>
			<div class="row">
				<input type="submit" value="CONSULTAR">
			</div>
		</form>

		<a href="{{ URL::route('representantes.encontrar.online') }}" class="botao-online">
			<p>COMPRE ONLINE!</p>
			<span>SITES QUE VENDEM NOSSOS PRODUTOS &raquo;</span>
		</a>

		@if(isset($resultados))
		<div class="encontrar-resultados-online">
			<h2>RESULTADO DA BUSCA DE LOJAS VIRTUAIS</h2>
			@if(sizeof($resultados) > 0)
				<div>
					@foreach($resultados as $k => $v)
					<a href="{{$v->link}}" target="_blank" title="{{$v->titulo}}">
						<div class="imagem">
							<img src="assets/images/consumidor/lojas/{{$v->imagem}}" alt="{{$v->titulo}}">
						</div>
						<p>{{$v->titulo}}</p>
					</a>
					@endforeach
				</div>
			@else
				<h3>NENHUMA LOJA VIRTUAL ENCONTRADA</h3>
			@endif
		</div>
		@endif
	</div>
</div>

@stop
