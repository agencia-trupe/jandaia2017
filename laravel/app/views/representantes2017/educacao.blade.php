@extends('templates.representantes2017')

@section('conteudo')

    <div class="landing-educacao">
        <div class="landing-educacao-banners">
            <img src="{{ asset('assets/images/landing-educacao/banner-menino.png') }}" alt="">
            <img src="{{ asset('assets/images/landing-educacao/banner-menina.png') }}" alt="">
            <div class="cycle-pager"></div>
        </div>

        <div class="landing-educacao-sections">
            <section class="section-1 section-branco">
                <div class="section-center">
                    <img src="{{ asset('assets/images/landing-educacao/lampada.png') }}" alt="">
                    <h2>SE LIGA NESSAS DICAS<br>PRA APROVEITAR O ANO<br>INTEIRO SEM PERDER<br>O PIQUE:</h2>
                    <p>Dê aquele gás<br>aprendendo<br>coisas novas.</p>
                    <div class="fio"></div>
                    <div class="fio"></div>
                </div>
            </section>
            <section class="section-2 section-azul">
                <div class="section-center">
                    <img src="{{ asset('assets/images/landing-educacao/computador.png') }}" alt="">
                    <ul>
                        <li>
                            <span>1/</span>
                            <a href="http://www.cousera.org" target="_blank">cousera.org</a>
                        </li>
                        <li>
                            <span>2/</span>
                            <a href="http://www.eduk.com.br" target="_blank">eduk.com.br</a>
                        </li>
                        <li>
                            <span>3/</span>
                            <a href="http://www.veduca.com.br" target="_blank">veduca.com.br</a>
                        </li>
                        <li>
                            <span>4/</span>
                            <a href="http://www.creativelive.com" target="_blank">creativelive.com</a>
                        </li>
                        <li>
                            <span>5/</span>
                            <a href="http://www.duolingo.com" target="_blank">duolingo.com</a>
                        </li>
                        <li>
                            <span>6/</span>
                            <a href="http://www.babel.com" target="_blank">babel.com</a>
                        </li>
                        <li>
                            <span>7/</span>
                            <a href="http://www.usachess.com" target="_blank">usachess.com</a>
                        </li>
                    </ul>
                    <div class="fio"></div>
                </div>
            </section>
            <section class="section-3 section-branco">
                <div class="section-center">
                    <h2>E PRA NÃO FICAR<br>PERDIDÃO, ENCONTRE<br>SEU RUMO.</h2>
                    <img src="{{ asset('assets/images/landing-educacao/seta.png') }}" alt="">
                    <ul>
                        <li>
                            <span>1/</span>
                            <a href="http://vestibular.uol.com.br" target="_blank">vestibular.uol.com.br</a>
                        </li>
                        <li>
                            <span>2/</span>
                            <a href="http://www.nube.com.br/teste-vocacional" target="_blank">nube.com.br/teste-vocacional</a>
                        </li>
                        <li>
                            <span>3/</span>
                            <a href="http://guiadoestudante.abril.com.br/profissoes" target="_blank">guiadoestudante.abril.com.br/profissões</a>
                        </li>
                        <li>
                            <span>4/</span>
                            <a href="http://www.mundovestibular.com.br/pages/teste_vocacional" target="_blank">mundovestibular.com.br/teste_vocacional</a>
                        </li>
                    </ul>
                    <div class="fio"></div>
                </div>
            </section>
            <section class="section-4 section-amarelo">
                <div class="section-center">
                    <img src="{{ asset('assets/images/landing-educacao/maozinha.png') }}" alt="">
                    <h2>MAS TAMBÉM É<br>BOM MOSCAR<br>NA WEB.</h2>
                    <ul>
                        <li>
                            <span>1/</span>
                            <a href="https://www.netflix.com/br/" target="_blank">Netflix</a>
                        </li>
                        <li>
                            <span>2/</span>
                            <a href="https://omelete.com.br/" target="_blank">Omelete</a>
                        </li>
                        <li>
                            <span>3/</span>
                            <a href="https://www.365filmes.com.br/" target="_blank">365 filmes</a>
                        </li>
                        <li>
                            <span>4/</span>
                            <a href="http://www.claquete.com/" target="_blank">Claquete</a>
                        </li>
                        <li>
                            <span>5/</span>
                            <a href="http://www.hypeness.com.br/" target="_blank">Hypeness</a>
                        </li>
                        <li>
                            <span>6/</span>
                            <a href="https://www.vevo.com/" target="_blank">Vevo</a>
                        </li>
                        <li>
                            <span>7/</span>
                            <a href="https://www.youtube.com/?hl=pt&gl=BR" target="_blank">Youtube</a>
                        </li>
                        <li>
                            <span>8/</span>
                            <a href="https://seriemaniacos.tv/" target="_blank">Seriemaniacos.tv</a>
                        </li>
                        <li>
                            <span>9/</span>
                            <a href="http://www.minhaserie.com.br/" target="_blank">Minhaserie.com.br</a>
                        </li>
                    </ul>
                    <div class="fio"></div>
                </div>
            </section>
            <section class="section-5 section-branco">
                <div class="section-center">
                    <h2>VIAJAR NUMA<br>LEITURA DAORA.</h2>
                    <ul>
                        <li>
                            <span>1/</span>
                            O apanhador no campo de centeio
                        </li>
                        <li>
                            <span>2/</span>
                            Eugênia e os robôs
                        </li>
                        <li>
                            <span>3/</span>
                            Feras de lugar nenhum
                        </li>
                        <li>
                            <span>4/</span>
                            Cinderela mudou de ideia
                        </li>
                        <li>
                            <span>5/</span>
                            Caninos brancos
                        </li>
                        <li>
                            <span>6/</span>
                            Começo, meio e fim
                        </li>
                        <li>
                            <span>7/</span>
                            Andróides sonham com ovelhas elétricas
                        </li>
                    </ul>
                    <img src="{{ asset('assets/images/landing-educacao/livro.png') }}" alt="">
                    <div class="fio"></div>
                </div>
            </section>
            <section class="section-6 section-azul">
                <div class="section-center">
                    <h2>DESCOBRIR AQUELA<br>SONZEIRA ANTES<br>DE TODO MUNDO.</h2>
                    <ul>
                        <li>
                            <span>1/</span>
                            <a href="https://www.albumoftheyear.org/" target="_blank">Album of the year</a>
                        </li>
                        <li>
                            <span>2/</span>
                            <a href="https://www.setlist.fm/" target="_blank">Setlist.fm</a>
                        </li>
                        <li>
                            <span>3/</span>
                            <a href="http://inflooenz.com/" target="_blank">Inflooenz</a>
                        </li>
                        <li>
                            <span>4/</span>
                            <a href="http://www.spotify.com/?" target="_blank">Spotify - monte aquela<br>playlist colaborativa com<br>os amigos</a>
                        </li>
                    </ul>
                    <img src="{{ asset('assets/images/landing-educacao/fone.png') }}" alt="">
                    <div class="fio"></div>
                </div>
            </section>
            <section class="section-7 section-branco">
                <div class="section-center">
                    <img src="{{ asset('assets/images/landing-educacao/baloes.png') }}" alt="">
                    <h2>SE LIGAR NUMAS<br>CONVERSAS BEM<br>LOUCAS. PODCASTS:</h2>
                    <ul>
                        <li>
                            <span>1/</span>
                            <a href="https://jovemnerd.com.br/nerdcast/" target="_blank">NerdCast</a>
                        </li>
                        <li>
                            <span>2/</span>
                            <a href="https://player.fm/series/hyperlink" target="_blank">Hyperlink</a>
                        </li>
                        <li>
                            <span>3/</span>
                            <a href="http://www.onomedissoemundo.com/" target="_blank">O Nome Disso é Mundo</a>
                        </li>
                        <li>
                            <span>4/</span>
                            <a href="https://www.naosalvo.com.br/podcasts/naoouvo/" target="_blank">Não Ouvo</a>
                        </li>
                        <li>
                            <span>5/</span>
                            <a href="http://www.b9.com.br/podcasts/mamilos/" target="_blank">Mamilos</a>
                        </li>
                        <li>
                            <span>6/</span>
                            <a href="http://www.redegeek.com.br/podcast/" target="_blank">Ultrageek</a>
                        </li>
                        <li>
                            <span>7/</span>
                            <a href="https://xadrezverbal.com/" target="_blank">Xadrez Verbal</a>
                        </li>
                    </ul>
                    <div class="fio"></div>
                </div>
            </section>
            <section class="section-8 section-amarelo">
                <div class="section-center">
                    <h2>E CURTIR AS COISAS<br>BOAS FORA DA<br>INTERNET.</h2>
                    <div class="texto-1">
                        <img src="{{ asset('assets/images/landing-educacao/aviao.png') }}" alt="">
                        <p>Expresse-se. Invista um tempinho em cursos de teatro, oratória, grupos de dança e afins.</p>
                        <p>Tudo isso ajuda a quebrar um pouco da barreira da timidez e te preparam pra encarar o mundo com muito mais confiança.</p>
                    </div>
                    <h2>MAS FIQUE<br>SEMPRE ALERTA.</h2>
                    <div class="texto-2">
                        <img src="{{ asset('assets/images/landing-educacao/lupa.png') }}" alt="">
                        <p>Não acate atos intimidadores ou violentos contra indivíduos ou grupos, bullying é coisa séria e provoca sérios danos psicológicos. Denuncie.</p>
                        <p>Cuidado com seus dados pessoais na internet, existem muitos perfis falsos na rede, fique atento.</p>
                    </div>
                    <div class="fio"></div>
                </div>
            </section>
        </div>
    </div>

@stop
