@section ('conteudo')

    <div class="jandaia sustentabilidade">
        <div class="center">
            @include('representantes2017.jandaia._nav')

            <div class="coluna-direita">
                <h1 class="titulo">PAPEL<br> SOCIAL E<br> RESPONSÁVEL</h1>
                <div class="texto">
                    {{$conteudo->texto}}
                </div>
            </div>

            <div class="imagem">
                <img src="assets/images/layout2017/img-sustentabilidade.png" alt="">
                @if($conteudo->imagem_maior)
                <img src="assets/images/institucional/{{$conteudo->imagem_maior}}" alt="{{$conteudo->titulo}}" class="img-area">
                @endif
            </div>
        </div>
    </div>

@stop
