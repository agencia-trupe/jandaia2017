@section ('conteudo')

    <div class="jandaia exportacao">
        <div class="center">
            @include('representantes2017.jandaia._nav')

            <div class="coluna-direita">
                <h1 class="titulo">MAIOR <br>EXPORTADORA <br>DE CADERNOS <br>DO BRASIL</h1>
                <div class="texto">
                    {{$conteudo->texto}}
                </div>
            </div>

            <div class="exportacao-colunas">
                <div class="downloads">
                    <p>Saiba mais:</p>
                    <a href="download/presentacion-jandaia.pdf" title="DOWNLOAD Apresentação do Grupo Bignardi [espanhol]" class="download-btn">
                        <strong>DOWNLOAD</strong>
                        Apresentação do Grupo Bignardi [espanhol]
                    </a>
                    <a href="download/presentation-jandaia.pdf" title="DOWNLOAD Apresentação do Grupo Bignardi [inglês]" class="download-btn">
                        <strong>DOWNLOAD</strong>
                        Apresentação do Grupo Bignardi [inglês]
                    </a>
                    <a href="download/CATALOGO_VA_2017.pdf" title="DOWNLOAD Catálogo de Produtos 2016" class="download-btn">
                        <strong>DOWNLOAD</strong>
                        Catálogo de Produtos 2017
                    </a>
                    <a href="http://issuu.com/thiagodias51/docs/catalogo_va_2017-low/1" title="VISUALIZAÇÃO Visualize o Catálogo Online" target="_blank" class="visualize-btn">
                        <strong>VISUALIZAÇÃO</strong>
                        Visualize o Catálogo Online
                    </a>
                </div>

                <div class="videos">
                    <a href="http://www.youtube.com/embed/q9ix0g9km8Q?wmode=opaque&autoplay=1" title="Institucional">
                        <img src="assets/images/empresa/exportacoes/video-thumb1-institucional.png" alt="Institucional">
                        <div class="texto">Institucional do Grupo</div>
                    </a>
                    <a href="http://www.youtube.com/embed/jWgGx2vl33s?wmode=opaque&autoplay=1" title="Bignardi Papéis">
                        <img src="assets/images/empresa/exportacoes/video-thumb2-bignardipapeis.png" alt="Bignardi Papéis">
                        <div class="texto">Bignardi Papéis</div>
                    </a>
                    <a href="http://www.youtube.com/embed/DWIHCx6uajA?wmode=opaque&autoplay=1" title="Fabricação dos Cadernos">
                        <img src="assets/images/empresa/exportacoes/video-thumb3-fabricacaocadernos.png" alt="Fabricação dos Cadernos">
                        <div class="texto">Jandaia</div>
                    </a>
                    <a href="http://www.youtube.com/embed/DGE-1TKa1uI?wmode=opaque&autoplay=1" title="Veja como são feitos">
                        <img src="assets/images/empresa/exportacoes/video-thumb4-comosaofeitos.png" alt="Veja como são feitos">
                        <div class="texto">Jandaia promocional</div>
                    </a>
                    <a href="http://www.youtube.com/embed/lGhGZf8Lzv0?wmode=opaque&autoplay=1" title="Coleção 2018">
                        <img src="assets/images/empresa/exportacoes/video-thumb5-colecao2018.png" alt="Coleção 2018">
                        <div class="texto">Coleção 2018</div>
                    </a>
                </div>
            </div>

            @if($conteudo->imagem_maior)
                <div class="img-center">
                    <img src="assets/images/institucional/{{$conteudo->imagem_maior}}" alt="{{$conteudo->titulo}}">
                </div>
            @endif
        </div>
    </div>

@stop
