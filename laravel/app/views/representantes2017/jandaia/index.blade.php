@section ('conteudo')

    <div class="jandaia empresa">
        <div class="center">
            @include('representantes2017.jandaia._nav')

            <div class="coluna-direita">
                <h1 class="titulo">TUDO <br>COMEÇOU <br>NA RUA <br>JANDAIA</h1>
                <div class="texto">
                    {{$conteudo->texto}}
                </div>
            </div>
        </div>

        <div class="empresa-linha center">
            <div id="timeline">
                <ul id="dates">
                    <li><a href="#" data-target="1956">1956</a></li>
                    <li><a href="#" data-target="1959">1959</a></li>
                    <li><a href="#" data-target="1972">1972</a></li>
                    <li><a href="#" data-target="1987">1987</a></li>
                    <li><a href="#" data-target="1993">1993</a></li>
                    <li><a href="#" data-target="1994">1994</a></li>
                    <li><a href="#" data-target="2006">2006</a></li>
                    <li><a href="#" data-target="2008">2008</a></li>
                    <li><a href="#" data-target="2009">2009</a></li>
                    <li><a href="#" data-target="2010-1">2010</a></li>
                    <li><a href="#" data-target="2010-2">2010</a></li>
                    <li><a href="#" data-target="2010-3">2010</a></li>
                    <li><a href="#" data-target="2011-1">2011</a></li>
                    <li><a href="#" data-target="2011-2">2011</a></li>
                    <li><a href="#" data-target="2013-1">2013</a></li>
                    <li><a href="#" data-target="2013-2">2013</a></li>
                </ul>
                <ul id="issues">
                    <li id="1956">
                        <div class="card">
                            <div class="imagem"><img src="assets/images/layout2017/linhadotempo/linhadotempo-1956.png" alt="História Jandaia - 1956"></div>
                            <p>
                                José Bignardi Neto inicia a produção de cadernos e listas telefônicas em sua pequena gráfica, na rua Jandaia, no centro de São Paulo.
                            </p>
                        </div>
                    </li>
                    <li id="1959">
                        <div class="card">
                            <div class="imagem"><img src="assets/images/layout2017/linhadotempo/linhadotempo-1959.png" alt="História Jandaia - 1959"></div>
                            <p>
                                A gráfica é transferida para um terreno mais amplo na Vila Guilherme, Zona Norte de São Paulo.
                            </p>
                        </div>
                    </li>
                    <li id="1972">
                        <div class="card">
                            <div class="imagem"><img src="assets/images/layout2017/linhadotempo/linhadotempo-1972.png" alt="História Jandaia - 1972"></div>
                            <p>
                                Aquisição da fábrica de papel Gordinho Braune, localizada em Jundiaí.
                            </p>
                        </div>
                    </li>
                    <li id="1987">
                        <div class="card">
                            <div class="imagem"><img src="assets/images/layout2017/linhadotempo/linhadotempo-1987.png" alt="História Jandaia - 1987"></div>
                            <p>
                                Após anos de crescimento, a gráfica Jandaia é transferida para um terreno de 20 mil m2 na cidade de Caieiras - SP
                            </p>
                        </div>
                    </li>
                    <li id="1993">
                        <div class="card">
                            <div class="imagem"><img src="assets/images/layout2017/linhadotempo/linhadotempo-1993.png" alt="História Jandaia - 1993"></div>
                            <p>
                                Nasce o Atacado Jandaia, especializado em materiais de escritório e escolar localizado na cidade de São Paulo.
                            </p>
                        </div>
                    </li>
                    <li id="1994">
                        <div class="card">
                            <div class="imagem"><img src="assets/images/layout2017/linhadotempo/linhadotempo-1994.png" alt="História Jandaia - 1994"></div>
                            <p>
                                Primeiros investimentos relevantes na modernização do parque gráfico, triplicando sua capacidade produtiva.
                            </p>
                        </div>
                    </li>
                    <li id="2006">
                        <div class="card">
                            <div class="imagem"><img src="assets/images/layout2017/linhadotempo/linhadotempo-2006.png" alt="História Jandaia - 2006"></div>
                            <p>
                                A Jandaia completa 50 anos de vida!
                            </p>
                        </div>
                    </li>
                    <li id="2008">
                        <div class="card">
                            <div class="imagem"><img src="assets/images/layout2017/linhadotempo/linhadotempo-2008.png" alt="História Jandaia - 2008"></div>
                            <p>
                                Inicio da segunda fase de investimentos na modernização de maquinário, aumentando ainda mais a capacidade produtiva dos produtos Jandaia.
                            </p>
                        </div>
                    </li>
                    <li id="2009">
                        <div class="card">
                            <div class="imagem"><img src="assets/images/layout2017/linhadotempo/linhadotempo-2009.png" alt="História Jandaia - 2009"></div>
                            <p>
                                Conquista do selo de certificação FSC® para toda a linha de produtos Jandaia
                            </p>
                        </div>
                    </li>
                    <li id="2010-1">
                        <div class="card">
                            <div class="imagem"><img src="assets/images/layout2017/linhadotempo/linhadotempo-2010.png" alt="História Jandaia - 2010"></div>
                            <p>
                                Conquista do selo Iso 9001
                            </p>
                        </div>
                    </li>
                    <li id="2010-2">
                        <div class="card">
                            <div class="imagem"><img src="assets/images/layout2017/linhadotempo/linhadotempo-2010b.png" alt="História Jandaia - 2010"></div>
                            <p>
                                Conquista do Prêmio Fernando Pini com o caderno da licença O Pequeno Príncipe.
                            </p>
                        </div>
                    </li>
                    <li id="2010-3">
                        <div class="card">
                            <div class="imagem"><img src="assets/images/layout2017/linhadotempo/linhadotempo-2010c.png" alt="História Jandaia - 2010"></div>
                            <p>
                                A Jandaia é premiada como empresa licenciada internacional do ano pela Coca-Cola, pela qualidade dos nosso produtos.
                            </p>
                        </div>
                    </li>
                    <li id="2011-1">
                        <div class="card">
                            <div class="imagem"><img src="assets/images/layout2017/linhadotempo/linhadotempo-2011.png" alt="História Jandaia - 2011"></div>
                            <p>
                                A Jandaia recebe mais um prêmio internacional da Coca-Cola, pela inovação tecnológica de nossos produtos.
                            </p>
                        </div>
                    </li>
                    <li id="2011-2">
                        <div class="card">
                            <div class="imagem"><img src="assets/images/layout2017/linhadotempo/linhadotempo-2011b.png" alt="História Jandaia - 2011"></div>
                            <p>
                                Mais um prêmio conquistado. Desta vez, como melhor desenvolvimento de produto pela Revista da Papelaria.
                            </p>
                        </div>
                    </li>
                    <li id="2013-1">
                        <div class="card">
                            <div class="imagem"><img src="assets/images/layout2017/linhadotempo/linhadotempo-2013.png" alt="História Jandaia - 2013"></div>
                            <p>
                                Conquista de prêmio Mattel - melhor desenvolvimento de produto da linha Max Steel
                            </p>
                        </div>
                    </li>
                    <li id="2013-2">
                        <div class="card">
                            <div class="imagem"><img src="assets/images/layout2017/linhadotempo/linhadotempo-2013b.png" alt="História Jandaia - 2013"></div>
                            <p>
                                Participação na Office Paper Brasil 2013
                            </p>
                        </div>
                    </li>
                </ul>
                <a href="#" id="next">+</a> <!-- optional -->
                <a href="#" id="prev">-</a> <!-- optional -->
            </div>
        </div>

        <div class="empresa-produtos">
            <div class="center">
                <div class="titulo">
                    <h3>PRODUTOS PERSONALIZADOS</h3>
                    <h2>UM CADERNO <br>COM A SUA <br>MARCA</h2>
                </div>
                <div class="texto">{{$produtos->texto}}</div>
                <img src="assets/images/institucional/{{$produtos->imagem_maior}}">
            </div>
        </div>
    </div>

@stop
