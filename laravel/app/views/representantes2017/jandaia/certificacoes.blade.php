@section ('conteudo')

    <div class="jandaia certificacoes">
        <div class="center">
            @include('representantes2017.jandaia._nav')

            <div class="coluna-direita">
                <h1 class="titulo">GARANTIA<br> DE ALTA<br> QUALIDADE</h1>
                <div class="texto">
                    {{$conteudo->texto}}
                </div>
            </div>

            <div class="certificacoes-lista">
                @foreach($certificacoes as $k => $v)
                    <div class="certificacao">
                        <div class="imagem">
                            <img src="assets/images/certificacoes/{{$v->imagem}}" alt="{{$v->titulo}}">
                        </div>
                        <div class="texto">{{$v->texto}}</div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

@stop
