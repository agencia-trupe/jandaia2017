<nav>
    <h2>A JANDAIA</h2>
    <a href="{{ route('representantes.jandaia.empresa') }}" @if(Route::currentRouteName() == 'representantes.jandaia.empresa') class="active" @endif>
        &raquo; Empresa e serviços
    </a>
    <a href="{{ route('representantes.jandaia.certificacoes') }}" @if(Route::currentRouteName() == 'representantes.jandaia.certificacoes') class="active" @endif>
        &raquo; Certificações
    </a>
    <a href="{{ route('representantes.jandaia.sustentabilidade') }}" @if(Route::currentRouteName() == 'representantes.jandaia.sustentabilidade') class="active" @endif>
        &raquo; Sustentabilidade
    </a>
    <a href="{{ route('representantes.jandaia.lab') }}" @if(Route::currentRouteName() == 'representantes.jandaia.lab') class="active" @endif>
        &raquo; Jandaia Lab
    </a>
    <a href="{{ route('representantes.jandaia.exportacao') }}" @if(Route::currentRouteName() == 'representantes.jandaia.exportacao') class="active" @endif>
        &raquo; Exportação
    </a>
</nav>
