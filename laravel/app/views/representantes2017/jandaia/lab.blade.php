@section ('conteudo')

    <div class="jandaia lab">
        <div class="center">
            @include('representantes2017.jandaia._nav')

            <div class="coluna-direita">
                <img src="assets/images/lab/marca-jandaia-lab.png" id="imagem-topo-lab" alt="Jandaia Lab">

                <img src="assets/images/lab/titulo-jandaia-lab.png" id="imagem-chamada-lab" alt="É um pássaro? É um avião? Afinal, o que é esse tal de Jandaia Lab?">

                <p class="island-p">Um belo dia você acorda e tem na sua frente a maior coleção de ideias super incríveis na forma de cadernos. Como é que tudo isso tomou forma? Simples: é o Jandaia Lab em ação.</p>

                <div class="box azul-claro">
                    <div class="innerbox vermelho">
                        <img src="assets/images/lab/jandaialab-img1-1.png" alt="">
                        <p>
                            Jandaia Lab é um ambiente de<br>
                            pesquisa e criação com equipes <br>
                            conectadas no mundo, on line, <br>
                            24 horas e nas nuvens.
                        </p>
                    </div>

                    <div class="innerbox azul">
                        <img src="assets/images/lab/jandaialab-img2.png" alt="">
                        <p>
                            Jandaia Lab é um time que reúne <br>
                            centenas de talentos de marketing, <br>
                            design gráfico, moda & tendências <br>
                            de consumo, ilustradores <br>
                            e criativos de muitas tribos.
                        </p>
                    </div>

                    <p class="inner-p">Esse povo todo abastece a gente com informações sobre o que crianças e jovens, estudantes e profissionais, mamães e papais estão vendo, comendo, usando e curtindo no mundo inteiro.</p>
                </div>

                <div class="box fundo-branco">
                    <p class="azul">Jandaia Lab mostra o que vai estar entre as coisas mais desejadas pelo público daqui a 6 meses.</p>

                    <p class="laranja">Jandaia Lab sabe com antecedência qual será o blockbuster da próxima temporada. O personagem que vai acontecer, os filmes que vão bombar, as cores que as meninas vão curtir, os carros e motos que os meninos vão preferir.</p>

                    <p class="rosa">Jandaia Lab antecipou, por exemplo, o sucesso espetacular da franquia Angry Birds. Quando eles eram só uns birds normais, a gente já sabia que a coisa ia acontecer e saímos na frente. Jandaia Lab põe os cadernos Jandaia à frente. À frente do tempo, da concorrência, do lojista e até do consumidor.</p>

                    <p class="verde">Jandaia Lab está de olho em tudo<br> e em todos os lugares.</p>
                </div>
            </div>
        </div>
    </div>

@stop
