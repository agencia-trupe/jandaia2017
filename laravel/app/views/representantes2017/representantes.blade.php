@section ('conteudo')

<div class="promocao-penaestrada">
	<div class="center">
		<img src="{{ asset('assets/images/promocao-penaestrada/imagem.png') }}" class="imagem">
		<div class="texto">
			<img src="{{ asset('assets/images/promocao-penaestrada/titulo.png') }}" alt="">
			<p>A promoção <span>Pé na Estrada Jandaia</span> já está valendo. Cada <span>R$ 5.000,00 em compras</span> equivalem a <span>um cupom</span>. O sorteado poderá <span>escolher</span> entre <span>6 destinos maravilhosos</span>. Vamos lá. Faça sua parte. Entre nessa de cabeça, pegue papel e caneta e ajude a Jandaia a escrever mais um capítulo de sucesso na história.</p>
			<a href="http://www.promocaopenaestradajandaia.com.br/" target="_blank">
				VISITE O HOTSITE DA PROMOÇÃO
			</a>
		</div>
		<img src="{{ asset('assets/images/promocao-penaestrada/detalhe.png') }}" class="detalhe">
	</div>
</div>

<div class="representantes">
	<div class="center">
		<div class="representantes-busca">
			<h1>ENCONTRE UM REPRESENTANTE</h1>
			<div class="texto">
				<p>
					Selecione o Estado para visualizar a lista de representantes próximos a você:
				</p>

				<form action="{{ URL::route('representantes.representantes') }}" method="post" id="form-representante-cidade">
					<select name="estado">
						<option value="">Estado</option>
						@if($estados)
							@foreach($estados as $estado)
								<option value="{{ $estado->id }}" id="option-{{ $estado->uf }}" @if($filtroEstado == $estado->id) selected @endif>{{ $estado->nome }}</option>
							@endforeach
						@endif
					</select>
					<input type="submit" value="CONSULTAR">
				</form>
			</div>
			<div class="representantes-mapa">
				<div id="mapa">
					<a href="representantes/ac" class="ac @if($marcarEstado=='ac') active @endif" title="Acre" data-uf="AC">Acre</a>
					<a href="representantes/al" class="al @if($marcarEstado=='al') active @endif" title="Alagoas" data-uf="AL">Alagoas</a>
					<a href="representantes/ap" class="ap @if($marcarEstado=='ap') active @endif" title="Amapá" data-uf="AP">Amapá</a>
					<a href="representantes/am" class="am @if($marcarEstado=='am') active @endif" title="Amazonas" data-uf="AM">Amazonas</a>
					<a href="representantes/ba" class="ba @if($marcarEstado=='ba') active @endif" title="Bahia " data-uf="BA">Bahia </a>
					<a href="representantes/ce" class="ce @if($marcarEstado=='ce') active @endif" title="Ceará" data-uf="CE">Ceará</a>
					<a href="representantes/df" class="df @if($marcarEstado=='df') active @endif" title="Distrito Federal" data-uf="DF">Distrito Federal</a>
					<a href="representantes/es" class="es @if($marcarEstado=='es') active @endif" title="Espírito Santo" data-uf="ES">Espírito Santo</a>
					<a href="representantes/go" class="go @if($marcarEstado=='go') active @endif" title="Goiás" data-uf="GO">Goiás</a>
					<a href="representantes/ma" class="ma @if($marcarEstado=='ma') active @endif" title="Maranhão" data-uf="MA">Maranhão</a>
					<a href="representantes/mt" class="mt @if($marcarEstado=='mt') active @endif" title="Mato Grosso" data-uf="MT">Mato Grosso</a>
					<a href="representantes/ms" class="ms @if($marcarEstado=='ms') active @endif" title="Mato Grosso do Sul" data-uf="MS">Mato Grosso do Sul</a>
					<a href="representantes/mg" class="mg @if($marcarEstado=='mg') active @endif" title="Minas Gerais" data-uf="MG">Minas Gerais</a>
					<a href="representantes/pa" class="pa @if($marcarEstado=='pa') active @endif" title="Pará" data-uf="PA">Pará</a>
					<a href="representantes/pb" class="pb @if($marcarEstado=='pb') active @endif" title="Paraíba" data-uf="PB">Paraíba</a>
					<a href="representantes/pr" class="pr @if($marcarEstado=='pr') active @endif" title="Paraná" data-uf="PR">Paraná</a>
					<a href="representantes/pe" class="pe @if($marcarEstado=='pe') active @endif" title="Pernambuco" data-uf="PE">Pernambuco</a>
					<a href="representantes/pi" class="pi @if($marcarEstado=='pi') active @endif" title="Piauí" data-uf="PI">Piauí</a>
					<a href="representantes/rj" class="rj @if($marcarEstado=='rj') active @endif" title="Rio de Janeiro" data-uf="RJ">Rio de Janeiro</a>
					<a href="representantes/rn" class="rn @if($marcarEstado=='rn') active @endif" title="Rio Grande do Norte" data-uf="RN">Rio Grande do Norte</a>
					<a href="representantes/rs" class="rs @if($marcarEstado=='rs') active @endif" title="Rio Grande do Sul" data-uf="RS">Rio Grande do Sul</a>
					<a href="representantes/ro" class="ro @if($marcarEstado=='ro') active @endif" title="Rondônia" data-uf="RO">Rondônia</a>
					<a href="representantes/rr" class="rr @if($marcarEstado=='rr') active @endif" title="Roraima" data-uf="RR">Roraima</a>
					<a href="representantes/sc" class="sc @if($marcarEstado=='sc') active @endif" title="Santa Catarina" data-uf="SC">Santa Catarina</a>
					<a href="representantes/sp" class="sp @if($marcarEstado=='sp') active @endif" title="São Paulo" data-uf="SP">São Paulo</a>
					<a href="representantes/se" class="se @if($marcarEstado=='se') active @endif" title="Sergipe" data-uf="SE">Sergipe</a>
					<a href="representantes/to" class="to @if($marcarEstado=='to') active @endif" title="Tocantins" data-uf="TO">Tocantins</a>
				</div>
			</div>
		</div>
		@if($listaRepresentantes)
			<div class="representantes-resultados">
				@foreach($listaRepresentantes as $value)
				<div>
					@if($value->regiao)
						Região: <strong>{{ $value->regiao }}</strong><br><br>
					@endif
					@if($value->empresa)
						{{ $value->empresa }}<br>
					@endif
					{{ $value->texto }}
					@if($value->email)
						E-mail: <a href="mailto:{{$value->email}}" title="Entre em contato!">{{ $value->email }}</a>
					@endif
				</div>
				@endforeach
			</div>
		@else
			@if(Session::has('results-fail'))
				<div class="representantes-fail">
					<h2>Nenhum representante encontrado.</h2>
				</div>
			@endif
		@endif
	</div>
</div>

@stop
