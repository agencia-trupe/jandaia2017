@section('conteudo')

    <div class="home">
        <div class="banners">
            @foreach($banners as $banner)
            <a href="{{ $banner->link }}" style="background-image:url('{{ asset('assets/images/banners/'.$banner->imagem) }}')"></a>
            @endforeach

            <div class="cycle-pager"></div>
        </div>

        <div class="chamadas">
            <div class="center">
                @foreach($chamadas as $chamada)
                <a href="{{ $chamada->link }}" title="{{ $chamada->titulo }}">
                    <img src="{{ asset('assets/images/chamadas/'.$chamada->imagem) }}" alt="">
                    <div class="overlay">
                        <span>{{ $chamada->titulo }}</span>
                    </div>
                </a>
                @endforeach
            </div>
        </div>

        <div class="novidade">
            <div class="center">
                <a href="produtos/linha/123-mulher-maravilha" class="imagem">
                    <img src="{{ asset('assets/images/layout2017/destaque-home/mulhermaravilha.png') }}" alt="">
                </a>
                <h3>MULHER MARAVILHA</h3>
                <p>
                    O sucesso de Mulher Maravilha nos cinemas se repete na linha de cadernos exclusivos que a Jandaia trouxe para o Brasil. As capas especialmente desenvolvidas para o mercado brasileiro são a imagem da força e da ação do filme e da personagem que está conquistando novos fãs e seguidores em todo o mundo. Será impossível não se apaixonar pela força dessa maravilhosa mulher!
                </p>
            </div>
        </div>

        <div class="mais">
            <div class="center">
                <h2>MAIS SOBRE A JANDAIA</h2>

                <div class="mais-wrapper">
                    <a href="produtos" class="mais-1">
                        <div class="icone"></div>
                        <span>Confira nossos produtos e coleções exclusivas</span>
                    </a>
                    <a href="a-jandaia/exportacao" class="mais-2">
                        <div class="icone"></div>
                        <span>Como nossos cadernos são fabricados</span>
                    </a>
                    <a href="a-jandaia" class="mais-3">
                        <div class="icone"></div>
                        <span>A história da maior fabricante brasileira de papel reciclado</span>
                    </a>
                    <a href="onde-encontrar" class="mais-4">
                        <div class="icone"></div>
                        <span>Encontre nossos cadernos perto de você</span>
                    </a>
                    <a href="educacao" class="mais-5">
                        <div class="icone"></div>
                        <span>Acompanhe o projeto Jandaia Educação</span>
                    </a>
                </div>
            </div>
        </div>

        <div class="video">
            <div class="center">
                <a href="http://www.youtube.com/user/cadernosjandaia" target="_blank">Acompanhe nosso canal no YouTube</a>

                <div class="video-wrapper">
                    <div class="embed">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/lGhGZf8Lzv0" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
