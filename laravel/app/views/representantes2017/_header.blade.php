<nav id="nav-mobile">
    <div class="superior">
        <a href="{{ route('representantes.home') }}" @if(Route::currentRouteName() == 'representantes.home') class="active" @endif>Home</a>
        {{--<a href="http://jandaia.com/catalogo-virtual/" target="_blank">Orçamento Virtual</a>--}}
        <a href="{{ route('representantes.representantes') }}" @if(Route::currentRouteName() == 'representantes.representantes') class="active" @endif>Área do Lojista</a>
        <a href="http://fvendas.bignardi.com.br/fvweb/" target="_blank">Área do Representante</a>
        <a href="{{ route('representantes.contato') }}" @if(Route::currentRouteName() == 'representantes.contato') class="active" @endif>Contato</a>
        <a href="http://fvendas.bignardi.com.br/fvweb/SegundaViaBoleto.jsp" target="_blank">Emissão de Boleto</a>
    </div>
    <div class="principal">
        <a href="{{ route('representantes.produtos') }}" @if(Route::currentRouteName() == 'representantes.produtos') class="active" @endif>Produtos</a>
        <a href="{{ route('representantes.encontrar') }}" @if(str_is('representantes.encontrar*', Route::currentRouteName())) class="active" @endif>Onde Encontrar</a>
        <a href="{{ route('representantes.jandaia.empresa') }}" @if(str_is('representantes.jandaia*', Route::currentRouteName())) class="active" @endif>A Jandaia</a>
        {{-- <a href="#">Blog</a> --}}
    </div>
</nav>

<header>
    <div class="menu-superior">
        <div class="center">
            <a href="{{ route('representantes.home') }}" @if(Route::currentRouteName() == 'representantes.home') class="active" @endif>Home</a>
            {{--<a href="http://jandaia.com/catalogo-virtual/" target="_blank">Orçamento Virtual</a>--}}
            <a href="{{ route('representantes.representantes') }}" @if(Route::currentRouteName() == 'representantes.representantes') class="active" @endif>Área do Lojista</a>
            <a href="http://fvendas.bignardi.com.br/fvweb/" target="_blank">Área do Representante</a>
            <a href="{{ route('representantes.contato') }}" @if(Route::currentRouteName() == 'representantes.contato') class="active" @endif>Contato</a>
            <a href="http://fvendas.bignardi.com.br/fvweb/SegundaViaBoleto.jsp" target="_blank">Emissão de Boleto</a>
        </div>
    </div>
    <div class="menu-principal center">
        <a href="{{ route('representantes.home') }}" class="logo">Jandaia</a>

        <nav class="nav-desktop">
            <a href="{{ route('representantes.produtos') }}" @if(Route::currentRouteName() == 'representantes.produtos') class="active" @endif>Produtos</a>
            <a href="{{ route('representantes.encontrar') }}" @if(str_is('representantes.encontrar*', Route::currentRouteName())) class="active" @endif>Onde Encontrar</a>
            <a href="{{ route('representantes.jandaia.empresa') }}" @if(str_is('representantes.jandaia*', Route::currentRouteName())) class="active" @endif>A Jandaia</a>
            {{-- <a href="#">Blog</a> --}}
        </nav>

        <form action="{{ URL::route('representantes.busca') }}" method="POST" class="busca">
            <div class="input-wrapper">
                <input type="text" name="termo" placeholder="busca" required>
            </div>
        </form>

        <button id="mobile-toggle" type="button" role="button">
            <span class="lines"></span>
        </button>

        <div class="social">
            <a href="https://www.facebook.com/cadernosjandaia" class="facebook" target="_blank">facebook</a>
            <a href="https://instagram.com/cadernosjandaia/" class="instagram" target="_blank">instagram</a>
            <a href="http://www.youtube.com/user/cadernosjandaia" class="youtube" target="_blank">youtube</a>
        </div>
    </div>
</header>
