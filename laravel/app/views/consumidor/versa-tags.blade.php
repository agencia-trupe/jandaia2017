@if(in_array(URL::current(), array(
    'http://cadernosjandaia',
    'http://cadernosjandaia.com.br',
    'http://cadernosjandaia.com.br/encontrar',
    'http://cadernosjandaia.com.br/produtos',
    'http://cadernosjandaia.com.br/produtos/linha/36-angry-birds',
    'http://cadernosjandaia.com.br/produtos/linha/94-angry-birds-girls',
    'http://cadernosjandaia.com.br/produtos/linha/135-as-meninas-superpoderosas',
    'http://cadernosjandaia.com.br/produtos/linha/122-batman-vs-superman',
    'http://cadernosjandaia.com.br/produtos/linha/139-chilli-beans',
    'http://cadernosjandaia.com.br/produtos/linha/133-christian-figueiredo',
    'http://cadernosjandaia.com.br/produtos/linha/coca-cola',
    'http://cadernosjandaia.com.br/produtos/linha/64-coke-girls',
    'http://cadernosjandaia.com.br/produtos/linha/102-dc-comics',
    'http://cadernosjandaia.com.br/produtos/linha/140-dc-girl-power',
    'http://cadernosjandaia.com.br/produtos/linha/141-deadpool',
    'http://cadernosjandaia.com.br/produtos/linha/143-elena-de-avalor',
    'http://cadernosjandaia.com.br/produtos/linha/144-esquadrao-suicida',
    'http://cadernosjandaia.com.br/produtos/linha/145-frida-kahlo',
    'http://cadernosjandaia.com.br/produtos/linha/110-frozen',
    'http://cadernosjandaia.com.br/produtos/linha/142-kefera',
    'http://cadernosjandaia.com.br/produtos/linha/104-liga-da-justica',
    'http://cadernosjandaia.com.br/produtos/linha/47-max-steel',
    'http://cadernosjandaia.com.br/produtos/linha/123-mulher-maravilha',
    'http://cadernosjandaia.com.br/produtos/linha/73-o-pequeno-principe',
    'http://cadernosjandaia.com.br/produtos/linha/95-scooby-doo',
    'http://cadernosjandaia.com.br/produtos/linha/150-shopkins',
    'http://cadernosjandaia.com.br/produtos/linha/128-show-da-luna',
    'http://cadernosjandaia.com.br/produtos/linha/93-star-wars',
    'http://cadernosjandaia.com.br/produtos/linha/154-star-wars-rogue-one',
    'http://cadernosjandaia.com.br/produtos/linha/130-thomas-e-seus-amigoa',
    'http://cadernosjandaia.com.br/produtos/linha/134-youtubers'
  )))

  <script>
    //
    // NOTE: You can test if the tags are working correctly before the campaign launches
    // as follows:  Browse to http://bs.serving-sys.com/BurstingPipe/adServer.bs?cn=at, which is
    // a page that lets you set your local machine to 'testing' mode.  In this mode, when
    // visiting a page that includes a VersaTag, a new window will open, showing you
    // the tags activated by the VersaTag and the data sent by the VersaTag tag to the Sizmek servers.
    //

    var versaTag = {};
    versaTag.id = "6616";
    versaTag.sync = 0;
    versaTag.dispType = "js";
    versaTag.ptcl = "HTTP";
    versaTag.bsUrl = "bs.serving-sys.com/BurstingPipe";
    //VersaTag activity parameters include all conversion parameters including custom parameters and Predefined parameters. Syntax: "ParamName1":"ParamValue1", "ParamName2":"ParamValue2". ParamValue can be empty.
    versaTag.activityParams = {
    //Predefined parameters:
    "Session":""
    //Custom parameters:
    };
    //Static retargeting tags parameters. Syntax: "TagID1":"ParamValue1", "TagID2":"ParamValue2". ParamValue can be empty.
    versaTag.retargetParams = {};
    //Dynamic retargeting tags parameters. Syntax: "TagID1":"ParamValue1", "TagID2":"ParamValue2". ParamValue can be empty.
    versaTag.dynamicRetargetParams = {};
    // Third party tags conditional parameters and mapping rule parameters. Syntax: "CondParam1":"ParamValue1", "CondParam2":"ParamValue2". ParamValue can be empty.
    versaTag.conditionalParams = {};
  </script>

  <script id="ebOneTagUrlId" src="http://ds.serving-sys.com/SemiCachedScripts/ebOneTag.js"></script>

  <noscript>
    <iframe src="http://bs.serving-sys.com/BurstingPipe?cn=ot&amp;onetagid=6616&amp;ns=1&amp;activityValues=$$Session=[Session]$$&amp;retargetingValues=$$$$&amp;dynamicRetargetingValues=$$$$&amp;acp=$$$$&amp;" style="display:none;width:0px;height:0px"></iframe>
  </noscript>

@endif
