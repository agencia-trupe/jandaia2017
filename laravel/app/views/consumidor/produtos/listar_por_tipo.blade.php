@section('conteudo')
	
	<div class="centro">
		<div class="grid-produtos pure-g-r">
			
			<aside class="pure-u-1-4">

				<div class="titulo padl">
					Selecione uma linha <a href="#" id="mostrar-linhas">[<span>ver</span> todas]</a>
				</div>

				@if($listaLinhas)
					<ul class="lista-linhas">
						@foreach($listaLinhas as $val)
							<li>
								@if($tipo)
									<a href="produtos/{{ $tipo->slug }}/{{ $val->slug }}" title="{{ $val->titulo }}"><span>&raquo;</span> {{ $val->titulo }}</a>
								@else
									<a href="produtos/linha/{{ $val->slug }}" title="{{ $val->titulo }}"><span>&raquo;</span> {{ $val->titulo }}</a>
								@endif
							</li>
						@endforeach
						<li><a href="produtos" title="Visualize por linha de Produtos" class="visualizar-por-linha">[visualize por linha]</a></li>	
					</ul>
				@endif		
			</aside>
			
			<section class="pure-u-3-4 recuo-top">

				@if($listaTipos)
					<div id="seleciona-tipo">
						
						<div class="pure-g-r">
							@if(!is_null($tipo))
								<a href="produtos/{{ $tipo->slug }}" title="Exibindo {{$tipo->titulo}}" class="mn-{{ $tipo->slug }} selecionado pure-u-1-3">
									<div class="pure-g">									
										<div class="titulo pure-u-3-5">
											<div class="pad">
												exibindo {{ $tipo->titulo }}
											</div>
										</div>

										<div class="imagem pure-u-2-5">
											<img src="assets/images/consumidor/layout/{{ $tipo->slug }}.png" alt="{{ $tipo->titulo }}">
										</div>
									</div>
								</a>
							@else
								<a href="produtos/linha/{{ $linha->slug }}" title="Exibindo Linha" class="mn-linha selecionado pure-u-1-3">exibindo Linha Completa</a>
							@endif

							<a href="#" title="Ver Outros Tipo de Produto" class="alterar-tipo pure-u-1-3">[outras categorias de produto]</a>
							@if($linha && $tipo)
								<a href="produtos/linha/{{ $linha->slug }}" title="Ver Linha {{ $linha->titulo }} Completa" class="ver-linha-completa pure-u-1-3">[ver Linha completa]</a>
							@else
								<div class="pure-u-1-3"></div>
							@endif
						</div>

						<div class="opcoes escondidas pure-g-r">
							@foreach($listaTipos as $val)
								@if(!is_null($tipo) && $val->slug != $tipo->slug)
									<a href="produtos/{{ $val->slug }}" title="Exibir {{$val->titulo}}" class="mn-{{$val->slug}} pure-u-1-4">
										<div class="pure-g">
											<div class="imagem pure-u-1-4">
												<img src="assets/images/consumidor/layout/{{ $val->slug }}.png" alt="{{ $val->titulo }}">
											</div>
											<div class="titulo pure-u-3-4">
												<div class="pad">
													&raquo; {{ $val->titulo }}
												</div>
											</div>
										</div>
									</a>
								@endif
							@endforeach
						</div>
					</div>
				@endif

				<div id="fundo-papel"></div>
				<div class="lista-produtos">
					<div class="pure-g-r">
						@if($listaProdutos)
							@foreach($listaProdutos as $produto)
								<a href="produtos/{{ $produto->tipo_slug }}/{{ $produto->linha_slug }}/{{ $produto->slug }}" title="{{ $produto->titulo }}" class="pure-u-1-4">
									<img src="assets/images/produtos/thumbs/{{ $produto->imagem }}" alt="{{ $produto->titulo }}">
									<div class="overlay"></div>
								</a>
							@endforeach
						@endif
					</div>	
				</div>			

			</section>
			
		</div>
	</div>

@stop