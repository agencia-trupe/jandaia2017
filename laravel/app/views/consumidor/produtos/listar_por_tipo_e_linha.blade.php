@section('conteudo')

	<div class="centro">
		<div class="grid-produtos pure-g-r">

			<aside class="pure-u-1-4">

				<div class="titulo padl">
					Selecione uma linha <a href="#" id="mostrar-linhas">[<span>ver</span> todas]</a>
				</div>

				@if($listaLinhas)
					<ul class="lista-linhas">
						@foreach($listaLinhas as $val)
							<li>
								@if($tipo)
									<a href="produtos/{{ $tipo->slug }}/{{ $val->slug }}" title="{{ $val->titulo }}"><span>&raquo;</span> {{ $val->titulo }}</a>
								@else
									<a href="produtos/linha/{{ $val->slug }}" title="{{ $val->titulo }}"><span>&raquo;</span> {{ $val->titulo }}</a>
								@endif
							</li>
						@endforeach
						<li><a href="produtos" title="Visualize por linha de Produtos" class="visualizar-por-linha">[visualize por linha]</a></li>
					</ul>
				@endif
			</aside>

			<section class="pure-u-3-4 recuo-top">

				@if($listaTipos)
					<div id="seleciona-tipo">

						<div class="pure-g-r">
							@if(!is_null($tipo))
								@if($linha)
									<a href="produtos/{{ $tipo->slug }}/{{ $linha->slug }}" title="Exibindo {{$tipo->titulo}}" class="mn-{{ $tipo->slug }} selecionado pure-u-1-3">
								@else
									<a href="produtos/{{ $tipo->slug }}" title="Exibindo {{$tipo->titulo}}" class="mn-{{ $tipo->slug }} selecionado pure-u-1-3">
								@endif
									<div class="pure-g">
										<div class="titulo pure-u-3-5">
											<div class="pad">
												exibindo {{ $tipo->titulo }}
											</div>
										</div>

										<div class="imagem pure-u-2-5">
											<img src="assets/images/consumidor/layout/{{ $tipo->slug }}.png" alt="{{ $tipo->titulo }}">
										</div>
									</div>
								</a>
							@else
								<a href="produtos/linha/{{ $linha->slug }}" title="Exibindo Linha" class="mn-linha selecionado pure-u-1-3">exibindo Linha Completa</a>
							@endif

							<a href="#" title="Ver Outros Tipo de Produto" class="alterar-tipo pure-u-1-3">[outras categorias de produto]</a>
							@if($linha && $tipo)
								<a href="produtos/linha/{{ $linha->slug }}" title="Ver Linha {{ $linha->titulo }} Completa" class="ver-linha-completa pure-u-1-3">[ver Linha completa]</a>
							@else
								<div class="pure-u-1-3"></div>
							@endif
						</div>

						<div class="opcoes escondidas pure-g-r">
							@foreach($listaTipos as $val)
								@if(!is_null($tipo) && $val->slug != $tipo->slug)

									@if($linha)
										<a href="produtos/{{ $val->slug }}/{{ $linha->slug }}" title="Exibir {{$val->titulo}}" class="mn-{{$val->slug}} pure-u-1-4">
									@else
										<a href="produtos/{{ $val->slug }}" title="Exibir {{$val->titulo}}" class="mn-{{$val->slug}} pure-u-1-4">
									@endif
										<div class="pure-g">
											<div class="imagem pure-u-1-4">
												<img src="assets/images/consumidor/layout/{{ $val->slug }}.png" alt="{{ $val->titulo }}">
											</div>
											<div class="titulo pure-u-3-4">
												<div class="pad">
													&raquo; {{ $val->titulo }}
												</div>
											</div>
										</div>
									</a>
								@endif
							@endforeach
						</div>
					</div>
				@endif

				<div id="fundo-papel"></div>
				<div id="detalhe-produtos">
					<div class="pure-g-r">

						@if($produto)

							<div id="ampliada" class="pure-u-7-12">
								<img src="assets/images/produtos/{{ $produto->imagem }}" alt="{{ $produto->titulo }}" id="detalhe">

								<div class="like-container">
									<div class="fb-like" data-href="{{ Request::url() }}" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div>
								</div>
							</div>

							<div id="texto" class="pure-u-5-12">

								<div id="detalhes">
									<h3 class="{{ $tipo->slug }}">{{ $produto->titulo }}</h3>
									{{ str_replace('<p>&nbsp;</p>','', $produto->detalhes) }}
								</div>

								@if($listaProdutosMesmoTipoMiolos)
									<div class="miolos pure-g">
										@foreach($listaProdutosMesmoTipoMiolos as $prod)
											<a href="produtos/{{ $tipo->slug }}/{{ $linha->slug }}/{{ $prod->slug }}" title="{{ $prod->titulo }}" class="pure-u-1-3 @if(!is_null($produto) && $prod->id == $produto->id) ativo @endif">
												<div class="pad">
													<img src="assets/images/produtos/thumbs/{{ $prod->imagem }}" alt="{{ $prod->titulo }}">
												</div>
											</a>
										@endforeach
									</div>
								@endif

							</div>

							<div class="lista-produtos maior">
								<div class="pure-g">
									@if($listaProdutosMesmoTipoCapas)
										@foreach($listaProdutosMesmoTipoCapas as $prod)
											<a href="produtos/{{ $tipo->slug }}/{{ $linha->slug }}/{{ $prod->slug }}" title="{{ $prod->titulo }}" class="pure-u-1-4 @if(!is_null($produto) && $prod->id == $produto->id) ativo @endif">
												<img src="assets/images/produtos/thumbs/{{ $prod->imagem }}" alt="{{ $prod->titulo }}">
											</a>
										@endforeach
									@endif
								</div>
							</div>

							<?php $tipo_caderno_atual = $produto->tipo_caderno;?>
						@else

							<?php $tipo_caderno_atual = 0?>
							@if($mostrarTitulo)
								<h1 class="pure-u-1 {{ $tipo->slug }}">{{ $mostrarTitulo }}</h1>
							@endif
						@endif


						@if($listaProdutos)
							<div class="lista-produtos maior pure-g">
								@foreach($listaProdutos as $i => $prod)

									@if($prod->produtos_tipo_id == 11 && $prod->tipo_caderno != $tipo_caderno_atual && !in_array($prod->tipo_caderno, array(5,6,7)))
										</div>
										<h2 class="pure-u-1 tit-outros-tipos {{ $tipo->slug }}">
											@if($prod->tipo_caderno == 1)
												BROCHURA
											@elseif($prod->tipo_caderno == 2)
												ESPIRAL
											@elseif($prod->tipo_caderno == 3)
												PLUS
											@elseif($prod->tipo_caderno == 4)
												PEDAGÓGICO
											@elseif($prod->tipo_caderno == 8)
												CARTOGRÁFICO
											@endif
										</h2>
										<div class="lista-produtos maior pure-g">
										<?php $tipo_caderno_atual = $prod->tipo_caderno ?>
									@endif
									<a href="produtos/{{ $prod->tipo_slug }}/{{ $prod->linha_slug }}/{{ $prod->slug }}" title="{{ $prod->titulo }}" class="pure-u-1-4 @if(!is_null($produto) && $prod->id == $produto->id) ativo @endif">
										<img src="assets/images/produtos/thumbs/{{ $prod->imagem }}" alt="{{ $prod->titulo }}">
									</a>
								@endforeach
							</div>
						@endif

					</div>
				</div>

				@if(isset($outrosProdutos) && $outrosProdutos)
					<div id="ver-outros-tipos">
						<h1>VER MAIS PRODUTOS DA MESMA LINHA</h1>
						@foreach($outrosProdutos as $links)
							<a href="produtos/{{$links->slug}}/{{ $linha->slug }}" title="ver {{ $links->titulo }} da linha {{ $linha->titulo }}" class="outros-tipos {{ $links->slug }}">&raquo; {{ $links->titulo }} &laquo;</a><br>
						@endforeach
					</div>
				@endif

			</section>

		</div>
	</div>

@stop