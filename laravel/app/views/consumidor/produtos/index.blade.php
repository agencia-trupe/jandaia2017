@section('conteudo')
	
	<div class="centro">
		<div class="grid-produtos pure-g-r">
			
			<aside class="pure-u-1-4">

				<div class="titulo padl">
					Visualize por tipo de produto
				</div>

				@if($listaTipos)
					<ul class="lista-tipos">
						@foreach($listaTipos as $val)
							<li>
								<a href="produtos/{{ $val->slug }}" title="{{ $val->titulo }}" class="mn-{{ $val->slug }} pure-g">
									<div class="imagem pure-u-2-5">
										<img src="assets/images/consumidor/layout/{{ $val->slug }}.png" alt="{{ $val->titulo }}">
									</div>
									<div class="titulo pure-u-3-5">
										<div class="pad">
											&raquo; {{ $val->titulo }}
										</div>
									</div>
								</a>
							</li>
						@endforeach		
					</ul>
				@endif
		
			</aside>
			
			<section class="pure-u-3-4">

				<div class="titulo">
					Visualize por linha de produtos <a href="#" id="mostrar-linhas">[<span>ver</span> todas as linhas]</a>
				</div>

				<div class="pure-g-r">
					
					@if($listaLinhas)
						<ul class="pure-u-1-3 lista-linhas">

							<?php $num_colunas = 3 ?>

							<?php $por_coluna = ceil(sizeof($listaLinhas)/$num_colunas); ?>

							@foreach($listaLinhas as $indice => $val)

								@if($indice%$por_coluna == 0 && $indice > 0)
									</ul><ul class="pure-u-1-3 lista-linhas">
								@endif

								<li>
									<a href="produtos/linha/{{ $val->slug }}" title="{{ $val->titulo }}"><span>&raquo;</span> {{ $val->titulo }}</a>
								</li>
							@endforeach
						</ul>
					@endif

				</div>
			</section>
			
		</div>
	</div>

@stop