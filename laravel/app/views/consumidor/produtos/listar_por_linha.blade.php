@section('conteudo')
	
	<div class="centro">
		<div class="grid-produtos pure-g-r">
			
			<aside class="pure-u-1-4">

				<div class="titulo padl">
					Selecione uma linha <a href="#" id="mostrar-linhas">[<span>ver</span> todas]</a>
				</div>

				@if($listaLinhas)
					<ul class="lista-linhas">
						@foreach($listaLinhas as $val)
							<li>
								<a href="produtos/linha/{{ $val->slug }}" title="{{ $val->titulo }}"><span>&raquo;</span> {{ $val->titulo }}</a>
							</li>
						@endforeach						
					</ul>
				@endif		
			</aside>
			
			<section class="pure-u-3-4 recuo-top">

				@if($listaTipos)
					<div id="seleciona-tipo">
						
						<div class="pure-g-r">
							@if(!is_null($tipo))
								@if($linha)
									<a href="produtos/{{ $tipo->slug }}/{{ $linha->slug }}" title="Exibindo {{$tipo->titulo}}" class="mn-{{ $tipo->slug }} selecionado pure-u-1-3">
								@else
									<a href="produtos/{{ $tipo->slug }}" title="Exibindo {{$tipo->titulo}}" class="mn-{{ $tipo->slug }} selecionado pure-u-1-3">
								@endif
									<div class="pure-g">									
										<div class="titulo pure-u-3-5">
											<div class="pad">
												exibindo {{ $tipo->titulo }}
											</div>
										</div>

										<div class="imagem pure-u-2-5">
											<img src="assets/images/consumidor/layout/{{ $tipo->slug }}.png" alt="{{ $tipo->titulo }}">
										</div>
									</div>
								</a>
							@else
								<a href="produtos/linha/{{ $linha->slug }}" title="Exibindo Linha" class="mn-linha selecionado pure-u-1-3">
									<div class="pure-g">									
										<div class="titulo pure-u-1">
											<div class="pad">
												exibindo Linha Completa
											</div>
										</div>									
									</div>
								</a>
							@endif

							<a href="#" title="Ver Outros Tipo de Produto" class="alterar-tipo pure-u-1-3">[outras categorias de produto]</a>
							@if($linha && $tipo)
								<a href="produtos/linha/{{ $linha->slug }}" title="Ver Linha {{ $linha->titulo }} Completa" class="ver-linha-completa pure-u-1-3">[ver Linha completa]</a>
							@else
								<div class="pure-u-1-3"></div>
							@endif
						</div>
						<div class="opcoes escondidas pure-g">
							@foreach($listaTipos as $val)
									@if($linha)
										<a href="produtos/{{ $val->slug }}/{{ $linha->slug }}" title="Exibir {{$val->titulo}}" class="mn-{{$val->slug}} pure-u-1-4">
									@else
										<a href="produtos/{{ $val->slug }}" title="Exibir {{$val->titulo}}" class="mn-{{$val->slug}} pure-u-1-4">
									@endif
										<div class="pure-g">
											<div class="imagem pure-u-1-4">
												<img src="assets/images/consumidor/layout/{{ $val->slug }}.png" alt="{{ $val->titulo }}">
											</div>
											<div class="titulo pure-u-3-4">
												<div class="pad">
													&raquo; {{ $val->titulo }}
												</div>
											</div>
										</div>
									</a>								
							@endforeach
						</div>
					</div>
				@endif

				<div id="fundo-papel"></div>
				<div class="lista-produtos">
					<div class="pure-g-r">

						@if($linha->texto_promocional)
							<p class="promo pure-u-1">{{ $linha->texto_promocional }}</p>
						@endif

						@if($listaProdutos)

							<?php $tipo_atual = ''; ?>


							@foreach($listaProdutos as $produto)

								@if($tipo_atual != $produto->tipo_slug.$produto->miolo)
									<h1 class="pure-u-1">{{ $produto->tipo_titulo }}</h1>
									<?php $tipo_atual = $produto->tipo_slug.$produto->miolo ?>
								@endif

								<a href="produtos/{{ $produto->tipo_slug }}/{{ $produto->linha_slug }}/{{ $produto->slug }}" title="{{ $produto->titulo }}" class="pure-u-1-4">
									<img src="assets/images/produtos/thumbs/{{ $produto->imagem }}" alt="{{ $produto->titulo }}">
									<div class="overlay"></div>
								</a>
							@endforeach
						@endif
					</div>	
				</div>			

			</section>
			
		</div>
	</div>

@stop