@section('conteudo')

<div class="container">

  	<div class="page-header users-header">
    	<h2>
      		Adicionar Post
    	</h2>
  	</div>

  	<div class="row">
    	<div class="span12 columns">

			{{ Form::open( array('route' => 'painel.blog.store', 'files' => true, 'method' => 'post', 'id' => 'form-create-blog') ) }}

			<label>Título<br>
			<input type="text" name="titulo" required required-message="O título é obrigatório!" class="input-xxlarge"></label>

			<label>Data<br>
			<input type="text" name="data" class="datepicker" required required-message="A Data é obrigatória!"></label>

			<label>Categoria<br>
				<select name="blog_categorias_id"  required required-message="A Categoria é obrigatória!">
					<option value="">Selecione uma Categoria</option>
					@if($categorias)
					    @foreach($categorias as $categoria)
					    	<option value="{{ $categoria->id }}">{{ $categoria->titulo }}</option>
					    @endforeach
					@endif
				</select>
			</label>

			<label>Imagem<br>
			<input type="file" name="imagem" required required-message="A imagem é obrigatória!"></label>

			<label>Texto<br>
			<textarea name="texto" class="grande blog"></textarea></label>

			<div class="form-actions">
	        	{{ Form::submit('Inserir', array('class' => 'btn btn-primary')) }}
	        	{{ Form::button('Voltar', array('class' => 'btn btn-voltar')) }}
	      	</div>

			{{ Form::close() }}

		</div>
	</div>

@stop