@section('conteudo')

<div class="container">

	@if(Session::has('sucesso'))
	  <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

	@if(Session::has('falha'))
		<div class="alert alert-block alert-error fade in"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('falha') }}</div>
	@endif

  	<div class="page-header users-header">
    	<h2>
      		Categorias <a href="{{ URL::route('painel.categorias.create') }}" class="btn btn-success"><i class="icon-plus-sign"></i> Adicionar Categoria</a>
    	</h2>
  	</div>

  	<div class="row">
    	<div class="span12 columns">
      		<table class="table table-striped table-bordered table-condensed table-sortable" data-tabela="blog_categorias">

          		<thead>
            		<tr>
                  <th>Título</th>
                  <th><i class="icon-cog"></i></th>
                </tr>
          		</thead>

          		<tbody>
            	@foreach ($categorias as $categoria)

                	<tr class="tr-row" id="row_{{ $categoria->id }}">
                      <td>{{ $categoria->titulo }}</td>                  	    
                  		<td class="crud-actions">
                    		<a href="{{ URL::route('painel.categorias.edit', $categoria->id ) }}" class="btn btn-primary">editar</a>

                    	   {{ Form::open(array('route' => array('painel.categorias.destroy', $categoria->id), 'method' => 'delete')) }}
                                <button type="submit" href="{{ URL::route( 'painel.categorias.destroy', $categoria->id) }}" class="btn btn-danger btn-delete">excluir</butfon>
	                       {{ Form::close() }}
                  		</td>
                	</tr>

            	@endforeach
          		</tbody>

        	</table>

        {{ $categorias->links() }}

    	</div>
  	</div>

@stop