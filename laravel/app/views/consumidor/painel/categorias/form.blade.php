@section('conteudo')

<div class="container">

  	<div class="page-header users-header">
    	<h2>
      		Adicionar Categoria
    	</h2>
  	</div>

  	<div class="row">
    	<div class="span12 columns">

			{{ Form::open( array('route' => 'painel.categorias.store', 'files' => false, 'method' => 'post', 'id' => 'form-create-categorias') ) }}

			<label>Título<br>
			<input type="text" name="titulo" required required-message="O título é obrigatório!" class="input-xxlarge"></label>

			<div class="form-actions">
	        	{{ Form::submit('Inserir', array('class' => 'btn btn-primary')) }}
	        	{{ Form::button('Voltar', array('class' => 'btn btn-voltar')) }}
	      	</div>

			{{ Form::close() }}

		</div>
	</div>

@stop