@section('conteudo')

<div class="container">

  	<div class="page-header users-header">
    	<h2>
      		Adicionar Banner
    	</h2>
  	</div>

  	<div class="row">
    	<div class="span12 columns">

			{{ Form::open( array('route' => 'painel.banners.store', 'files' => true, 'method' => 'post', 'id' => 'form-create-banner') ) }}

			<label>Imagem<br>
			<input type="file" name="imagem" required></label>

			<label>Linha<br>
				<select name="linha" required>
					<option value="">Selecione uma Linha para o Banner</option>		
					@if($linhas)
						@foreach($linhas as $l)
							<option value="{{ $l->id }}">{{ $l->titulo }}</option>
						@endforeach
					@endif
				</select>
			</label>

			<label>Título<br>
			<input type="text" name="titulo" required class="input-xxlarge"></label>

			<label>Link<br>
			<input type="text" name="link" required class="input-xxlarge"></label>

			<div class="form-actions">
	        	{{ Form::submit('Inserir', array('class' => 'btn btn-primary')) }}
	        	{{ Form::button('Voltar', array('class' => 'btn btn-voltar')) }}
	      	</div>
		{{ Form::close() }}

		</div>
	</div>

@stop