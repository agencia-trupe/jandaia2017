@section('conteudo')

	<div class="centro">

		<h1>Fique por dentro <span>Acompanhe todas as novidades que a Jandaia busca pra você, aqui no nosso blog!</span></h1>
		
		@if($categoria)
			<h2>{{ $categoria->titulo }}</h2>
		@endif

		@if($posts)
			<div class="posts">
				<div class="pure-g-r grid">
				    @foreach($posts as $post)
						<a href="fique-por-dentro/ler/{{ $post->slug }}" class="pure-u-1-3 post-thumb" title="{{ $post->titulo }}">
							<div class="pad">
								<div class="imagem">
									<img src="assets/images/consumidor/blog/thumbs/{{ $post->imagem }}" alt="{{ $post->titulo }}">
									<div class="data">{{ Tools::exibeData($post->data) }}</div>
								</div>
								<div class="titulo">
									{{ $post->titulo }}
								</div>
								<div class="num_comentarios">
									<fb:comments-count href="{{ url().'/fique-por-dentro/ler/'. $post->slug }}"></fb:comments-count> pessoas já comentaram. Comente!
								</div>
								<div class="compartilhe">
									Que tal compartilhar?
								</div>
							</div>
						</a>
				    @endforeach
			    </div>
				
				@if($paginar)
					<div>
						<a href="fique-por-dentro" id="ver-mais-posts" title="Ver mais posts" data-pagina="{{$proximaPagina}}">ver mais <img src="assets/images/consumidor/layout/vermais-seta.png" alt="ver mais posts"> </a>
					</div>
				@endif

			</div>
		@endif

		<input type="hidden" id="ajCat" value="@if($categoria){{$categoria->slug}}@else0@endif">		

	</div>

@stop