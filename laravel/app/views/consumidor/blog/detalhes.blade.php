@section('conteudo')

	<div class="centro">

		<h1>Fique por dentro <span>Acompanhe todas as novidades que a Jandaia busca pra você, aqui no nosso blog!</span></h1>
	
		<h2><span>{{ $categoria->titulo }}</span></h2>

		<div class="detalhe pure-g-r">
			
			<div class="pure-u-1-2 detalhe-post">
				
				<div class="data">{{ Tools::exibeData($post->data) }}</div>

				<div class="titulo">{{ $post->titulo }}</div>

				<img class="img-principal" src="assets/images/consumidor/blog/{{$post->imagem}}" alt="{{$post->titulo}}">
				
				<div class="content">
					{{stripslashes($post->texto)}}
				</div>

				<div class="likes">
					<div class="fb-like" data-href="{{ Request::url() }}" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div>
				</div>
				
				@if($navProximo || $navAnterior)
					<div class="nav">
						@if($navAnterior)
							<a href="fique-por-dentro/ler/{{$navAnterior->slug}}" class="prev" title="ver post anterior">ver post anterior</a>
						@endif
						@if($navProximo)
							<a href="fique-por-dentro/ler/{{$navProximo->slug}}" class="next" title="ver próximo post">ver próximo post</a>
						@endif
					</div>
				@endif
				
			</div>

			<div class="pure-u-1-2 lateral-post">
				<div class="pad">
					@if($listaCategorias)
						<aside class="categorias">
							mais categorias:
							<ul>
								@foreach($listaCategorias as $cat)
									<li><a href="fique-por-dentro/{{$cat->slug}}" title="{{$cat->titulo}}">{{$cat->titulo}}</a></li>
								@endforeach
							</ul>
						</aside>
					@endif

					<div id="fb-comments">
						<div class="fb-comments" data-href="{{ Request::url() }}" data-width="400px" data-numposts="10" data-colorscheme="light"></div>
					</div>
				</div>
			</div>

		</div>

	</div>

@stop