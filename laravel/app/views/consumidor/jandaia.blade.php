@section('conteudo')
	
	<div class="centro">
		<div id="produtos-jandaia">
			<img src="assets/images/consumidor/layout/cadernos-agendas-jandaia.png" alt="Cadernos e Agendas Jandaia">
		</div>

		<h1>A Jandaia é uma das maiores fabricantes de cadernos e agendas do Brasil.</h1>

		<div class="pure-g timeline">
			<div class="pure-u-1-4">
				<div class="pad">
					<img src="assets/images/consumidor/layout/institucional1.png" alt="Quando começou? Desde 1956">
					Quando começou?<br>Desde 1956
				</div>
			</div>
			<div class="pure-u-1-4">
				<div class="pad">
					<img src="assets/images/consumidor/layout/institucional2.png" alt="Aonde fica? Na cidade de Caieiras - SP">
					Aonde fica?<br>Na cidade de Caieiras - SP
				</div>
			</div>
			<div class="pure-u-1-4">
				<div class="pad">
					<img src="assets/images/consumidor/layout/institucional3.png" alt="O grupo é o maior fabricante de papel reciclado do Brasil">
					O grupo é o maior fabricante de papel reciclado do Brasil
				</div>
			</div>
			<div class="pure-u-1-4">
				<div class="pad">
					<img src="assets/images/consumidor/layout/institucional4.png" alt="É uma empresa 100% nacional">
					É uma empresa<br>100% nacional
				</div>
			</div>
		</div>
	</div>

@stop