<!DOCTYPE html>
<html>
<head>
    <title>Mensagem de contato via formulário do site</title>
    <meta charset="utf-8">
</head>
<body>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Nome :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $name }}</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>E-mail :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $email }}</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Telefone :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $telephone }}</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>País :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $country }}</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Mensagem :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $user_message }}</span>
</body>
</html>
