<?php

class UsersConcursoTableSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->where('user_painel_concurso', '=', '1')->delete();

        User::create(
        	array(
        		'email' => 'bruno@trupe.net',
				'username' => 'trupeconcurso',
                'user_painel_concurso' => 1,
				'password' => Hash::make('senhatrupe')
        	)
        );
    }

}