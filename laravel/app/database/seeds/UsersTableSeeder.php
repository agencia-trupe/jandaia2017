<?php

class UsersTableSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->where('user_painel_concurso', '=', '0')->delete();

        User::create(
        	array(
        		'email' => 'contato@trupe.net',
				'username' => 'trupe',
                'user_painel_concurso' => 0,
				'password' => Hash::make('senhatrupe')
        	)
        );
    }

}