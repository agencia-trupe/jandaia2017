<?php

class RepresentantesTableSeeder extends Seeder {

    public function run()
    {
    	DB::table('representantes')->delete();
        $data = [
            [
            	'regiao' => 'Campinas, Sorocaba, Rib. Preto',
				'texto' => 'Tristique pulvinar lectus natoque! Est urna. Turpis proin magnis duis, mid tristique! Cum lorem dignissim, scelerisque elit scelerisque a nunc natoque urna amet etiam! Magna? Montes magna sit! Arcu vut.',
				'email' => 'contato@jandaia.com.br',
				'cep' => '04815-330',
				'representantes_estado_id' => '3',
				'representantes_cidade_id' => '1'
            ],
            [
            	'regiao' => 'ABC Paulista',
				'texto' => 'Tristique pulvinar lectus natoque! Est urna. Turpis proin magnis duis, mid tristique! Cum lorem dignissim, scelerisque elit scelerisque a nunc natoque urna amet etiam! Magna? Montes magna sit! Arcu vut.',
				'email' => 'contato@jandaia.com.br',
				'cep' => '04815-330',
				'representantes_estado_id' => '1',
				'representantes_cidade_id' => '1'
            ],
            [
            	'regiao' => 'São Paulo Capital',
				'texto' => 'Tristique pulvinar lectus natoque! Est urna. Turpis proin magnis duis, mid tristique! Cum lorem dignissim, scelerisque elit scelerisque a nunc natoque urna amet etiam! Magna? Montes magna sit! Arcu vut.',
				'email' => 'contato@jandaia.com.br',
				'cep' => '04815-330',
				'representantes_estado_id' => '4',
				'representantes_cidade_id' => '6'
            ],
            [
            	'regiao' => 'Visconde de Mauá',
				'texto' => 'Tristique pulvinar lectus natoque! Est urna. Turpis proin magnis duis, mid tristique! Cum lorem dignissim, scelerisque elit scelerisque a nunc natoque urna amet etiam! Magna? Montes magna sit! Arcu vut.',
				'email' => 'contato@jandaia.com.br',
				'cep' => '04815-330',
				'representantes_estado_id' => '1',
				'representantes_cidade_id' => '8'
            ],
        ];

        DB::table('representantes')->insert($data);
    }

}