<?php

class NovidadesTableSeeder extends Seeder {

    public function run()
    {
        DB::table('novidades')->delete();
        $novidades = [
            [
                'titulo' => 'Novidade 1',
                'slug' => 'novidade_1',
                'imagem' => 'imagem1.jpg',
                'olho' => 'Tincidunt quis lundium porta diam cursus! Lorem augue? Auctor. Facilisis! A sed, ac scelerisque elementum.',
                'texto' => 'Tincidunt quis lundium porta diam cursus! Lorem augue? Auctor. Facilisis! A sed, ac scelerisque elementum.Tincidunt quis lundium porta diam cursus! Lorem augue? Auctor. Facilisis! A sed, ac scelerisque elementum.Tincidunt quis lundium porta diam cursus! Lorem augue? Auctor. Facilisis! A sed, ac scelerisque elementum.',
                'data' => Date('Y-m-d H:i:s'),
                'created_at' => Date('Y-m-d H:i:s'),
                'updated_at' => Date('Y-m-d H:i:s')
            ],
            [
                'titulo' => 'Novidade 2',
                'slug' => 'novidade_2',
                'imagem' => 'imagem2.jpg',
                'olho' => 'Tincidunt quis lundium porta diam cursus! Lorem augue? Auctor. Facilisis! A sed, ac scelerisque elementum.',
                'texto' => 'Tincidunt quis lundium porta diam cursus! Lorem augue? Auctor. Facilisis! A sed, ac scelerisque elementum.Tincidunt quis lundium porta diam cursus! Lorem augue? Auctor. Facilisis! A sed, ac scelerisque elementum.Tincidunt quis lundium porta diam cursus! Lorem augue? Auctor. Facilisis! A sed, ac scelerisque elementum.',
                'data' => Date('Y-m-d H:i:s'),
                'created_at' => Date('Y-m-d H:i:s'),
                'updated_at' => Date('Y-m-d H:i:s')
            ],
            [
                'titulo' => 'Novidade 3',
                'slug' => 'novidade_3',
                'imagem' => 'imagem3.jpg',
                'olho' => 'Tincidunt quis lundium porta diam cursus! Lorem augue? Auctor. Facilisis! A sed, ac scelerisque elementum.',
                'texto' => 'Tincidunt quis lundium porta diam cursus! Lorem augue? Auctor. Facilisis! A sed, ac scelerisque elementum.Tincidunt quis lundium porta diam cursus! Lorem augue? Auctor. Facilisis! A sed, ac scelerisque elementum.Tincidunt quis lundium porta diam cursus! Lorem augue? Auctor. Facilisis! A sed, ac scelerisque elementum.',
                'data' => Date('Y-m-d H:i:s'),
                'created_at' => Date('Y-m-d H:i:s'),
                'updated_at' => Date('Y-m-d H:i:s')
            ]
        ];

        DB::table('novidades')->insert($novidades);
    }

}