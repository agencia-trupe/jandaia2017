<?php

class BannersTableSeeder extends Seeder {

    public function run()
    {
        DB::table('banners')->delete();
        $banners = [
            [
            	'imagem' => 'banner1.jpg',
				'titulo' => 'Primeiro Banner',
				'subtitulo' => 'Subtitulo Primeiro Banner',
				'link' => 'destino',
				'ordem' => '0',
				'created_at' => Date('Y-m-d H:i:s'),
                'updated_at' => Date('Y-m-d H:i:s')
            ],
            [
            	'imagem' => 'banner2.jpg',
				'titulo' => 'Segundo Banner',
				'subtitulo' => 'Subtitulo Segundo Banner',
				'link' => 'destino',
				'ordem' => '1',
				'created_at' => Date('Y-m-d H:i:s'),
                'updated_at' => Date('Y-m-d H:i:s')
            ],
            [
            	'imagem' => 'banner3.jpg',
				'titulo' => 'Terceiro Banner',
				'subtitulo' => 'Subtitulo Terceiro Banner',
				'link' => 'destino',
				'ordem' => '2',
				'created_at' => Date('Y-m-d H:i:s'),
                'updated_at' => Date('Y-m-d H:i:s')
            ]
        ];

        DB::table('banners')->insert($banners);
    }

}