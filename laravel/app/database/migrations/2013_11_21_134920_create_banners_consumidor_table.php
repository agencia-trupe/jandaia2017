<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannersConsumidorTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('banners_consumidor', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('imagem');
			$table->text('titulo');
			$table->text('subtitulo');
			$table->text('link');
			$table->integer('ordem')->default('0');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('banners_consumidor');
	}

}
