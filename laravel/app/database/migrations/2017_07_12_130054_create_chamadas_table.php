<?php

use Illuminate\Database\Migrations\Migration;

class CreateChamadasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('chamadas', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('imagem');
			$table->text('titulo');
			$table->text('link');
			$table->integer('ordem')->default('0');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('chamadas');
	}

}
