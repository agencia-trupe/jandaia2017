<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterParticipacoesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('participacoes_concurso_todentro', function(Blueprint $table)
		{
			$table->dropColumn('menor');
			$table->integer('semcpf')->default(0)->after('cpf');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('participacoes_concurso_todentro', function(Blueprint $table)
		{
			$table->dropColumn('semcpf');
			$table->integer('menor')->default(0)->after('cpf');
		});
	}

}