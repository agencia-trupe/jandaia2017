<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterRepresentantesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('representantes', function(Blueprint $table)
		{
			$table->dropColumn('representantes_estado_id');
			$table->dropColumn('representantes_cidade_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('representantes', function(Blueprint $table)
		{
			//
		});
	}

}