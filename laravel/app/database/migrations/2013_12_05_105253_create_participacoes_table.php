<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParticipacoesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('participacoes_concurso_todentro', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('etapa');
			$table->string('frase', 141);
			$table->string('nome_completo', 250);
			$table->string('email', 250);
			$table->string('telefone', 25);
			$table->string('cep', 10);
			$table->string('cpf', 15);
			$table->integer('menor');
			$table->string('nome_responsavel', 250);
			$table->string('cpf_responsavel', 15);
			$table->date('data_nascimento');
			$table->integer('aceite');
			$table->integer('newsletter');
			$table->date('data_cadastro');
			$table->string('ip_cadastro', 18);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('participacoes_concurso_todentro');
	}

}
