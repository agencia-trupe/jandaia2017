<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterLinhasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('produtos_linhas', function(Blueprint $table)
		{
			$table->text('texto_promocional')->after('detalhe_titulo');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('produtos_linhas', function(Blueprint $table)
		{
			$table->dropColumn('texto_promocional');
		});
	}

}