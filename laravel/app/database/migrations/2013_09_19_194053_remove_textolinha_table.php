<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveTextolinhaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::dropIfExists('produtos_linhas_textos');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::create('produtos_linhas_textos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('linha_id');
			$table->integer('tipo_id');
			$table->text('texto');
			$table->timestamps();
		});
	}

}