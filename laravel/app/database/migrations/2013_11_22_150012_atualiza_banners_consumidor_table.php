<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AtualizaBannersConsumidorTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('banners_consumidor', function(Blueprint $table)
		{
			$table->dropColumn('subtitulo');
			$table->integer('linha')->after('ordem');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('banners_consumidor', function(Blueprint $table)
		{
			$table->text('subtitulo')->after('titulo');
			$table->dropColumn('linha');
		});
	}

}