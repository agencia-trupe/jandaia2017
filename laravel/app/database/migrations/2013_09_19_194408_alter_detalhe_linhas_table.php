<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDetalheLinhasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('produtos_linhas', function(Blueprint $table)
		{
			$table->dropColumn('detalhe_titulo');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('produtos_linhas', function(Blueprint $table)
		{
			$table->text('detalhe_titulo')->after('slug');
		});
	}

}