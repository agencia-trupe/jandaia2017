<?php

// Namespace
namespace Consumidor;

// Core
use View, DB, App, Log, Redirect, Request, Mail;

// Models
use Tipo, Linha, Produto, Cadastro;

// BaseClass
//use BaseController;

class ProdutosController extends \Consumidor\BaseController {

	 /**
     * The layout that should be used for responses.
     */
    protected $layout = 'templates.consumidor';

	public function index($tipo = 'linha', $linha = false, $produto = false)
	{
		// Listagem de Tipos para o menu colorido
		$listaTipos = Tipo::orderBy('ordem')->get();
		// Listagem de Linhas para o menu lateral
		$listaLinhas = Linha::orderBy('titulo')->get();
		// Listagem de Produtos
		$listaProdutos = '';

		// Link para outros tipos de produtos da mesma linha
		$outrosProdutos = false;

		// Lista de Produtos da mesma linha, mesmo tipo mas subdivisões diferente
		$listaProdutosMesmoTipoCapas = false;
		$listaProdutosMesmoTipoMiolos = false;

		// Para Tipos de Produtos que não tem Miolos, esticar a caixa Branca de texto
		$esticarCaixa = false;

		// Para views com LINHA definida e TIPO definido mas sem PRODUTO
		// Mostrar título
		$mostrarTitulo = false;

		// Vamos verificar se o tipo existe
		$objTipo = Tipo::where('slug', '=', $tipo)->first();
		// Se não existir, fazer oq ?
		if(is_null($objTipo)){
			$tipo = 'linha';
		}

		// Vamos verificar se a linha existe
		$objLinha = Linha::where('slug', '=', $linha)->first();
		// Se não existir, fazer oq ?
		if(is_null($objLinha)){
			$linha = false;
		}

		// Verificando se o produto existe
		$objProduto = Produto::where('slug', '=', $produto)->where('produtos.publicar_cadernos', '=', '1')->first();
		// Se não existir
		if(is_null($objProduto)){
			$produto = false;
		}else{
			$seo = array(
				'title' => $objProduto->titulo,
				'image' => $objProduto->imagem,
				'description' => $objProduto->detalhes
			);

			$this->layout->with('seo', $seo);
		}



		// Determinação de View a ser exibida

		// url: /produtos ou /produtos/linha -> Mostrar seleção de tipo/linha
		if($tipo == 'linha' && !$linha && !$produto)
			$view = "index";


		// url: /produtos/agendas/ -> Mostrar lista de agendas
		elseif($tipo != "linha" && !$linha && !$produto){
			$view = "listar_por_tipo";

			// Listar 16 Produtos de um tipo
			$listaProdutos = Produto::orderBy(DB::raw('RAND()'))->where('produtos.publicar_cadernos', '=', '1')->where('produtos_tipo_id', '=', $objTipo->id)->where('miolo', 0)->take(12)->get();

			// Pegar slugs de Linha e de Tipo para cada produto
			foreach($listaProdutos as $k => $v){
				$this_linha = Linha::find($v->produtos_linha_id);
				$v->linha_slug = is_null($this_linha) ? '' : $this_linha->slug;
				$v->tipo_slug = $objTipo->slug;
			}

$qryLinhasComProdutos = <<<STR
SELECT *
FROM produtos_linhas
WHERE produtos_linhas.id IN(
    SELECT DISTINCT produtos.produtos_linha_id
    FROM produtos
    WHERE produtos.produtos_tipo_id = {$objTipo->id}
    AND produtos.publicar_cadernos = 1
) ORDER BY produtos_linhas.titulo ASC
STR;
			// Lista de Linhas que tem Produtos no mesmo Tipo do Selecionado
			$listaLinhas = DB::select(DB::raw($qryLinhasComProdutos));

		}

		// url: /produtos/papeis/coca-cola -> Mostrar papéis da Coca Cola
		// Se não houver slug de produto, destacar o primeiro
		elseif($tipo != "linha" && $linha){

			/**********************************/
			//		DETALHE DO PRODUTO
			/**********************************/

			$view = "listar_por_tipo_e_linha";

			// Listar Produtos de um tipo e de uma linha

			// Se o produto for um CADERNO (tipo == 11)
			// ordenar resultados pelo tipo, começando pelo tipo do atual
			if($objTipo->id == 11){

				// if(!is_null($objProduto)){
				// 	// Ordenação de cadernos muda de acordo com o selecionado
				// 	switch ($objProduto->tipo_caderno) {
				// 		case 1:
				// 			$str_ordenacao = "2, 5, 6, 7, 3, 1, 4, 8";
				// 			break;
				// 		case 2:
				// 			$str_ordenacao = "2, 5, 6, 7, 3, 1, 4, 8";
				// 			break;
				// 		case 3:
				// 			$str_ordenacao = "2, 5, 6, 7, 3, 1, 4, 8";
				// 			break;
				// 		case 4:
				// 			$str_ordenacao = "2, 5, 6, 7, 3, 1, 4, 8";
				// 			break;
				// 		default:
				// 			$str_ordenacao = "2, 5, 6, 7, 3, 1, 4, 8";
				// 			break;
				// 	}
				// }else{
					// Ordenação Padrão
					// Espiral 1(id:2) - 2(id:5) - 3(id:6) - 4(id:7) -> Plus(id:3) -> Brochura(id:1) -> Pedagógico(id:4) -> Cartográfico(id:8)
					$str_ordenacao = "2, 5, 6, 7, 3, 1, 4, 8";
				//}

				if(!is_null($objProduto) && $objProduto->produtos_tipo_id == 11){

					// Seleciona CAPAS de cadernos do mesmo tipo para serem mostrados abaixo do detalhe
					$listaProdutosMesmoTipoCapas = Produto::orderBy('ordem', 'ASC')
													->where('produtos.publicar_cadernos', '=', '1')
													->where('produtos_tipo_id', '=', $objTipo->id)
													->where('produtos_linha_id', '=', $objLinha->id)
													->where('tipo_caderno', '=', $objProduto->tipo_caderno)
													->where('miolo', '=', '0')
													->get();

					// Seleciona MIOLOS de cadernos do mesmo tipo para serem mostrados abaixo do detalhe
					$listaProdutosMesmoTipoMiolos = Produto::orderBy('ordem', 'ASC')
													->where('produtos.publicar_cadernos', '=', '1')
													->where('produtos_tipo_id', '=', $objTipo->id)
													->where('produtos_linha_id', '=', $objLinha->id)
													->where('tipo_caderno', '=', $objProduto->tipo_caderno)
													->where('miolo', '=', '1')
													->get();

					// Seleciona os demais tipos de caderno da mesma Linha
					$listaProdutos = Produto::orderBy(DB::raw("FIELD(tipo_caderno, $str_ordenacao), miolo, ordem"))
											->where('produtos.publicar_cadernos', '=', '1')
											->where('produtos_tipo_id', '=', $objTipo->id)
											->where('produtos_linha_id', '=', $objLinha->id)
											->where('tipo_caderno', '!=', $objProduto->tipo_caderno)
											->where('produtos.miolo', '=', 0)
											->get();

				}else{

					$listaProdutos = Produto::orderBy(DB::raw("FIELD(tipo_caderno, $str_ordenacao), miolo, ordem"))
											->where('produtos.publicar_cadernos', '=', '1')
											->where('produtos_tipo_id', '=', $objTipo->id)
											->where('produtos_linha_id', '=', $objLinha->id)
											->where('produtos.miolo', '=', 0)
											->get();
				}
			}else{
				if(!is_null($objProduto) && $objProduto->produtos_tipo_id == 12){
					$listaProdutosMesmoTipoCapas = Produto::orderBy('ordem', 'ASC')
													->where('produtos.publicar_cadernos', '=', '1')
													->where('produtos_tipo_id', '=', $objTipo->id)
													->where('produtos_linha_id', '=', $objLinha->id)
													->where('miolo', '=', '0')
													->get();

					$listaProdutosMesmoTipoMiolos = Produto::orderBy('ordem', 'ASC')
													->where('produtos.publicar_cadernos', '=', '1')
													->where('produtos_tipo_id', '=', $objTipo->id)
													->where('produtos_linha_id', '=', $objLinha->id)
													->where('miolo', '=', '1')
													->get();

					$listaProdutos = FALSE;
				}else{
					$esticarCaixa = TRUE;
					$mostrarTitulo = $objTipo->titulo.' '.$objLinha->titulo;
					$listaProdutos = Produto::orderBy('miolo', 'ASC')
											->orderBy('ordem', 'ASC')
											->where('produtos.publicar_cadernos', '=', '1')
											->where('produtos_tipo_id', '=', $objTipo->id)
											->where('produtos_linha_id', '=', $objLinha->id)
											->where('miolo', '=', '0')
											->get();
				}
			}

			// Pegar slugs de Linha e de Tipo para cada produto
			if($listaProdutos){
				foreach($listaProdutos as $k => $v){
					$this_linha = Linha::find($v->produtos_linha_id);
					$v->linha_slug = is_null($this_linha) ? '' : $this_linha->slug;
					$v->tipo_slug = $objTipo->slug;
					if(!$produto && $k == 0){
						$produto = $v;
					}
				}
			}

			// Se não houver nenhum produto a ser mostrado, redireciona para a home
			// ou para a home do tipo (caso) esteja selecionado
			if(($listaProdutos === FALSE || sizeof($listaProdutos) == 0) && !$listaProdutosMesmoTipoCapas && !$listaProdutosMesmoTipoMiolos && empty($objProduto)){
				if($objTipo)
					return Redirect::to("produtos/{$objTipo->slug}");
				else
					return Redirect::to("produtos");
			}

			// Buscar outros tipos de Produtos da mesma Linha
			$outrosProdutos = DB::table('produtos')
								->select(DB::raw("DISTINCT produtos.produtos_tipo_id"), 'produtos_tipos.slug', 'produtos_tipos.titulo')
								->join('produtos_tipos', 'produtos.produtos_tipo_id', '=', 'produtos_tipos.id')
								->where('produtos.produtos_linha_id', '=', $objLinha->id)
								->where('produtos.produtos_tipo_id', '!=', $objTipo->id)
								->where('miolo', '=', '0')
								->where('produtos.publicar_cadernos', '=', '1')
								->get();

$qryLinhasComProdutos = <<<STR
SELECT *
FROM produtos_linhas
WHERE produtos_linhas.id IN(
    SELECT DISTINCT produtos.produtos_linha_id
    FROM produtos
    WHERE produtos.produtos_tipo_id = {$objTipo->id}
    AND produtos.publicar_cadernos = 1
) ORDER BY produtos_linhas.titulo ASC
STR;
			// Lista de Linhas que tem Produtos no mesmo Tipo do Selecionado
			$listaLinhas = DB::select(DB::raw($qryLinhasComProdutos));
		}

		// url: /produtos/linha/coca-cola -> Mostrar todos os produtos da Linha CocaCola
		elseif($tipo == 'linha' && $linha && !$produto){
			$view = "listar_por_linha";

			// Listar Produtos de uma linha e de todos os tipos
			$listaProdutos = DB::table('produtos')
								->join('produtos_tipos', 'produtos.produtos_tipo_id', '=', 'produtos_tipos.id')
								->join('produtos_linhas', 'produtos.produtos_linha_id', '=', 'produtos_linhas.id')
								->orderBy(DB::raw("FIELD(produtos.produtos_tipo_id, 11, 12, 14, 15, 13), produtos.miolo, produtos.ordem"))
								->where('produtos.produtos_linha_id', '=', $objLinha->id)
								->where('produtos.miolo', '=', 0)
								->where('produtos.publicar_cadernos', '=', '1')
								->select('produtos.*', 'produtos_tipos.slug as tipo_slug', 'produtos_linhas.slug as linha_slug', 'produtos_tipos.titulo as tipo_titulo')
								->get();
		}

		// url:: /produtos/linha/coca-cola/15-caderno-capa-dura -> Retornar a view do produtos em Ajax
		elseif($tipo == 'linha' && $linha && $produto){
			// View em modal via AJAX foi descontinuada
			//$view = "detalhe_produto_ajax";
			App::abort(404);
		}

		else{
			App::abort(404);
		}

		$this->layout->content = View::make('consumidor.produtos.'.$view)->with('tipo', $objTipo)
																		 ->with('linha', $objLinha)
																		 ->with('produto', $objProduto)
																		 ->with('listaTipos', $listaTipos)
																		 ->with('listaLinhas', $listaLinhas)
																		 ->with('listaProdutos', $listaProdutos)
																		 ->with('outrosProdutos', $outrosProdutos)
																		 ->with('listaProdutosMesmoTipoCapas', $listaProdutosMesmoTipoCapas)
																		 ->with('listaProdutosMesmoTipoMiolos', $listaProdutosMesmoTipoMiolos)
																		 ->with('esticarCaixa', $esticarCaixa)
																		 ->with('mostrarTitulo', $mostrarTitulo);
	}

	// Promoção One Direction - encerrada em 27/01/2014
	// Prorrogada até 02/02/2014
	// Encerrada em 05/02/2014

	// public function formPromoModal(){
	// 	return View::make('consumidor.produtos.form_promo_modal')->with('envio', false);
	// }

	// public function enviarPromoModal(){
	// 	$data['nome'] = Request::get('nome');
	// 	$data['email'] = Request::get('email');
	// 	$data['telefone'] = Request::get('telefone');
	// 	$data['mensagem'] = Request::get('mensagem');

	// 	Mail::send('emails.promo_one_direction', $data, function($message) use ($data)
	// 	{
	// 	    $message->to('sac@jandaia.com', 'SAC Jandaia')
	// 	    		->subject('Promoção Rádio Disney One Direction')
	// 	    		->cc('promocao@radiod.com.br', 'Rádio Disney')
	// 	    		->bcc($data['email'], $data['nome'])
	// 	    		->replyTo($data['email'], $data['nome']);
	// 	});

	// 	$inscrever = Request::get('inscrever');
	// 	if($inscrever == '1'){
	// 		$check = Cadastro::where('email', '=', $data['email'])->get();

	// 		if(sizeof($check) == 0 && $data['email']){
	// 			$cad = new Cadastro;
	// 			$cad->nome = $data['nome'];
	// 			$cad->email = $data['email'];
	// 			$cad->save();
	// 		}
	// 	}

	// 	return View::make('consumidor.produtos.form_promo_modal')->with('envio', true);
	// }
}