<?php

// Namespace
namespace Consumidor;

// Core
use View, Request, DB, Response, App, Str;

// Models
use Blog, BlogCategoria;

// BaseClass
//use BaseController;

class BlogController extends \Consumidor\BaseController {

	 /**
     * The layout that should be used for responses.
     */
    protected $layout = 'templates.consumidor';

	public function index($slug_categoria = '')
	{

		$this->layout->with('css', 'consumidor/blog');

		$objCategoria = BlogCategoria::where('slug', '=', $slug_categoria)->first();
		
		$porPagina = 3;
		$paginaAtual = Request::get('page') ?: 0;
		$offset = $porPagina * $paginaAtual;
		$paginar = true;
		
		if (!is_null($objCategoria)) {
			$postagens = Blog::orderBy('data', 'desc')->where('blog_categorias_id', '=', $objCategoria->id)->skip($offset)->take($porPagina)->get();
			
			if(($offset + $porPagina) >= count(Blog::where('blog_categorias_id', '=', $objCategoria->id)->get()->toArray())){
				$paginar = false;
			}

		}else{
			$postagens = Blog::orderBy('data', 'desc')->skip($offset)->take($porPagina)->get();

			if(($offset + $porPagina) >= count(Blog::get()->toArray())){
				$paginar = false;
			}
		}

		if(Request::ajax()){
			return Response::json(array('posts' => $postagens->toArray(), 'proximaPagina' => $paginaAtual + 1, 'paginar' => $paginar));
    	}else{
			$this->layout->content = View::make('consumidor.blog.index')->with('posts', $postagens)
																	    ->with('categoria', $objCategoria)
																	    ->with('proximaPagina', $paginaAtual + 1)
																	    ->with('paginar', $paginar);
		}
	}

	public function detalhes($slug = '')
	{
		$this->layout->with('css', 'consumidor/blog');

		$navProximo = false;
		$navAnterior = false;

		$listaCategorias = BlogCategoria::all();

		$post = Blog::where('slug', '=', $slug)->first();

		if(is_null($post))
			App::abort('404');

		$objCategoria = BlogCategoria::where('id', '=', $post->blog_categorias_id)->first();

		$navProximo = Blog::orderBy('created_at', 'asc')->where('created_at', '>', $post->created_at)->first();
		$navAnterior = Blog::orderBy('created_at', 'desc')->where('created_at', '<', $post->created_at)->first();

		$seo = array(
			'title' => $post->titulo,
			'description' => Str::words(strip_tags($post->texto), 30),
			'image' => 'blog/'.$post->imagem
		);
		$this->layout->with('seo', $seo);

		$this->layout->content = View::make('consumidor.blog.detalhes')->with('post', $post)
																		->with('listaCategorias', $listaCategorias)
																		->with('categoria', $objCategoria)
																		->with('navProximo', $navProximo)
																		->with('navAnterior', $navAnterior);	
	}
}