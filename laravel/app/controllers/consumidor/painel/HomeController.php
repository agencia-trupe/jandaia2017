<?php

namespace Consumidor\Painel;

class HomeController extends BaseAdminController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/
	 /**
     * The layout that should be used for responses.
     */
    protected $layout = 'templates.painel.consumidor';

	public function index()
	{
		$data = array();
		$this->layout->content = \View::make('consumidor.painel.home', $data);
	}

	public function login()
	{
		return \View::make('consumidor.painel.login');
	}

}