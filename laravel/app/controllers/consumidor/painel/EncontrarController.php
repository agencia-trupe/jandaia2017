<?php

namespace Consumidor\Painel;

use View, Input, Str, Session, Redirect, Encontrar, EncontrarLocais, Excel, PHPExcel_IOFactory, chunkReadFilter;

class EncontrarController extends BaseAdminController {

	protected $layout = 'templates.painel.consumidor';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('consumidor.painel.encontrar.index')->with('registros', Encontrar::orderBy('id')->paginate(60));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('consumidor.painel.encontrar.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Encontrar;

		$object->descritivo = Input::get('descritivo');
		
		$arquivo = Input::file('arquivo');
		$filename = 'lista_encontrar_'.Date('d-m-Y_H-i-s').'.'.$arquivo->getClientOriginalExtension();
		$arquivo->move(app_path().'/internal_files/planilhas/', $filename);
		$object->arquivo = $filename;
		$object->save();

		$this->inserirCSV(app_path().'/internal_files/planilhas/'.$filename, $object->id);
		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Lista criada com sucesso.');

		return Redirect::route('painel.encontrar.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return View::make('consumidor.painel.encontrar.tabela')->with('registros', Encontrar::find($id)->registros);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('consumidor.painel.encontrar.edit')->with('registro', Encontrar::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Encontrar::find($id);

		$object->descritivo = Input::get('descritivo');
		
		$arquivo = Input::file('arquivo');
		$filename = 'lista_encontrar_'.Date('d-m-Y_H-i-s').'.'.$arquivo->getClientOriginalExtension();
		$arquivo->move(app_path().'/internal_files/planilhas/', $filename);
		$object->arquivo = $filename;

		$object->save();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Lista atualizada com sucesso.');

		return Redirect::route('painel.encontrar.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Encontrar::find($id);

		EncontrarLocais::where('id_listas', $object->id)->delete();
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Lista removida com sucesso.');

		return Redirect::route('painel.encontrar.index');
	}

	private function inserirCSV($arquivo, $id_lista){
		ini_set('display_errors', '1');
        ini_set('default_socket_timeout', 0);
        ini_set('memory_limit', '999M');
        set_time_limit(0);
		
		$row = 1;
		if (($handle = fopen($arquivo, "r")) !== FALSE) {
		    while (($data = fgetcsv($handle, 1000, ",", "\"")) !== FALSE) {
		        $num = count($data);
		        if($data[0] != "cod_categoria" && $data[0] != "CATEGORIA" && $data[0] != "CATEGORIA " && $data[0] != "" && $data[1] != "" && $data[2] != "" && $data[3] != ""):

		        	$rows = EncontrarLocais::where('tipo', '=', $data[0])
		        							->where('linha', '=', $data[1])
		        							->where('razao_social', '=', $data[2])
		        							->where('endereco', '=', $data[3])
		        							->get();

		        	if(count($rows) == 0){
						$registro = new EncontrarLocais;
						$registro->id_listas = $id_lista;
						$registro->tipo = $data[0] ?: '';
						$registro->linha = $data[1] ?: '';
						$registro->razao_social = $data[2] ?: '';
						$registro->endereco = $data[3] ?: '';
						$registro->bairro = $data[4] ?: '';
						$registro->cep = $data[5] ?: '';
						$registro->cidade = $data[6] ?: '';
						$registro->uf = $data[7] ?: '';
						$registro->telefone = $data[8] ?: '';
						//$registro->item = $data[] ?: '';
						//$registro->descricao = $data[] ?: '';
						//$registro->estado = $data[] ?: '';
						$registro->save();
					}
				endif;
		    }
		    fclose($handle);
		}		
	}
/*
Separar o arquivo csv : split -l 10000 lista.csv
SQL para Atualizar tipos de produtos após cadastrar
UPDATE onde_encontrar_registros SET tipo = 'agendas' WHERE tipo = 'AGENDAS';
UPDATE onde_encontrar_registros SET tipo = 'papeis-e-escritorio' WHERE tipo = 'PAPÉIS E BLOCOS';
UPDATE onde_encontrar_registros SET tipo = 'bolsas-e-ficharios' WHERE tipo = 'FICHÁRIOS E BOLSAS';
UPDATE onde_encontrar_registros SET tipo = 'gifts' WHERE tipo = 'GIFTS';
UPDATE onde_encontrar_registros SET tipo = 'cadernos' WHERE tipo LIKE 'CADERNO%';
UPDATE onde_encontrar_registros SET tipo = 'cadernos' WHERE descricao LIKE 'CAD.%';
*/	
}