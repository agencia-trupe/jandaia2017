<?php

// Namespace
namespace Consumidor\Painel;

// Core
use View, Input, Str, Session, Redirect, File;

// Models
use BlogCategoria;

// Libs
use Imagine;

class BlogCategoriasController extends BaseAdminController {

	protected $layout = 'templates.painel.consumidor';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('consumidor.painel.categorias.index')->with('categorias', BlogCategoria::orderBy('id')->paginate(10));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('consumidor.painel.categorias.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$categoria = new BlogCategoria;

		$categoria->titulo = Input::get('titulo');
		$categoria->save();

		$categoria->slug = Str::slug($categoria->id.'_'.$categoria->titulo);
		$categoria->save();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Categoria criada com sucesso.');

		return Redirect::route('painel.categorias.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('consumidor.painel.categorias.edit')->with('categoria', BlogCategoria::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$categoria = BlogCategoria::find($id);

		$categoria->titulo = Input::get('titulo');
		$categoria->save();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Categoria alterada com sucesso.');

		return Redirect::route('painel.categorias.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$categoria = BlogCategoria::find($id);
		$categoria->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Categoria removida com sucesso.');

		return Redirect::route('painel.categorias.index');
	}

}