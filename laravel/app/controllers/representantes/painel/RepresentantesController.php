<?php

// Namespace
namespace Representantes\Painel;

// Core
use View, Input, Str, Session, Redirect, DB;

// Models
use Representante, Estado, Cidade, Local;

// Libs


class RepresentantesController extends BaseAdminController {

	protected $layout = 'templates.painel.representantes';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$estado = Input::get('filtra-estado');
		$cidade = Input::get('filtra-cidade');

		if($estado && !$cidade){
			$listaRepresentantes = DB::table('representantes')->join('representantes_locais', 'representantes.id', '=', 'representantes_locais.id_representante')
															  ->where('representantes_locais.id_estado', $estado)
															  ->select('representantes.*')
															  ->paginate(20);
		}elseif(!$estado && $cidade){
			$listaRepresentantes = DB::table('representantes')->join('representantes_locais', 'representantes.id', '=', 'representantes_locais.id_representante')
															  ->where('representantes_locais.id_cidade', $cidade)
															  ->select('representantes.*')
															  ->paginate(20);
		}elseif($estado && $cidade){
			$listaRepresentantes = DB::table('representantes')->join('representantes_locais', 'representantes.id', '=', 'representantes_locais.id_representante')
															  ->where('representantes_locais.id_estado', $estado)
															  ->where('representantes_locais.id_cidade', $cidade)
															  ->select('representantes.*')
															  ->paginate(20);
		}else{
			$listaRepresentantes = Representante::paginate(20);
		}

		foreach ($listaRepresentantes as $key => $value) {
			$value->locais = Local::where('id_representante', '=', $value->id)->get();
		}

		$this->layout->content = View::make('representantes.painel.representantes.index')->with('representantes', $listaRepresentantes)
																			->with('estados', Estado::orderBy('nome')->get())
																			->with('cidades', Cidade::orderBy('nome')->get())
																			->with('filtra_estado', $estado)
																			->with('filtra_cidade', $cidade);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('representantes.painel.representantes.form')->with('estados', Estado::orderBy('nome')->get())
																		->with('cidades', Cidade::orderBy('nome')->get());
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Representante;

		$object->empresa = Input::get('empresa');
		$object->regiao = Input::get('regiao');
		$object->texto = Input::get('texto');
		$object->email = Input::get('email');
		$object->cep = Input::get('cep');
		$object->save();

		$estados = Input::get('representantes_estado_id');
		$cidades = Input::get('representantes_cidade_id');

		foreach ($estados as $key => $value) {
			if($value){
				$local = new Local;
				$local->id_estado = $value;
				if($cidades[$key])
					$local->id_cidade = $cidades[$key];
				$local->id_representante = $object->id;
				$local->save();
			}
		}

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Representante criado com sucesso.');

		return Redirect::route('painel.representantes.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('representantes.painel.representantes.edit')->with('representante', Representante::find($id))
																			->with('locais', Local::where('id_representante','=',$id)->get())
																			->with('estados', Estado::orderBy('nome')->get())
																			->with('cidades', Cidade::orderBy('nome')->get());
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Representante::find($id);

		$object->empresa = Input::get('empresa');
		$object->regiao = Input::get('regiao');
		$object->texto = Input::get('texto');
		$object->email = Input::get('email');
		$object->cep = Input::get('cep');
		$object->save();

		$estados = Input::get('representantes_estado_id');
		$cidades = Input::get('representantes_cidade_id');
		Local::where('id_representante', '=', $object->id)->delete();
		foreach ($estados as $key => $value) {
			if($value){
				$local = new Local;
				$local->id_estado = $value;
				if($cidades[$key])
					$local->id_cidade = $cidades[$key];
				$local->id_representante = $object->id;
				$local->save();
			}
		}

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Representante alterado com sucesso.');

		return Redirect::route('painel.representantes.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Representante::find($id);
		$object->delete();

		Local::where('id_representante', '=', $object->id)->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Representante removido com sucesso.');

		return Redirect::route('painel.representantes.index');
	}

}