<?php

// Namespace
namespace Representantes\Painel;

// Core
use View, Input, Str, Session, Redirect;

// Models
use Tipo;

// Libs
use Imagine, Tools;

class TiposController extends BaseAdminController {

	protected $layout = 'templates.painel.representantes';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('representantes.painel.tipos.index')->with('tipos', Tipo::orderBy('ordem')->get());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return Redirect::route('painel.tipos.index');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		return Redirect::route('painel.tipos.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('representantes.painel.tipos.edit')->with('tipo', Tipo::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$tipo = Tipo::find($id);
		$tipo->slug = Str::slug(Input::get('titulo'));
		$tipo->titulo = Input::get('titulo');
		$tipo->cor = Input::get('cor');
		$tipo->save();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Tipo alterado com sucesso.');

		return Redirect::route('painel.tipos.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$tipo = Tipo::find($id);
		$tipo->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Tipo removido com sucesso.');

		return Redirect::route('painel.tipos.index');
	}

}