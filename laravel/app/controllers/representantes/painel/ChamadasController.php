<?php

// Namespace
namespace Representantes\Painel;

// Core
use View, Input, Str, Session, Redirect, File;

// Models
use Chamada;

// Libs
use Imagine;

class ChamadasController extends BaseAdminController {

	protected $layout = 'templates.painel.representantes';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('representantes.painel.chamadas.index')->with('chamadas', Chamada::orderBy('ordem')->get());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('representantes.painel.chamadas.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

		$chamada = new Chamada;

		if(Input::hasFile('imagem')){

			$imagem = Input::file('imagem');

			//$filename = Str::random(20) .'.'. $imagem->getClientOriginalExtension();
			$random = '';
			do
			{
			    $filename = Str::slug($imagem->getClientOriginalName()).'_'.$random.'.'.File::extension($imagem->getClientOriginalName());
			    $file_path = 'assets/images/chamadas/'.$filename;
			    $random = Str::random(6);
			}
			while (File::exists($file_path));

			$imagem->move('assets/images/chamadas', $filename);
			$chamada->imagem = $filename;

			$imagine = new Imagine\Gd\Imagine();
			$sizeOriginal = new Imagine\Image\Box(370,260);
			$mode    = Imagine\Image\ImageInterface::THUMBNAIL_OUTBOUND;

			ini_set('memory_limit','256M');
			$imagine->open('assets/images/chamadas/'.$filename)
					->thumbnail($sizeOriginal, $mode)
					->save('assets/images/chamadas/'.$filename, array('quality' => 100));
		}

		$chamada->titulo = Input::get('titulo');
		$chamada->link = Input::get('link');
		$chamada->save();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Chamada criada com sucesso.');

		return Redirect::route('painel.chamadas.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('representantes.painel.chamadas.edit')->with('chamada', Chamada::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$chamada = Chamada::find($id);

		if(Input::hasFile('imagem')){
			$imagem = Input::file('imagem');
			//$filename = Str::random(20) .'.'. $imagem->getClientOriginalExtension();
			$random = '';
			do
			{
			    $filename = Str::slug($imagem->getClientOriginalName()).'_'.$random.'.'.File::extension($imagem->getClientOriginalName());
			    $file_path = 'assets/images/chamadas/'.$filename;
			    $random = Str::random(6);
			}
			while (File::exists($file_path));

			$imagem->move('assets/images/chamadas', $filename);
			$chamada->imagem = $filename;

			$imagine = new Imagine\Gd\Imagine();
			$sizeOriginal = new Imagine\Image\Box(370,260);
			$mode    = Imagine\Image\ImageInterface::THUMBNAIL_OUTBOUND;

			ini_set('memory_limit','256M');
			$imagine->open('assets/images/chamadas/'.$filename)
					->thumbnail($sizeOriginal, $mode)
					->save('assets/images/chamadas/'.$filename, array('quality' => 100));
		}

		$chamada->titulo = Input::get('titulo');
		$chamada->link = Input::get('link');
		$chamada->save();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Chamada alterada com sucesso.');

		return Redirect::route('painel.chamadas.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$chamada = Chamada::find($id);
		$chamada->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Chamada removida com sucesso.');

		return Redirect::route('painel.chamadas.index');
	}

}
