<?php

namespace Representantes\Painel;

use View, Input, File, Image, Str, Session, Redirect, Certificacoes;

class CertificacoesController extends BaseAdminController {

	protected $layout = 'templates.painel.representantes';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('representantes.painel.certificacoes.index')->with('certificacoes', Certificacoes::orderBy('ordem', 'asc')->get());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('representantes.painel.certificacoes.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Certificacoes;

		if(Input::hasFile('imagem')){

			$imagem = Input::file('imagem');

			$random = '';
			do
			{
			    $filename = Str::slug($imagem->getClientOriginalName()).'_'.$random.'.'.File::extension($imagem->getClientOriginalName());
			    $file_path = 'assets/images/certificacoes/'.$filename;
			    $random = Str::random(6);
			}
			while (File::exists($file_path));
			
			$object->imagem = $filename;

			ini_set('memory_limit','256M');
			Image::make(Input::file('imagem')->getRealPath())->resize(200, 90, true, false)->save('assets/images/certificacoes/'.$filename);			
		}		

		$object->titulo = Input::get('titulo');
		$object->texto = Input::get('texto');

		$object->save();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Certificação criada com sucesso.');

		return Redirect::route('painel.certificacoes.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('representantes.painel.certificacoes.edit')->with('certificacao', Certificacoes::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Certificacoes::find($id);

		if(Input::hasFile('imagem')){

			$imagem = Input::file('imagem');

			$random = '';
			do
			{
			    $filename = Str::slug($imagem->getClientOriginalName()).'_'.$random.'.'.File::extension($imagem->getClientOriginalName());
			    $file_path = 'assets/images/certificacoes/'.$filename;
			    $random = Str::random(6);
			}
			while (File::exists($file_path));
			
			$object->imagem = $filename;

			ini_set('memory_limit','256M');
			Image::make(Input::file('imagem')->getRealPath())->resize(200, 90, true, false)->save('assets/images/certificacoes/'.$filename);			
		}		

		$object->titulo = Input::get('titulo');
		$object->texto = Input::get('texto');
		$object->save();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Certificação alterada com sucesso.');

		return Redirect::route('painel.certificacoes.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Certificacoes::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Certificação removida com sucesso.');

		return Redirect::route('painel.certificacoes.index');
	}

}