<?php

// Namespace
namespace Representantes\Painel;

// Core
use View, Input, Str, Session, Redirect, File;

// Models
use Tipo, Linha, LinhaTexto;

// Libs
use Tools;

class LinhasController extends BaseAdminController {

	protected $layout = 'templates.painel.representantes';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('representantes.painel.linhas.index')->with('linhas', Linha::all());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('representantes.painel.linhas.form')->with('tipos', Tipo::all());
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$linha = new Linha;
		$linha->titulo = Input::get('titulo');
		$linha->texto_promocional = Input::get('texto_promocional');
		$linha->save();

		$linha->slug = Str::slug($linha->id.' '.$linha->titulo);
		$linha->save();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Linha inserida com sucesso.');

		return Redirect::route('painel.linhas.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('representantes.painel.linhas.edit')->with('linha', Linha::find($id))->with('tipos', Tipo::all());
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$linha = Linha::find($id);
		$linha->titulo = Input::get('titulo');
		$linha->texto_promocional = Input::get('texto_promocional');
		$linha->save();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Linha alterada com sucesso.');

		return Redirect::route('painel.linhas.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$tipo = Linha::find($id);
		$tipo->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Linha removida com sucesso.');

		return Redirect::route('painel.linhas.index');
	}

}