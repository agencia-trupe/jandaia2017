<?php

// Namespace
namespace Representantes\Painel;

// Core
use View, Input, Str, Session, Redirect, File, Response;

// Models
use Cadastro;

// Libs
use Excel;

class CadastrosController extends BaseAdminController {

	protected $layout = 'templates.painel.representantes';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('representantes.painel.cadastros.index')->with('cadastros', Cadastro::orderBy('nome', 'ASC')->paginate(40));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return Redirect::route('painel.cadastros.index');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		return Redirect::route('painel.cadastros.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return Redirect::route('painel.cadastros.index');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		return Redirect::route('painel.cadastros.index');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		return Redirect::route('painel.cadastros.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Cadastro::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Cadastro removido com sucesso.');

		return Redirect::route('painel.cadastros.index');
	}

	public function download()
	{
		$resultados = Cadastro::orderBy('nome', 'ASC')->get()->toArray();
		$pathToFile = public_path() . '/assets/temp/inscritos_newsletter_'.date('d-m-Y-H-i-s').'.xls';

		$titulos = array(
			'id' => 'ID',
			'nome' => 'Nome',
            'email' => 'E-mail',
            'created_at' => 'Data de Cadastro',
            'updated_at' => 'Última Atualização'
	    );
    	array_unshift($resultados, $titulos);
		Excel::fromArray($resultados)->save($pathToFile);
		return Response::download($pathToFile);
	}

}