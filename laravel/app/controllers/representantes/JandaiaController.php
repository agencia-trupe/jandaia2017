<?php

// Namespace
namespace Representantes;

// Core
use View;

// Models
use Institucional, Certificacoes;

// BaseClass
//use BaseController;

class JandaiaController extends \Representantes\BaseController {

	 /**
     * The layout that should be used for responses.
     */
    protected $layout = 'templates.representantes2017';

    public function index(){
        $this->layout->content = View::make('representantes2017.jandaia.index')
            ->with('conteudo', Institucional::where('slug', '=', 'historico')->first())
            ->with('produtos', Institucional::where('slug', '=', 'produtos-personalizados')->first());
    }

    public function sustentabilidade(){
        $this->layout->content = View::make('representantes2017.jandaia.sustentabilidade')
            ->with('conteudo', Institucional::where('slug', '=', 'sustentabilidade')->first());
    }

    public function lab(){
        $this->layout->content = View::make('representantes2017.jandaia.lab');
    }

    public function certificacoes(){
        $this->layout->content = View::make('representantes2017.jandaia.certificacoes')
            ->with('conteudo', Institucional::where('slug', '=', 'certificacoes')->first())
            ->with('certificacoes', Certificacoes::orderBy('ordem', 'ASC')->get());
    }

    public function exportacao(){
        $this->layout->content = View::make('representantes2017.jandaia.exportacao')
            ->with('conteudo', Institucional::where('slug', '=', 'exportacao')->first());
    }

}
