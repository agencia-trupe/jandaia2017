<?php

class Local extends Eloquent
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'representantes_locais';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	// array of attributes we CAN set on the backend
    protected $fillable = array('id_estado', 'id_cidade', 'id_representante', 'created_at', 'updated_at');

    // and those we CAN'T set on the backend
    protected $guarded = array('id');

}
