<?php

class Encontrar extends Eloquent
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'onde_encontrar_listas';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	// array of attributes we CAN set on the backend
    protected $fillable = array('descritivo', 'arquivo', 'created_at', 'updated_at');

    // and those we CAN'T set on the backend
    protected $guarded = array('id');

	public function registros()
    {
        return $this->hasMany('EncontrarLocais', 'id_listas');
    }
}
