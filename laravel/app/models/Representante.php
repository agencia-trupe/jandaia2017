<?php

class Representante extends Eloquent
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'representantes';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	// array of attributes we CAN set on the backend
    protected $fillable = array('regiao', 'texto', 'email', 'cep', 'representantes_estado_id', 'representantes_cidade_id',  'created_at', 'updated_at');

    // and those we CAN'T set on the backend
    protected $guarded = array('id');

    public function cidade(){
    	return $this->hasMany('cidade');
    }

    public function estado(){
    	return $this->hasMany('estado');
    }
}
