<?php

class ParticipacoesConcurso extends Eloquent
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'participacoes_concurso_todentro';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	// array of attributes we CAN set on the backend
    protected $fillable = array('nome_completo', 'email', 'telefone', 'cep', 'cpf', 'menor', 'nome_responsavel', 'cpf_responsavel', 'aceite', 'newsletter', 'data_cadastro', 'ip_cadastro');

    // and those we CAN'T set on the backend
    protected $guarded = array('id');

}
