<?php

class LinhaTexto extends Eloquent
{

	//**************************************//
	//   DESCONTINUADO NA VERSÃO FINAL 		//
	//**************************************//


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'produtos_linhas_textos';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	// array of attributes we CAN set on the backend
    protected $fillable = array('linha_id','tipo_id','texto', 'created_at', 'updated_at');

    // and those we CAN'T set on the backend
    protected $guarded = array('id');

    public static function texto($tipo_id, $linha_id){
    	$qry = DB::table('produtos_linhas_textos')->where('tipo_id', $tipo_id)->where('linha_id', $linha_id)->get();
    	return $qry && isset($qry[0]) ? $qry[0]->texto : '';
    }
}
