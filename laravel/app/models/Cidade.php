<?php

class Cidade extends Eloquent
{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'representantes_cidades';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	// array of attributes we CAN set on the backend
    protected $fillable = array('estado', 'uf', 'nome', 'created_at', 'updated_at');

    // and those we CAN'T set on the backend
    protected $guarded = array('id');

    public function estado(){
    	return $this->hasOne('estado');
    }

    public function representantes(){
    	return $this->hasMany('representante');
    }
}
