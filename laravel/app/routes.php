<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|	  Ordenação das Rotas:
| 1 - Rotas do site de Representantes (1ROTREP)
| 2 - Rotas do site de Consumidores (2ROTCON)
| 3 - Rotas do site Concurso To Dentro (3ROTTOD)
| 4 - Rotas para os Painéis Administrativos (4ROTPAI)
|
*/

// LOCAL
// $dominioRepresentantes = "jandaia.dev";
// $dominioCadernos = "cadernosjandaia";
// $dominioConcurso = "todentro";

// TESTE
// $dominioRepresentantes = "jandaia.test.10.68.204.118.xip.io";
// $dominioCadernos = "cadernosjandaia.test.10.68.204.118.xip.io";
// $dominioConcurso = "todentro.test.10.68.204.118.xip.io";

// PRODUÇÃO
$dominioRepresentantes = "jandaia.com";
$dominioCadernos = "desativado";
$dominioConcurso = "desativado";

/*
|--------------------------------------------------------------------------
| Rotas para o site : REPRESENTANTES (1ROTREP)
|--------------------------------------------------------------------------
| Rotas que integram o funcionamento do site principal 'Representantes'
| o 'Representantes' atua como o site principal
*/
Route::group(array(), function()
{
	/* Views estáticas adicionais
	Route::get('campanha', function(){
		return View::make('representantes.campanha');
	});
	Route::get('fabricacao', function(){
		return View::make('representantes.fabricacao');
	}); */
	Route::get('educacao', array('as' => 'landing-educacao', function() {
		return View::make('representantes2017.educacao');
	}));

	Route::get('/', array('as' => 'representantes.home', 'uses' => 'Representantes\HomeController@index'));
	// Route::post('cadastro', array('as' => 'representantes.cadastro', 'uses' => 'Representantes\HomeController@cadastro'));

	Route::get('onde-encontrar', array('as' => 'representantes.encontrar', 'uses' => 'Representantes\EncontrarController@index'));
	Route::get('onde-encontrar/online', array('as' => 'representantes.encontrar.online', 'uses' => 'Representantes\EncontrarController@online'));
	Route::get('onde-encontrar/buscar', array('as' => 'representantes.encontrar.buscar', 'uses' => 'Representantes\EncontrarController@buscar'));

	/* EMPRESA */
	Route::get('a-jandaia', [
		'as'   => 'representantes.jandaia.empresa',
		'uses' => 'Representantes\JandaiaController@index'
	]);
	Route::get('a-jandaia/certificacoes', [
		'as'   => 'representantes.jandaia.certificacoes',
		'uses' => 'Representantes\JandaiaController@certificacoes'
	]);
	Route::get('a-jandaia/sustentabilidade', [
		'as'   => 'representantes.jandaia.sustentabilidade',
		'uses' => 'Representantes\JandaiaController@sustentabilidade'
	]);
	Route::get('a-jandaia/lab', [
		'as'   => 'representantes.jandaia.lab',
		'uses' => 'Representantes\JandaiaController@lab'
	]);
	Route::get('a-jandaia/exportacao', [
		'as'   => 'representantes.jandaia.exportacao',
		'uses' => 'Representantes\JandaiaController@exportacao'
	]);


	/*
	Route::get('empresa', function(){ return Redirect::to('empresa/historico'); });
	Route::get('empresa/melhor-escolha', array('as' => 'representantes.empresa.escolha', 'uses' => 'Representantes\EmpresaController@escolha'));
	Route::get('empresa/historico', array('as' => 'representantes.empresa.historico', 'uses' => 'Representantes\EmpresaController@historico'));
	Route::get('empresa/sustentabilidade', array('as' => 'representantes.empresa.sustentabilidade', 'uses' => 'Representantes\EmpresaController@sustentabilidade'));
	Route::get('empresa/certificacoes', array('as' => 'representantes.empresa.certificacoes', 'uses' => 'Representantes\EmpresaController@certificacoes'));
	Route::get('empresa/qualidade-jandaia', array('as' => 'representantes.empresa.qualidade', 'uses' => 'Representantes\EmpresaController@qualidade'));
	Route::get('empresa/produtos-personalizados', array('as' => 'representantes.empresa.produtos', 'uses' => 'Representantes\EmpresaController@produtos'));
	Route::get('empresa/exportacao', array('as' => 'representantes.empresa.exportacao', 'uses' => 'Representantes\EmpresaController@exportacao'));
	Route::get('empresa/lab', array('as' => 'representantes.empresa.lab', 'uses' => 'Representantes\EmpresaController@lab'));

	Route::get('international', array('as' => 'representantes.international', 'uses' => 'Representantes\EmpresaController@international'));
	Route::post('international/enviar', array('as' => 'representantes.contato-international', 'uses' => 'Representantes\ContatoController@enviarInternacional'));*/

	Route::get('download/{arquivo?}', function($arquivo = false){

		if(!$arquivo)
			App::abort('404');
		else{

			$pathToFile = app_path().'/internal_files/arquivos/'.$arquivo;

			if(file_exists($pathToFile))
				return Response::download($pathToFile);
			else
				App::abort('404');
		}
	});

	/* PRODUTOS */
	Route::get('produtos/{tipo?}/{linha?}/{produto?}', array('as' => 'representantes.produtos', 'uses' => 'Representantes\ProdutosController@index'));

	/* BUSCA DE PRODUTOS */
	Route::any('busca', array('as' => 'representantes.busca', function(){

		$termo = Input::get('termo');

		if(!$termo){
			return Redirect::to('produtos');
		}else{

			$resultados = DB::table('produtos_linhas')
								->orderBy('produtos_linhas.titulo')
								->join('produtos', 'produtos_linhas.id', '=', 'produtos.produtos_linha_id')
								->where('produtos_linhas.titulo', 'like', "%$termo%")
								->orWhere('produtos.detalhes', 'like', "%$termo%")
								->orWhere('produtos.titulo', 'like', "%$termo%")
								->get();

			foreach($resultados as $k => $v){
				$this_linha = Linha::find($v->produtos_linha_id);
				$v->linha_slug = is_null($this_linha) ? '' : $this_linha->slug;
				$this_tipo = Tipo::find($v->produtos_tipo_id);
				$v->tipo_slug = is_null($this_tipo) ? '' : $this_tipo->slug;
			}

			if(count($resultados) == 0){

				return View::make('representantes.produtos.busca2017')->with('mensagem', "Nenhum resultado para o termo: <strong>$termo</strong>")
														 ->with('listaTipos', Tipo::orderBy('titulo')->get())
														 ->with('listaLinhas', Linha::orderBy('titulo')->get())
														 ->with('resultados', false)
														 ->with('termo', $termo)
														 ->with('css', 'produtos');

			}else{

				return View::make('representantes.produtos.busca2017')->with('mensagem', "Resultados para o termo: <strong>$termo</strong>")
														->with('listaTipos', Tipo::orderBy('titulo')->get())
														->with('listaLinhas', Linha::orderBy('titulo')->get())
														->with('resultados', $resultados)
														->with('termo', $termo)
														->with('css', 'produtos');
			}
		}
	}));


	/* REPRESENTANTES */
	Route::any('representantes/{estado?}', array('as' => 'representantes.representantes', 'uses' => 'Representantes\RepresentantesController@index'));

	/* NOVIDADES
	Route::get('novidades/{slug?}', array('as' => 'representantes.novidades', 'uses' => 'Representantes\NovidadesController@index')); */

	/* CONTATO */
	Route::get('contato', array('as' => 'representantes.contato', 'uses' => 'Representantes\ContatoController@index'));
	Route::post('contato', array('as' => 'representantes.contato.enviar', 'uses' => 'Representantes\ContatoController@enviar'));

	/*
	|--------------------------------------------------------------------------
	| Rotas do Painel de Administração
	|--------------------------------------------------------------------------|
	*/

	// Página Inicial do Painel
	// Se estiver logado mostra a view
	// Se não estiver redireciona para painel/login
	Route::get('painel', array('before' => 'auth', 'as' => 'representantes.painel.home', 'uses' => 'Representantes\Painel\HomeController@index'));

	// Página de Login
	Route::get('painel/login', array('as' => 'representantes.painel.login', 'uses' => 'Representantes\Painel\HomeController@login'));

	// Autenticação do Login (via POST)
	Route::post('painel/login',  array('as' => 'representantes.painel.auth', function(){

		$authvars = array(
			'username' => Input::get('username'),
			'password' => Input::get('password'),
			'user_painel_concurso' => '0'
		);
		if(Auth::attempt($authvars)){
			return Redirect::to('painel');
		}else{
			Session::flash('login_errors', true);
			return Redirect::to('painel/login');
		}
	}));

	// Ação de logout
	Route::get('painel/logout', array('as' => 'representantes.painel.off', function(){
		Auth::logout();
		return Redirect::to('painel');
	}));

	/* Ajax de ordenação de resultados do painel */
	Route::post('ajax/gravaOrdem', array('before' => 'auth', 'uses' => 'Representantes\AjaxController@gravaOrdem'));

	/* Busca Assíncrona de cidades para /fornecedores DEPRECATED */
	Route::get('ajax/pegarCidades/{id}', function($id){
		return Response::json(Cidade::where('estado', '=', $id)->get());
	});
});


Route::group(array('prefix' => 'painel', 'before' => 'auth'), function()
{
	Route::get('institucional/texto', array('as' => 'painel.institucional.texto', 'uses' => 'Representantes\Painel\InstitucionalController@texto'));
	Route::put('institucional/updateTexto', array('as' => 'painel.institucional.updateTexto', 'uses' => 'Representantes\Painel\InstitucionalController@updateTexto'));

    Route::resource('usuarios', 'Representantes\Painel\UsuariosController');
    Route::resource('banners', 'Representantes\Painel\BannersController');
    Route::resource('chamadas', 'Representantes\Painel\ChamadasController');
    Route::resource('novidades', 'Representantes\Painel\NovidadesController');
    Route::resource('tipos', 'Representantes\Painel\TiposController');
    Route::resource('linhas', 'Representantes\Painel\LinhasController');
    Route::resource('produtos', 'Representantes\Painel\ProdutosController');
    Route::resource('representantes', 'Representantes\Painel\RepresentantesController');
	Route::resource('institucional', 'Representantes\Painel\InstitucionalController');
	Route::resource('certificacoes', 'Representantes\Painel\CertificacoesController');
	Route::resource('cadastros', 'Representantes\Painel\CadastrosController');
	Route::get('painel/cadastros/download', array('as' => 'painel.cadastros.download', 'uses' => 'Representantes\Painel\CadastrosController@download'));

	Route::resource('onde-encontrar', 'Representantes\Painel\EncontrarController');
	Route::resource('onde-encontrar-online', 'Representantes\Painel\LojasOnlineController');
});
